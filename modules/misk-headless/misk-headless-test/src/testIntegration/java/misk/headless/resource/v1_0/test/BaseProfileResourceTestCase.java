package misk.headless.resource.v1_0.test;

import com.liferay.petra.reflect.ReflectionUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.json.JSONUtil;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.CompanyLocalServiceUtil;
import com.liferay.portal.kernel.test.util.GroupTestUtil;
import com.liferay.portal.kernel.test.util.RandomTestUtil;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.DateFormatFactoryUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.odata.entity.EntityField;
import com.liferay.portal.odata.entity.EntityModel;
import com.liferay.portal.test.rule.Inject;
import com.liferay.portal.test.rule.LiferayIntegrationTestRule;
import com.liferay.portal.vulcan.resource.EntityModelResource;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import java.text.DateFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Generated;

import javax.ws.rs.core.MultivaluedHashMap;

import misk.headless.client.dto.v1_0.AppUser;
import misk.headless.client.http.HttpInvoker;
import misk.headless.client.pagination.Page;
import misk.headless.client.resource.v1_0.ProfileResource;

import org.apache.commons.beanutils.BeanUtilsBean;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public abstract class BaseProfileResourceTestCase {

	@ClassRule
	@Rule
	public static final LiferayIntegrationTestRule liferayIntegrationTestRule =
		new LiferayIntegrationTestRule();

	@BeforeClass
	public static void setUpClass() throws Exception {
		_dateFormat = DateFormatFactoryUtil.getSimpleDateFormat(
			"yyyy-MM-dd'T'HH:mm:ss'Z'");
	}

	@Before
	public void setUp() throws Exception {
		irrelevantGroup = GroupTestUtil.addGroup();
		testGroup = GroupTestUtil.addGroup();

		testCompany = CompanyLocalServiceUtil.getCompany(
			testGroup.getCompanyId());

		_profileResource.setContextCompany(testCompany);

		ProfileResource.Builder builder = ProfileResource.builder();

		profileResource = builder.authentication(
			"test@liferay.com", "test"
		).locale(
			LocaleUtil.getDefault()
		).build();
	}

	@After
	public void tearDown() throws Exception {
		GroupTestUtil.deleteGroup(irrelevantGroup);
		GroupTestUtil.deleteGroup(testGroup);
	}

	@Test
	public void testGetMyBookings() throws Exception {
		Page<Profile> page = profileResource.getMyBookings(
			RandomTestUtil.randomString(), null);

		long totalCount = page.getTotalCount();

		Profile profile1 = testGetMyBookings_addProfile(randomProfile());

		Profile profile2 = testGetMyBookings_addProfile(randomProfile());

		page = profileResource.getMyBookings(null, null);

		Assert.assertEquals(totalCount + 2, page.getTotalCount());

		assertContains(profile1, (List<Profile>)page.getItems());
		assertContains(profile2, (List<Profile>)page.getItems());
		assertValid(page);
	}

	protected Profile testGetMyBookings_addProfile(Profile profile)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	@Test
	public void testCheckValidChangePhoneRequest() throws Exception {
		Assert.assertTrue(true);
	}

	protected void assertContains(Object profile, List<Object> profiles) {
		boolean contains = false;

		for (Object item : profiles) {
			if (equals(profile, item)) {
				contains = true;

				break;
			}
		}

		Assert.assertTrue(profiles + " does not contain " + profile, contains);
	}

	protected void assertHttpResponseStatusCode(
		int expectedHttpResponseStatusCode,
		HttpInvoker.HttpResponse actualHttpResponse) {

		Assert.assertEquals(
			expectedHttpResponseStatusCode, actualHttpResponse.getStatusCode());
	}

	protected void assertEquals(Object profile1, Object profile2) {
		Assert.assertTrue(
			profile1 + " does not equal " + profile2,
			equals(profile1, profile2));
	}

	protected void assertEquals(
		List<Object> profiles1, List<Object> profiles2) {

		Assert.assertEquals(profiles1.size(), profiles2.size());

		for (int i = 0; i < profiles1.size(); i++) {
			Object profile1 = profiles1.get(i);
			Object profile2 = profiles2.get(i);

			assertEquals(profile1, profile2);
		}
	}

	protected void assertEquals(AppUser appUser1, AppUser appUser2) {
		Assert.assertTrue(
			appUser1 + " does not equal " + appUser2,
			equals(appUser1, appUser2));
	}

	protected void assertEqualsIgnoringOrder(
		List<Object> profiles1, List<Object> profiles2) {

		Assert.assertEquals(profiles1.size(), profiles2.size());

		for (Object profile1 : profiles1) {
			boolean contains = false;

			for (Object profile2 : profiles2) {
				if (equals(profile1, profile2)) {
					contains = true;

					break;
				}
			}

			Assert.assertTrue(
				profiles2 + " does not contain " + profile1, contains);
		}
	}

	protected void assertValid(Object profile) throws Exception {
		boolean valid = true;

		for (String additionalAssertFieldName :
				getAdditionalAssertFieldNames()) {

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		Assert.assertTrue(valid);
	}

	protected void assertValid(Page<Object> page) {
		boolean valid = false;

		java.util.Collection<Object> profiles = page.getItems();

		int size = profiles.size();

		if ((page.getLastPage() > 0) && (page.getPage() > 0) &&
			(page.getPageSize() > 0) && (page.getTotalCount() > 0) &&
			(size > 0)) {

			valid = true;
		}

		Assert.assertTrue(valid);
	}

	protected void assertValid(AppUser appUser) {
		boolean valid = true;

		for (String additionalAssertFieldName :
				getAdditionalAppUserAssertFieldNames()) {

			if (Objects.equals(
					"androidDeviceToken", additionalAssertFieldName)) {

				if (appUser.getAndroidDeviceToken() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("appUserId", additionalAssertFieldName)) {
				if (appUser.getAppUserId() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("appleUserId", additionalAssertFieldName)) {
				if (appUser.getAppleUserId() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("autoLogin", additionalAssertFieldName)) {
				if (appUser.getAutoLogin() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("confirmPassword", additionalAssertFieldName)) {
				if (appUser.getConfirmPassword() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("deactivateReason", additionalAssertFieldName)) {
				if (appUser.getDeactivateReason() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("deleteReason", additionalAssertFieldName)) {
				if (appUser.getDeleteReason() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("emailAddress", additionalAssertFieldName)) {
				if (appUser.getEmailAddress() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals(
					"emailAddressVerified", additionalAssertFieldName)) {

				if (appUser.getEmailAddressVerified() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("facebookId", additionalAssertFieldName)) {
				if (appUser.getFacebookId() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("fullName", additionalAssertFieldName)) {
				if (appUser.getFullName() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("gcpToken", additionalAssertFieldName)) {
				if (appUser.getGcpToken() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("googleUserId", additionalAssertFieldName)) {
				if (appUser.getGoogleUserId() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("iosDeviceToken", additionalAssertFieldName)) {
				if (appUser.getIosDeviceToken() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("isVerified", additionalAssertFieldName)) {
				if (appUser.getIsVerified() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("languageId", additionalAssertFieldName)) {
				if (appUser.getLanguageId() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("newPassword", additionalAssertFieldName)) {
				if (appUser.getNewPassword() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("passcode", additionalAssertFieldName)) {
				if (appUser.getPasscode() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("password", additionalAssertFieldName)) {
				if (appUser.getPassword() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals(
					"passwordResetToken", additionalAssertFieldName)) {

				if (appUser.getPasswordResetToken() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("phoneNumber", additionalAssertFieldName)) {
				if (appUser.getPhoneNumber() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals(
					"phoneNumberVerified", additionalAssertFieldName)) {

				if (appUser.getPhoneNumberVerified() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals(
					"profileBannerImage", additionalAssertFieldName)) {

				if (appUser.getProfileBannerImage() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("profileImage", additionalAssertFieldName)) {
				if (appUser.getProfileImage() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("requestStatus", additionalAssertFieldName)) {
				if (appUser.getRequestStatus() == null) {
					valid = false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		Assert.assertTrue(valid);
	}

	protected String[] getAdditionalAssertFieldNames() {
		return new String[0];
	}

	protected String[] getAdditionalAppUserAssertFieldNames() {
		return new String[0];
	}

	protected List<GraphQLField> getGraphQLFields() throws Exception {
		List<GraphQLField> graphQLFields = new ArrayList<>();

		return graphQLFields;
	}

	protected List<GraphQLField> getGraphQLFields(Field... fields)
		throws Exception {

		List<GraphQLField> graphQLFields = new ArrayList<>();

		for (Field field : fields) {
			com.liferay.portal.vulcan.graphql.annotation.GraphQLField
				vulcanGraphQLField = field.getAnnotation(
					com.liferay.portal.vulcan.graphql.annotation.GraphQLField.
						class);

			if (vulcanGraphQLField != null) {
				Class<?> clazz = field.getType();

				if (clazz.isArray()) {
					clazz = clazz.getComponentType();
				}

				List<GraphQLField> childrenGraphQLFields = getGraphQLFields(
					getDeclaredFields(clazz));

				graphQLFields.add(
					new GraphQLField(field.getName(), childrenGraphQLFields));
			}
		}

		return graphQLFields;
	}

	protected String[] getIgnoredEntityFieldNames() {
		return new String[0];
	}

	protected boolean equals(Object profile1, Object profile2) {
		if (profile1 == profile2) {
			return true;
		}

		for (String additionalAssertFieldName :
				getAdditionalAssertFieldNames()) {

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		return true;
	}

	protected boolean equals(
		Map<String, Object> map1, Map<String, Object> map2) {

		if (Objects.equals(map1.keySet(), map2.keySet())) {
			for (Map.Entry<String, Object> entry : map1.entrySet()) {
				if (entry.getValue() instanceof Map) {
					if (!equals(
							(Map)entry.getValue(),
							(Map)map2.get(entry.getKey()))) {

						return false;
					}
				}
				else if (!Objects.deepEquals(
							entry.getValue(), map2.get(entry.getKey()))) {

					return false;
				}
			}

			return true;
		}

		return false;
	}

	protected boolean equals(AppUser appUser1, AppUser appUser2) {
		if (appUser1 == appUser2) {
			return true;
		}

		for (String additionalAssertFieldName :
				getAdditionalAppUserAssertFieldNames()) {

			if (Objects.equals(
					"androidDeviceToken", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						appUser1.getAndroidDeviceToken(),
						appUser2.getAndroidDeviceToken())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("appUserId", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getAppUserId(), appUser2.getAppUserId())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("appleUserId", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getAppleUserId(), appUser2.getAppleUserId())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("autoLogin", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getAutoLogin(), appUser2.getAutoLogin())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("confirmPassword", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getConfirmPassword(),
						appUser2.getConfirmPassword())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("deactivateReason", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getDeactivateReason(),
						appUser2.getDeactivateReason())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("deleteReason", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getDeleteReason(),
						appUser2.getDeleteReason())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("emailAddress", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getEmailAddress(),
						appUser2.getEmailAddress())) {

					return false;
				}

				continue;
			}

			if (Objects.equals(
					"emailAddressVerified", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						appUser1.getEmailAddressVerified(),
						appUser2.getEmailAddressVerified())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("facebookId", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getFacebookId(), appUser2.getFacebookId())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("fullName", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getFullName(), appUser2.getFullName())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("gcpToken", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getGcpToken(), appUser2.getGcpToken())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("googleUserId", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getGoogleUserId(),
						appUser2.getGoogleUserId())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("iosDeviceToken", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getIosDeviceToken(),
						appUser2.getIosDeviceToken())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("isVerified", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getIsVerified(), appUser2.getIsVerified())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("languageId", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getLanguageId(), appUser2.getLanguageId())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("newPassword", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getNewPassword(), appUser2.getNewPassword())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("passcode", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getPasscode(), appUser2.getPasscode())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("password", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getPassword(), appUser2.getPassword())) {

					return false;
				}

				continue;
			}

			if (Objects.equals(
					"passwordResetToken", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						appUser1.getPasswordResetToken(),
						appUser2.getPasswordResetToken())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("phoneNumber", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getPhoneNumber(), appUser2.getPhoneNumber())) {

					return false;
				}

				continue;
			}

			if (Objects.equals(
					"phoneNumberVerified", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						appUser1.getPhoneNumberVerified(),
						appUser2.getPhoneNumberVerified())) {

					return false;
				}

				continue;
			}

			if (Objects.equals(
					"profileBannerImage", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						appUser1.getProfileBannerImage(),
						appUser2.getProfileBannerImage())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("profileImage", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getProfileImage(),
						appUser2.getProfileImage())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("requestStatus", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getRequestStatus(),
						appUser2.getRequestStatus())) {

					return false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		return true;
	}

	protected Field[] getDeclaredFields(Class clazz) throws Exception {
		Stream<Field> stream = Stream.of(
			ReflectionUtil.getDeclaredFields(clazz));

		return stream.filter(
			field -> !field.isSynthetic()
		).toArray(
			Field[]::new
		);
	}

	protected java.util.Collection<EntityField> getEntityFields()
		throws Exception {

		if (!(_profileResource instanceof EntityModelResource)) {
			throw new UnsupportedOperationException(
				"Resource is not an instance of EntityModelResource");
		}

		EntityModelResource entityModelResource =
			(EntityModelResource)_profileResource;

		EntityModel entityModel = entityModelResource.getEntityModel(
			new MultivaluedHashMap());

		Map<String, EntityField> entityFieldsMap =
			entityModel.getEntityFieldsMap();

		return entityFieldsMap.values();
	}

	protected List<EntityField> getEntityFields(EntityField.Type type)
		throws Exception {

		java.util.Collection<EntityField> entityFields = getEntityFields();

		Stream<EntityField> stream = entityFields.stream();

		return stream.filter(
			entityField ->
				Objects.equals(entityField.getType(), type) &&
				!ArrayUtil.contains(
					getIgnoredEntityFieldNames(), entityField.getName())
		).collect(
			Collectors.toList()
		);
	}

	protected String getFilterString(
		EntityField entityField, String operator, Object profile) {

		StringBundler sb = new StringBundler();

		String entityFieldName = entityField.getName();

		sb.append(entityFieldName);

		sb.append(" ");
		sb.append(operator);
		sb.append(" ");

		throw new IllegalArgumentException(
			"Invalid entity field " + entityFieldName);
	}

	protected String invoke(String query) throws Exception {
		HttpInvoker httpInvoker = HttpInvoker.newHttpInvoker();

		httpInvoker.body(
			JSONUtil.put(
				"query", query
			).toString(),
			"application/json");
		httpInvoker.httpMethod(HttpInvoker.HttpMethod.POST);
		httpInvoker.path("http://localhost:8080/o/graphql");
		httpInvoker.userNameAndPassword("test@liferay.com:test");

		HttpInvoker.HttpResponse httpResponse = httpInvoker.invoke();

		return httpResponse.getContent();
	}

	protected JSONObject invokeGraphQLMutation(GraphQLField graphQLField)
		throws Exception {

		GraphQLField mutationGraphQLField = new GraphQLField(
			"mutation", graphQLField);

		return JSONFactoryUtil.createJSONObject(
			invoke(mutationGraphQLField.toString()));
	}

	protected JSONObject invokeGraphQLQuery(GraphQLField graphQLField)
		throws Exception {

		GraphQLField queryGraphQLField = new GraphQLField(
			"query", graphQLField);

		return JSONFactoryUtil.createJSONObject(
			invoke(queryGraphQLField.toString()));
	}

	protected AppUser randomAppUser() throws Exception {
		return new AppUser() {
			{
				androidDeviceToken = RandomTestUtil.randomString();
				appUserId = RandomTestUtil.randomLong();
				appleUserId = RandomTestUtil.randomString();
				autoLogin = RandomTestUtil.randomBoolean();
				confirmPassword = RandomTestUtil.randomString();
				deactivateReason = RandomTestUtil.randomString();
				deleteReason = RandomTestUtil.randomString();
				emailAddress = RandomTestUtil.randomString();
				emailAddressVerified = RandomTestUtil.randomBoolean();
				facebookId = RandomTestUtil.randomString();
				fullName = RandomTestUtil.randomString();
				gcpToken = RandomTestUtil.randomString();
				googleUserId = RandomTestUtil.randomString();
				iosDeviceToken = RandomTestUtil.randomString();
				isVerified = RandomTestUtil.randomBoolean();
				languageId = RandomTestUtil.randomString();
				newPassword = RandomTestUtil.randomString();
				passcode = RandomTestUtil.randomString();
				password = RandomTestUtil.randomString();
				passwordResetToken = RandomTestUtil.randomString();
				phoneNumber = RandomTestUtil.randomString();
				phoneNumberVerified = RandomTestUtil.randomBoolean();
				profileBannerImage = RandomTestUtil.randomString();
				profileImage = RandomTestUtil.randomString();
			}
		};
	}

	protected ProfileResource profileResource;
	protected Group irrelevantGroup;
	protected Company testCompany;
	protected Group testGroup;

	protected class GraphQLField {

		public GraphQLField(String key, GraphQLField... graphQLFields) {
			this(key, new HashMap<>(), graphQLFields);
		}

		public GraphQLField(String key, List<GraphQLField> graphQLFields) {
			this(key, new HashMap<>(), graphQLFields);
		}

		public GraphQLField(
			String key, Map<String, Object> parameterMap,
			GraphQLField... graphQLFields) {

			_key = key;
			_parameterMap = parameterMap;
			_graphQLFields = Arrays.asList(graphQLFields);
		}

		public GraphQLField(
			String key, Map<String, Object> parameterMap,
			List<GraphQLField> graphQLFields) {

			_key = key;
			_parameterMap = parameterMap;
			_graphQLFields = graphQLFields;
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder(_key);

			if (!_parameterMap.isEmpty()) {
				sb.append("(");

				for (Map.Entry<String, Object> entry :
						_parameterMap.entrySet()) {

					sb.append(entry.getKey());
					sb.append(": ");
					sb.append(entry.getValue());
					sb.append(", ");
				}

				sb.setLength(sb.length() - 2);

				sb.append(")");
			}

			if (!_graphQLFields.isEmpty()) {
				sb.append("{");

				for (GraphQLField graphQLField : _graphQLFields) {
					sb.append(graphQLField.toString());
					sb.append(", ");
				}

				sb.setLength(sb.length() - 2);

				sb.append("}");
			}

			return sb.toString();
		}

		private final List<GraphQLField> _graphQLFields;
		private final String _key;
		private final Map<String, Object> _parameterMap;

	}

	private static final com.liferay.portal.kernel.log.Log _log =
		LogFactoryUtil.getLog(BaseProfileResourceTestCase.class);

	private static BeanUtilsBean _beanUtilsBean = new BeanUtilsBean() {

		@Override
		public void copyProperty(Object bean, String name, Object value)
			throws IllegalAccessException, InvocationTargetException {

			if (value != null) {
				super.copyProperty(bean, name, value);
			}
		}

	};
	private static DateFormat _dateFormat;

	@Inject
	private misk.headless.resource.v1_0.ProfileResource _profileResource;

}