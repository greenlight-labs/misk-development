package misk.headless.resource.v1_0.test;

import com.liferay.petra.reflect.ReflectionUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.json.JSONUtil;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.CompanyLocalServiceUtil;
import com.liferay.portal.kernel.test.util.GroupTestUtil;
import com.liferay.portal.kernel.test.util.RandomTestUtil;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.DateFormatFactoryUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.odata.entity.EntityField;
import com.liferay.portal.odata.entity.EntityModel;
import com.liferay.portal.test.rule.Inject;
import com.liferay.portal.test.rule.LiferayIntegrationTestRule;
import com.liferay.portal.vulcan.resource.EntityModelResource;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import java.text.DateFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Generated;

import javax.ws.rs.core.MultivaluedHashMap;

import misk.headless.client.dto.v1_0.AppUser;
import misk.headless.client.dto.v1_0.DeactivateAccountPage;
import misk.headless.client.dto.v1_0.DeleteAccountPage;
import misk.headless.client.dto.v1_0.ManageAccountPage;
import misk.headless.client.http.HttpInvoker;
import misk.headless.client.pagination.Page;
import misk.headless.client.resource.v1_0.AccountResource;

import org.apache.commons.beanutils.BeanUtilsBean;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public abstract class BaseAccountResourceTestCase {

	@ClassRule
	@Rule
	public static final LiferayIntegrationTestRule liferayIntegrationTestRule =
		new LiferayIntegrationTestRule();

	@BeforeClass
	public static void setUpClass() throws Exception {
		_dateFormat = DateFormatFactoryUtil.getSimpleDateFormat(
			"yyyy-MM-dd'T'HH:mm:ss'Z'");
	}

	@Before
	public void setUp() throws Exception {
		irrelevantGroup = GroupTestUtil.addGroup();
		testGroup = GroupTestUtil.addGroup();

		testCompany = CompanyLocalServiceUtil.getCompany(
			testGroup.getCompanyId());

		_accountResource.setContextCompany(testCompany);

		AccountResource.Builder builder = AccountResource.builder();

		accountResource = builder.authentication(
			"test@liferay.com", "test"
		).locale(
			LocaleUtil.getDefault()
		).build();
	}

	@After
	public void tearDown() throws Exception {
		GroupTestUtil.deleteGroup(irrelevantGroup);
		GroupTestUtil.deleteGroup(testGroup);
	}

	@Test
	public void testGetDeactivateAccountPage() throws Exception {
		Account postAccount = testGetAccount_addAccount();

		DeactivateAccountPage postDeactivateAccountPage =
			testGetDeactivateAccountPage_addDeactivateAccountPage(
				postAccount.getId(), randomDeactivateAccountPage());

		DeactivateAccountPage getDeactivateAccountPage =
			accountResource.getDeactivateAccountPage(postAccount.getId());

		assertEquals(postDeactivateAccountPage, getDeactivateAccountPage);
		assertValid(getDeactivateAccountPage);
	}

	protected DeactivateAccountPage
			testGetDeactivateAccountPage_addDeactivateAccountPage(
				long accountId, DeactivateAccountPage deactivateAccountPage)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	@Test
	public void testGetManageAccountPage() throws Exception {
		Account postAccount = testGetAccount_addAccount();

		ManageAccountPage postManageAccountPage =
			testGetManageAccountPage_addManageAccountPage(
				postAccount.getId(), randomManageAccountPage());

		ManageAccountPage getManageAccountPage =
			accountResource.getManageAccountPage(postAccount.getId());

		assertEquals(postManageAccountPage, getManageAccountPage);
		assertValid(getManageAccountPage);
	}

	protected ManageAccountPage testGetManageAccountPage_addManageAccountPage(
			long accountId, ManageAccountPage manageAccountPage)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	@Test
	public void testDeactivateAccount() throws Exception {
		Assert.assertTrue(true);
	}

	@Test
	public void testReactivateAccount() throws Exception {
		Assert.assertTrue(true);
	}

	@Test
	public void testDeleteAccount() throws Exception {
		Assert.assertTrue(true);
	}

	@Test
	public void testGetDeleteAccountPage() throws Exception {
		Account postAccount = testGetAccount_addAccount();

		DeleteAccountPage postDeleteAccountPage =
			testGetDeleteAccountPage_addDeleteAccountPage(
				postAccount.getId(), randomDeleteAccountPage());

		DeleteAccountPage getDeleteAccountPage =
			accountResource.getDeleteAccountPage(postAccount.getId());

		assertEquals(postDeleteAccountPage, getDeleteAccountPage);
		assertValid(getDeleteAccountPage);
	}

	protected DeleteAccountPage testGetDeleteAccountPage_addDeleteAccountPage(
			long accountId, DeleteAccountPage deleteAccountPage)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	protected void assertContains(Object account, List<Object> accounts) {
		boolean contains = false;

		for (Object item : accounts) {
			if (equals(account, item)) {
				contains = true;

				break;
			}
		}

		Assert.assertTrue(accounts + " does not contain " + account, contains);
	}

	protected void assertHttpResponseStatusCode(
		int expectedHttpResponseStatusCode,
		HttpInvoker.HttpResponse actualHttpResponse) {

		Assert.assertEquals(
			expectedHttpResponseStatusCode, actualHttpResponse.getStatusCode());
	}

	protected void assertEquals(Object account1, Object account2) {
		Assert.assertTrue(
			account1 + " does not equal " + account2,
			equals(account1, account2));
	}

	protected void assertEquals(
		List<Object> accounts1, List<Object> accounts2) {

		Assert.assertEquals(accounts1.size(), accounts2.size());

		for (int i = 0; i < accounts1.size(); i++) {
			Object account1 = accounts1.get(i);
			Object account2 = accounts2.get(i);

			assertEquals(account1, account2);
		}
	}

	protected void assertEquals(
		DeactivateAccountPage deactivateAccountPage1,
		DeactivateAccountPage deactivateAccountPage2) {

		Assert.assertTrue(
			deactivateAccountPage1 + " does not equal " +
				deactivateAccountPage2,
			equals(deactivateAccountPage1, deactivateAccountPage2));
	}

	protected void assertEquals(
		ManageAccountPage manageAccountPage1,
		ManageAccountPage manageAccountPage2) {

		Assert.assertTrue(
			manageAccountPage1 + " does not equal " + manageAccountPage2,
			equals(manageAccountPage1, manageAccountPage2));
	}

	protected void assertEquals(AppUser appUser1, AppUser appUser2) {
		Assert.assertTrue(
			appUser1 + " does not equal " + appUser2,
			equals(appUser1, appUser2));
	}

	protected void assertEquals(
		DeleteAccountPage deleteAccountPage1,
		DeleteAccountPage deleteAccountPage2) {

		Assert.assertTrue(
			deleteAccountPage1 + " does not equal " + deleteAccountPage2,
			equals(deleteAccountPage1, deleteAccountPage2));
	}

	protected void assertEqualsIgnoringOrder(
		List<Object> accounts1, List<Object> accounts2) {

		Assert.assertEquals(accounts1.size(), accounts2.size());

		for (Object account1 : accounts1) {
			boolean contains = false;

			for (Object account2 : accounts2) {
				if (equals(account1, account2)) {
					contains = true;

					break;
				}
			}

			Assert.assertTrue(
				accounts2 + " does not contain " + account1, contains);
		}
	}

	protected void assertValid(Object account) throws Exception {
		boolean valid = true;

		for (String additionalAssertFieldName :
				getAdditionalAssertFieldNames()) {

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		Assert.assertTrue(valid);
	}

	protected void assertValid(Page<Object> page) {
		boolean valid = false;

		java.util.Collection<Object> accounts = page.getItems();

		int size = accounts.size();

		if ((page.getLastPage() > 0) && (page.getPage() > 0) &&
			(page.getPageSize() > 0) && (page.getTotalCount() > 0) &&
			(size > 0)) {

			valid = true;
		}

		Assert.assertTrue(valid);
	}

	protected void assertValid(DeactivateAccountPage deactivateAccountPage) {
		boolean valid = true;

		for (String additionalAssertFieldName :
				getAdditionalDeactivateAccountPageAssertFieldNames()) {

			if (Objects.equals("answers", additionalAssertFieldName)) {
				if (deactivateAccountPage.getAnswers() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals(
					"deactivateAccountLabel", additionalAssertFieldName)) {

				if (deactivateAccountPage.getDeactivateAccountLabel() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("description", additionalAssertFieldName)) {
				if (deactivateAccountPage.getDescription() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("question", additionalAssertFieldName)) {
				if (deactivateAccountPage.getQuestion() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("questionExcerpt", additionalAssertFieldName)) {
				if (deactivateAccountPage.getQuestionExcerpt() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("title", additionalAssertFieldName)) {
				if (deactivateAccountPage.getTitle() == null) {
					valid = false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		Assert.assertTrue(valid);
	}

	protected void assertValid(ManageAccountPage manageAccountPage) {
		boolean valid = true;

		for (String additionalAssertFieldName :
				getAdditionalManageAccountPageAssertFieldNames()) {

			if (Objects.equals(
					"deactivateAccountLabel", additionalAssertFieldName)) {

				if (manageAccountPage.getDeactivateAccountLabel() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals(
					"deleteAccountLabel", additionalAssertFieldName)) {

				if (manageAccountPage.getDeleteAccountLabel() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("description", additionalAssertFieldName)) {
				if (manageAccountPage.getDescription() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("subtitle", additionalAssertFieldName)) {
				if (manageAccountPage.getSubtitle() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("title", additionalAssertFieldName)) {
				if (manageAccountPage.getTitle() == null) {
					valid = false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		Assert.assertTrue(valid);
	}

	protected void assertValid(AppUser appUser) {
		boolean valid = true;

		for (String additionalAssertFieldName :
				getAdditionalAppUserAssertFieldNames()) {

			if (Objects.equals(
					"androidDeviceToken", additionalAssertFieldName)) {

				if (appUser.getAndroidDeviceToken() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("appUserId", additionalAssertFieldName)) {
				if (appUser.getAppUserId() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("appleUserId", additionalAssertFieldName)) {
				if (appUser.getAppleUserId() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("autoLogin", additionalAssertFieldName)) {
				if (appUser.getAutoLogin() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("confirmPassword", additionalAssertFieldName)) {
				if (appUser.getConfirmPassword() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("deactivateReason", additionalAssertFieldName)) {
				if (appUser.getDeactivateReason() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("deleteReason", additionalAssertFieldName)) {
				if (appUser.getDeleteReason() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("emailAddress", additionalAssertFieldName)) {
				if (appUser.getEmailAddress() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals(
					"emailAddressVerified", additionalAssertFieldName)) {

				if (appUser.getEmailAddressVerified() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("facebookId", additionalAssertFieldName)) {
				if (appUser.getFacebookId() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("fullName", additionalAssertFieldName)) {
				if (appUser.getFullName() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("gcpToken", additionalAssertFieldName)) {
				if (appUser.getGcpToken() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("googleUserId", additionalAssertFieldName)) {
				if (appUser.getGoogleUserId() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("iosDeviceToken", additionalAssertFieldName)) {
				if (appUser.getIosDeviceToken() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("isVerified", additionalAssertFieldName)) {
				if (appUser.getIsVerified() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("languageId", additionalAssertFieldName)) {
				if (appUser.getLanguageId() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("newPassword", additionalAssertFieldName)) {
				if (appUser.getNewPassword() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("passcode", additionalAssertFieldName)) {
				if (appUser.getPasscode() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("password", additionalAssertFieldName)) {
				if (appUser.getPassword() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals(
					"passwordResetToken", additionalAssertFieldName)) {

				if (appUser.getPasswordResetToken() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("phoneNumber", additionalAssertFieldName)) {
				if (appUser.getPhoneNumber() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals(
					"phoneNumberVerified", additionalAssertFieldName)) {

				if (appUser.getPhoneNumberVerified() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals(
					"profileBannerImage", additionalAssertFieldName)) {

				if (appUser.getProfileBannerImage() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("profileImage", additionalAssertFieldName)) {
				if (appUser.getProfileImage() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("requestStatus", additionalAssertFieldName)) {
				if (appUser.getRequestStatus() == null) {
					valid = false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		Assert.assertTrue(valid);
	}

	protected void assertValid(DeleteAccountPage deleteAccountPage) {
		boolean valid = true;

		for (String additionalAssertFieldName :
				getAdditionalDeleteAccountPageAssertFieldNames()) {

			if (Objects.equals("answers", additionalAssertFieldName)) {
				if (deleteAccountPage.getAnswers() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals(
					"deleteAccountLabel", additionalAssertFieldName)) {

				if (deleteAccountPage.getDeleteAccountLabel() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("description", additionalAssertFieldName)) {
				if (deleteAccountPage.getDescription() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("question", additionalAssertFieldName)) {
				if (deleteAccountPage.getQuestion() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("questionExcerpt", additionalAssertFieldName)) {
				if (deleteAccountPage.getQuestionExcerpt() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("title", additionalAssertFieldName)) {
				if (deleteAccountPage.getTitle() == null) {
					valid = false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		Assert.assertTrue(valid);
	}

	protected String[] getAdditionalAssertFieldNames() {
		return new String[0];
	}

	protected String[] getAdditionalDeactivateAccountPageAssertFieldNames() {
		return new String[0];
	}

	protected String[] getAdditionalManageAccountPageAssertFieldNames() {
		return new String[0];
	}

	protected String[] getAdditionalAppUserAssertFieldNames() {
		return new String[0];
	}

	protected String[] getAdditionalDeleteAccountPageAssertFieldNames() {
		return new String[0];
	}

	protected List<GraphQLField> getGraphQLFields() throws Exception {
		List<GraphQLField> graphQLFields = new ArrayList<>();

		return graphQLFields;
	}

	protected List<GraphQLField> getGraphQLFields(Field... fields)
		throws Exception {

		List<GraphQLField> graphQLFields = new ArrayList<>();

		for (Field field : fields) {
			com.liferay.portal.vulcan.graphql.annotation.GraphQLField
				vulcanGraphQLField = field.getAnnotation(
					com.liferay.portal.vulcan.graphql.annotation.GraphQLField.
						class);

			if (vulcanGraphQLField != null) {
				Class<?> clazz = field.getType();

				if (clazz.isArray()) {
					clazz = clazz.getComponentType();
				}

				List<GraphQLField> childrenGraphQLFields = getGraphQLFields(
					getDeclaredFields(clazz));

				graphQLFields.add(
					new GraphQLField(field.getName(), childrenGraphQLFields));
			}
		}

		return graphQLFields;
	}

	protected String[] getIgnoredEntityFieldNames() {
		return new String[0];
	}

	protected boolean equals(Object account1, Object account2) {
		if (account1 == account2) {
			return true;
		}

		for (String additionalAssertFieldName :
				getAdditionalAssertFieldNames()) {

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		return true;
	}

	protected boolean equals(
		Map<String, Object> map1, Map<String, Object> map2) {

		if (Objects.equals(map1.keySet(), map2.keySet())) {
			for (Map.Entry<String, Object> entry : map1.entrySet()) {
				if (entry.getValue() instanceof Map) {
					if (!equals(
							(Map)entry.getValue(),
							(Map)map2.get(entry.getKey()))) {

						return false;
					}
				}
				else if (!Objects.deepEquals(
							entry.getValue(), map2.get(entry.getKey()))) {

					return false;
				}
			}

			return true;
		}

		return false;
	}

	protected boolean equals(
		DeactivateAccountPage deactivateAccountPage1,
		DeactivateAccountPage deactivateAccountPage2) {

		if (deactivateAccountPage1 == deactivateAccountPage2) {
			return true;
		}

		for (String additionalAssertFieldName :
				getAdditionalDeactivateAccountPageAssertFieldNames()) {

			if (Objects.equals("answers", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						deactivateAccountPage1.getAnswers(),
						deactivateAccountPage2.getAnswers())) {

					return false;
				}

				continue;
			}

			if (Objects.equals(
					"deactivateAccountLabel", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						deactivateAccountPage1.getDeactivateAccountLabel(),
						deactivateAccountPage2.getDeactivateAccountLabel())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("description", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						deactivateAccountPage1.getDescription(),
						deactivateAccountPage2.getDescription())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("question", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						deactivateAccountPage1.getQuestion(),
						deactivateAccountPage2.getQuestion())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("questionExcerpt", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						deactivateAccountPage1.getQuestionExcerpt(),
						deactivateAccountPage2.getQuestionExcerpt())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("title", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						deactivateAccountPage1.getTitle(),
						deactivateAccountPage2.getTitle())) {

					return false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		return true;
	}

	protected boolean equals(
		ManageAccountPage manageAccountPage1,
		ManageAccountPage manageAccountPage2) {

		if (manageAccountPage1 == manageAccountPage2) {
			return true;
		}

		for (String additionalAssertFieldName :
				getAdditionalManageAccountPageAssertFieldNames()) {

			if (Objects.equals(
					"deactivateAccountLabel", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						manageAccountPage1.getDeactivateAccountLabel(),
						manageAccountPage2.getDeactivateAccountLabel())) {

					return false;
				}

				continue;
			}

			if (Objects.equals(
					"deleteAccountLabel", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						manageAccountPage1.getDeleteAccountLabel(),
						manageAccountPage2.getDeleteAccountLabel())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("description", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						manageAccountPage1.getDescription(),
						manageAccountPage2.getDescription())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("subtitle", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						manageAccountPage1.getSubtitle(),
						manageAccountPage2.getSubtitle())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("title", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						manageAccountPage1.getTitle(),
						manageAccountPage2.getTitle())) {

					return false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		return true;
	}

	protected boolean equals(AppUser appUser1, AppUser appUser2) {
		if (appUser1 == appUser2) {
			return true;
		}

		for (String additionalAssertFieldName :
				getAdditionalAppUserAssertFieldNames()) {

			if (Objects.equals(
					"androidDeviceToken", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						appUser1.getAndroidDeviceToken(),
						appUser2.getAndroidDeviceToken())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("appUserId", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getAppUserId(), appUser2.getAppUserId())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("appleUserId", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getAppleUserId(), appUser2.getAppleUserId())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("autoLogin", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getAutoLogin(), appUser2.getAutoLogin())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("confirmPassword", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getConfirmPassword(),
						appUser2.getConfirmPassword())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("deactivateReason", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getDeactivateReason(),
						appUser2.getDeactivateReason())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("deleteReason", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getDeleteReason(),
						appUser2.getDeleteReason())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("emailAddress", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getEmailAddress(),
						appUser2.getEmailAddress())) {

					return false;
				}

				continue;
			}

			if (Objects.equals(
					"emailAddressVerified", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						appUser1.getEmailAddressVerified(),
						appUser2.getEmailAddressVerified())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("facebookId", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getFacebookId(), appUser2.getFacebookId())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("fullName", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getFullName(), appUser2.getFullName())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("gcpToken", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getGcpToken(), appUser2.getGcpToken())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("googleUserId", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getGoogleUserId(),
						appUser2.getGoogleUserId())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("iosDeviceToken", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getIosDeviceToken(),
						appUser2.getIosDeviceToken())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("isVerified", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getIsVerified(), appUser2.getIsVerified())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("languageId", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getLanguageId(), appUser2.getLanguageId())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("newPassword", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getNewPassword(), appUser2.getNewPassword())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("passcode", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getPasscode(), appUser2.getPasscode())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("password", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getPassword(), appUser2.getPassword())) {

					return false;
				}

				continue;
			}

			if (Objects.equals(
					"passwordResetToken", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						appUser1.getPasswordResetToken(),
						appUser2.getPasswordResetToken())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("phoneNumber", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getPhoneNumber(), appUser2.getPhoneNumber())) {

					return false;
				}

				continue;
			}

			if (Objects.equals(
					"phoneNumberVerified", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						appUser1.getPhoneNumberVerified(),
						appUser2.getPhoneNumberVerified())) {

					return false;
				}

				continue;
			}

			if (Objects.equals(
					"profileBannerImage", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						appUser1.getProfileBannerImage(),
						appUser2.getProfileBannerImage())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("profileImage", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getProfileImage(),
						appUser2.getProfileImage())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("requestStatus", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getRequestStatus(),
						appUser2.getRequestStatus())) {

					return false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		return true;
	}

	protected boolean equals(
		DeleteAccountPage deleteAccountPage1,
		DeleteAccountPage deleteAccountPage2) {

		if (deleteAccountPage1 == deleteAccountPage2) {
			return true;
		}

		for (String additionalAssertFieldName :
				getAdditionalDeleteAccountPageAssertFieldNames()) {

			if (Objects.equals("answers", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						deleteAccountPage1.getAnswers(),
						deleteAccountPage2.getAnswers())) {

					return false;
				}

				continue;
			}

			if (Objects.equals(
					"deleteAccountLabel", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						deleteAccountPage1.getDeleteAccountLabel(),
						deleteAccountPage2.getDeleteAccountLabel())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("description", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						deleteAccountPage1.getDescription(),
						deleteAccountPage2.getDescription())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("question", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						deleteAccountPage1.getQuestion(),
						deleteAccountPage2.getQuestion())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("questionExcerpt", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						deleteAccountPage1.getQuestionExcerpt(),
						deleteAccountPage2.getQuestionExcerpt())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("title", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						deleteAccountPage1.getTitle(),
						deleteAccountPage2.getTitle())) {

					return false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		return true;
	}

	protected Field[] getDeclaredFields(Class clazz) throws Exception {
		Stream<Field> stream = Stream.of(
			ReflectionUtil.getDeclaredFields(clazz));

		return stream.filter(
			field -> !field.isSynthetic()
		).toArray(
			Field[]::new
		);
	}

	protected java.util.Collection<EntityField> getEntityFields()
		throws Exception {

		if (!(_accountResource instanceof EntityModelResource)) {
			throw new UnsupportedOperationException(
				"Resource is not an instance of EntityModelResource");
		}

		EntityModelResource entityModelResource =
			(EntityModelResource)_accountResource;

		EntityModel entityModel = entityModelResource.getEntityModel(
			new MultivaluedHashMap());

		Map<String, EntityField> entityFieldsMap =
			entityModel.getEntityFieldsMap();

		return entityFieldsMap.values();
	}

	protected List<EntityField> getEntityFields(EntityField.Type type)
		throws Exception {

		java.util.Collection<EntityField> entityFields = getEntityFields();

		Stream<EntityField> stream = entityFields.stream();

		return stream.filter(
			entityField ->
				Objects.equals(entityField.getType(), type) &&
				!ArrayUtil.contains(
					getIgnoredEntityFieldNames(), entityField.getName())
		).collect(
			Collectors.toList()
		);
	}

	protected String getFilterString(
		EntityField entityField, String operator, Object account) {

		StringBundler sb = new StringBundler();

		String entityFieldName = entityField.getName();

		sb.append(entityFieldName);

		sb.append(" ");
		sb.append(operator);
		sb.append(" ");

		throw new IllegalArgumentException(
			"Invalid entity field " + entityFieldName);
	}

	protected String invoke(String query) throws Exception {
		HttpInvoker httpInvoker = HttpInvoker.newHttpInvoker();

		httpInvoker.body(
			JSONUtil.put(
				"query", query
			).toString(),
			"application/json");
		httpInvoker.httpMethod(HttpInvoker.HttpMethod.POST);
		httpInvoker.path("http://localhost:8080/o/graphql");
		httpInvoker.userNameAndPassword("test@liferay.com:test");

		HttpInvoker.HttpResponse httpResponse = httpInvoker.invoke();

		return httpResponse.getContent();
	}

	protected JSONObject invokeGraphQLMutation(GraphQLField graphQLField)
		throws Exception {

		GraphQLField mutationGraphQLField = new GraphQLField(
			"mutation", graphQLField);

		return JSONFactoryUtil.createJSONObject(
			invoke(mutationGraphQLField.toString()));
	}

	protected JSONObject invokeGraphQLQuery(GraphQLField graphQLField)
		throws Exception {

		GraphQLField queryGraphQLField = new GraphQLField(
			"query", graphQLField);

		return JSONFactoryUtil.createJSONObject(
			invoke(queryGraphQLField.toString()));
	}

	protected DeactivateAccountPage randomDeactivateAccountPage()
		throws Exception {

		return new DeactivateAccountPage() {
			{
				deactivateAccountLabel = RandomTestUtil.randomString();
				description = RandomTestUtil.randomString();
				question = RandomTestUtil.randomString();
				questionExcerpt = RandomTestUtil.randomString();
				title = RandomTestUtil.randomString();
			}
		};
	}

	protected ManageAccountPage randomManageAccountPage() throws Exception {
		return new ManageAccountPage() {
			{
				deactivateAccountLabel = RandomTestUtil.randomString();
				deleteAccountLabel = RandomTestUtil.randomString();
				description = RandomTestUtil.randomString();
				subtitle = RandomTestUtil.randomString();
				title = RandomTestUtil.randomString();
			}
		};
	}

	protected AppUser randomAppUser() throws Exception {
		return new AppUser() {
			{
				androidDeviceToken = RandomTestUtil.randomString();
				appUserId = RandomTestUtil.randomLong();
				appleUserId = RandomTestUtil.randomString();
				autoLogin = RandomTestUtil.randomBoolean();
				confirmPassword = RandomTestUtil.randomString();
				deactivateReason = RandomTestUtil.randomString();
				deleteReason = RandomTestUtil.randomString();
				emailAddress = RandomTestUtil.randomString();
				emailAddressVerified = RandomTestUtil.randomBoolean();
				facebookId = RandomTestUtil.randomString();
				fullName = RandomTestUtil.randomString();
				gcpToken = RandomTestUtil.randomString();
				googleUserId = RandomTestUtil.randomString();
				iosDeviceToken = RandomTestUtil.randomString();
				isVerified = RandomTestUtil.randomBoolean();
				languageId = RandomTestUtil.randomString();
				newPassword = RandomTestUtil.randomString();
				passcode = RandomTestUtil.randomString();
				password = RandomTestUtil.randomString();
				passwordResetToken = RandomTestUtil.randomString();
				phoneNumber = RandomTestUtil.randomString();
				phoneNumberVerified = RandomTestUtil.randomBoolean();
				profileBannerImage = RandomTestUtil.randomString();
				profileImage = RandomTestUtil.randomString();
			}
		};
	}

	protected DeleteAccountPage randomDeleteAccountPage() throws Exception {
		return new DeleteAccountPage() {
			{
				deleteAccountLabel = RandomTestUtil.randomString();
				description = RandomTestUtil.randomString();
				question = RandomTestUtil.randomString();
				questionExcerpt = RandomTestUtil.randomString();
				title = RandomTestUtil.randomString();
			}
		};
	}

	protected AccountResource accountResource;
	protected Group irrelevantGroup;
	protected Company testCompany;
	protected Group testGroup;

	protected class GraphQLField {

		public GraphQLField(String key, GraphQLField... graphQLFields) {
			this(key, new HashMap<>(), graphQLFields);
		}

		public GraphQLField(String key, List<GraphQLField> graphQLFields) {
			this(key, new HashMap<>(), graphQLFields);
		}

		public GraphQLField(
			String key, Map<String, Object> parameterMap,
			GraphQLField... graphQLFields) {

			_key = key;
			_parameterMap = parameterMap;
			_graphQLFields = Arrays.asList(graphQLFields);
		}

		public GraphQLField(
			String key, Map<String, Object> parameterMap,
			List<GraphQLField> graphQLFields) {

			_key = key;
			_parameterMap = parameterMap;
			_graphQLFields = graphQLFields;
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder(_key);

			if (!_parameterMap.isEmpty()) {
				sb.append("(");

				for (Map.Entry<String, Object> entry :
						_parameterMap.entrySet()) {

					sb.append(entry.getKey());
					sb.append(": ");
					sb.append(entry.getValue());
					sb.append(", ");
				}

				sb.setLength(sb.length() - 2);

				sb.append(")");
			}

			if (!_graphQLFields.isEmpty()) {
				sb.append("{");

				for (GraphQLField graphQLField : _graphQLFields) {
					sb.append(graphQLField.toString());
					sb.append(", ");
				}

				sb.setLength(sb.length() - 2);

				sb.append("}");
			}

			return sb.toString();
		}

		private final List<GraphQLField> _graphQLFields;
		private final String _key;
		private final Map<String, Object> _parameterMap;

	}

	private static final com.liferay.portal.kernel.log.Log _log =
		LogFactoryUtil.getLog(BaseAccountResourceTestCase.class);

	private static BeanUtilsBean _beanUtilsBean = new BeanUtilsBean() {

		@Override
		public void copyProperty(Object bean, String name, Object value)
			throws IllegalAccessException, InvocationTargetException {

			if (value != null) {
				super.copyProperty(bean, name, value);
			}
		}

	};
	private static DateFormat _dateFormat;

	@Inject
	private misk.headless.resource.v1_0.AccountResource _accountResource;

}