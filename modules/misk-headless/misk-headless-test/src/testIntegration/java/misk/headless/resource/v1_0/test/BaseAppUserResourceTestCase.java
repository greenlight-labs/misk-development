package misk.headless.resource.v1_0.test;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;

import com.liferay.petra.reflect.ReflectionUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.json.JSONUtil;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.CompanyLocalServiceUtil;
import com.liferay.portal.kernel.test.util.GroupTestUtil;
import com.liferay.portal.kernel.test.util.RandomTestUtil;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.DateFormatFactoryUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.odata.entity.EntityField;
import com.liferay.portal.odata.entity.EntityModel;
import com.liferay.portal.test.rule.Inject;
import com.liferay.portal.test.rule.LiferayIntegrationTestRule;
import com.liferay.portal.vulcan.resource.EntityModelResource;

import java.io.File;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import java.text.DateFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Generated;

import javax.ws.rs.core.MultivaluedHashMap;

import misk.headless.client.dto.v1_0.AppUser;
import misk.headless.client.http.HttpInvoker;
import misk.headless.client.pagination.Page;
import misk.headless.client.resource.v1_0.AppUserResource;
import misk.headless.client.serdes.v1_0.AppUserSerDes;

import org.apache.commons.beanutils.BeanUtilsBean;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public abstract class BaseAppUserResourceTestCase {

	@ClassRule
	@Rule
	public static final LiferayIntegrationTestRule liferayIntegrationTestRule =
		new LiferayIntegrationTestRule();

	@BeforeClass
	public static void setUpClass() throws Exception {
		_dateFormat = DateFormatFactoryUtil.getSimpleDateFormat(
			"yyyy-MM-dd'T'HH:mm:ss'Z'");
	}

	@Before
	public void setUp() throws Exception {
		irrelevantGroup = GroupTestUtil.addGroup();
		testGroup = GroupTestUtil.addGroup();

		testCompany = CompanyLocalServiceUtil.getCompany(
			testGroup.getCompanyId());

		_appUserResource.setContextCompany(testCompany);

		AppUserResource.Builder builder = AppUserResource.builder();

		appUserResource = builder.authentication(
			"test@liferay.com", "test"
		).locale(
			LocaleUtil.getDefault()
		).build();
	}

	@After
	public void tearDown() throws Exception {
		GroupTestUtil.deleteGroup(irrelevantGroup);
		GroupTestUtil.deleteGroup(testGroup);
	}

	@Test
	public void testClientSerDesToDTO() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper() {
			{
				configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
				configure(
					SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
				enable(SerializationFeature.INDENT_OUTPUT);
				setDateFormat(new ISO8601DateFormat());
				setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
				setSerializationInclusion(JsonInclude.Include.NON_NULL);
				setVisibility(
					PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
				setVisibility(
					PropertyAccessor.GETTER, JsonAutoDetect.Visibility.NONE);
			}
		};

		AppUser appUser1 = randomAppUser();

		String json = objectMapper.writeValueAsString(appUser1);

		AppUser appUser2 = AppUserSerDes.toDTO(json);

		Assert.assertTrue(equals(appUser1, appUser2));
	}

	@Test
	public void testClientSerDesToJSON() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper() {
			{
				configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
				configure(
					SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
				setDateFormat(new ISO8601DateFormat());
				setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
				setSerializationInclusion(JsonInclude.Include.NON_NULL);
				setVisibility(
					PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
				setVisibility(
					PropertyAccessor.GETTER, JsonAutoDetect.Visibility.NONE);
			}
		};

		AppUser appUser = randomAppUser();

		String json1 = objectMapper.writeValueAsString(appUser);
		String json2 = AppUserSerDes.toJSON(appUser);

		Assert.assertEquals(
			objectMapper.readTree(json1), objectMapper.readTree(json2));
	}

	@Test
	public void testEscapeRegexInStringFields() throws Exception {
		String regex = "^[0-9]+(\\.[0-9]{1,2})\"?";

		AppUser appUser = randomAppUser();

		appUser.setAndroidDeviceToken(regex);
		appUser.setAppleUserId(regex);
		appUser.setConfirmPassword(regex);
		appUser.setDeactivateReason(regex);
		appUser.setDeleteReason(regex);
		appUser.setEmailAddress(regex);
		appUser.setFacebookId(regex);
		appUser.setFullName(regex);
		appUser.setGcpToken(regex);
		appUser.setGoogleUserId(regex);
		appUser.setIosDeviceToken(regex);
		appUser.setLanguageId(regex);
		appUser.setNewPassword(regex);
		appUser.setPasscode(regex);
		appUser.setPassword(regex);
		appUser.setPasswordResetToken(regex);
		appUser.setPhoneNumber(regex);
		appUser.setProfileBannerImage(regex);
		appUser.setProfileImage(regex);

		String json = AppUserSerDes.toJSON(appUser);

		Assert.assertFalse(json.contains(regex));

		appUser = AppUserSerDes.toDTO(json);

		Assert.assertEquals(regex, appUser.getAndroidDeviceToken());
		Assert.assertEquals(regex, appUser.getAppleUserId());
		Assert.assertEquals(regex, appUser.getConfirmPassword());
		Assert.assertEquals(regex, appUser.getDeactivateReason());
		Assert.assertEquals(regex, appUser.getDeleteReason());
		Assert.assertEquals(regex, appUser.getEmailAddress());
		Assert.assertEquals(regex, appUser.getFacebookId());
		Assert.assertEquals(regex, appUser.getFullName());
		Assert.assertEquals(regex, appUser.getGcpToken());
		Assert.assertEquals(regex, appUser.getGoogleUserId());
		Assert.assertEquals(regex, appUser.getIosDeviceToken());
		Assert.assertEquals(regex, appUser.getLanguageId());
		Assert.assertEquals(regex, appUser.getNewPassword());
		Assert.assertEquals(regex, appUser.getPasscode());
		Assert.assertEquals(regex, appUser.getPassword());
		Assert.assertEquals(regex, appUser.getPasswordResetToken());
		Assert.assertEquals(regex, appUser.getPhoneNumber());
		Assert.assertEquals(regex, appUser.getProfileBannerImage());
		Assert.assertEquals(regex, appUser.getProfileImage());
	}

	@Test
	public void testGetProfileDetails() throws Exception {
		Assert.assertTrue(false);
	}

	@Test
	public void testGraphQLGetProfileDetails() throws Exception {
		Assert.assertTrue(true);
	}

	@Test
	public void testGraphQLGetProfileDetailsNotFound() throws Exception {
		Assert.assertTrue(true);
	}

	@Test
	public void testPostRegister() throws Exception {
		AppUser randomAppUser = randomAppUser();

		AppUser postAppUser = testPostRegister_addAppUser(randomAppUser);

		assertEquals(randomAppUser, postAppUser);
		assertValid(postAppUser);
	}

	protected AppUser testPostRegister_addAppUser(AppUser appUser)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	@Test
	public void testUpdateRegister() throws Exception {
		AppUser randomAppUser = randomAppUser();

		AppUser postAppUser = testUpdateRegister_addAppUser(randomAppUser);

		assertEquals(randomAppUser, postAppUser);
		assertValid(postAppUser);
	}

	protected AppUser testUpdateRegister_addAppUser(AppUser appUser)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	@Test
	public void testVerifyRegistrationOTP() throws Exception {
		AppUser randomAppUser = randomAppUser();

		AppUser postAppUser = testVerifyRegistrationOTP_addAppUser(
			randomAppUser);

		assertEquals(randomAppUser, postAppUser);
		assertValid(postAppUser);
	}

	protected AppUser testVerifyRegistrationOTP_addAppUser(AppUser appUser)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	@Test
	public void testActivateVerifiedMobileOTPUser() throws Exception {
		AppUser randomAppUser = randomAppUser();

		AppUser postAppUser = testActivateVerifiedMobileOTPUser_addAppUser(
			randomAppUser);

		assertEquals(randomAppUser, postAppUser);
		assertValid(postAppUser);
	}

	protected AppUser testActivateVerifiedMobileOTPUser_addAppUser(
			AppUser appUser)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	@Test
	public void testEditUserProfile() throws Exception {
		AppUser randomAppUser = randomAppUser();

		Map<String, File> multipartFiles = getMultipartFiles();

		AppUser postAppUser = testEditUserProfile_addAppUser(
			randomAppUser, multipartFiles);

		assertEquals(randomAppUser, postAppUser);
		assertValid(postAppUser);

		assertValid(postAppUser, multipartFiles);
	}

	protected AppUser testEditUserProfile_addAppUser(
			AppUser appUser, Map<String, File> multipartFiles)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	@Test
	public void testUpdateUserProfileBannerImage() throws Exception {
		AppUser randomAppUser = randomAppUser();

		Map<String, File> multipartFiles = getMultipartFiles();

		AppUser postAppUser = testUpdateUserProfileBannerImage_addAppUser(
			randomAppUser, multipartFiles);

		assertEquals(randomAppUser, postAppUser);
		assertValid(postAppUser);

		assertValid(postAppUser, multipartFiles);
	}

	protected AppUser testUpdateUserProfileBannerImage_addAppUser(
			AppUser appUser, Map<String, File> multipartFiles)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	@Test
	public void testDeleteUserProfileImage() throws Exception {
		AppUser randomAppUser = randomAppUser();

		AppUser postAppUser = testDeleteUserProfileImage_addAppUser(
			randomAppUser);

		assertEquals(randomAppUser, postAppUser);
		assertValid(postAppUser);
	}

	protected AppUser testDeleteUserProfileImage_addAppUser(AppUser appUser)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	@Test
	public void testPostLogin() throws Exception {
		AppUser randomAppUser = randomAppUser();

		AppUser postAppUser = testPostLogin_addAppUser(randomAppUser);

		assertEquals(randomAppUser, postAppUser);
		assertValid(postAppUser);
	}

	protected AppUser testPostLogin_addAppUser(AppUser appUser)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	@Test
	public void testPostSocialLogin() throws Exception {
		AppUser randomAppUser = randomAppUser();

		AppUser postAppUser = testPostSocialLogin_addAppUser(randomAppUser);

		assertEquals(randomAppUser, postAppUser);
		assertValid(postAppUser);
	}

	protected AppUser testPostSocialLogin_addAppUser(AppUser appUser)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	@Test
	public void testResetPassword() throws Exception {
		AppUser randomAppUser = randomAppUser();

		AppUser postAppUser = testResetPassword_addAppUser(randomAppUser);

		assertEquals(randomAppUser, postAppUser);
		assertValid(postAppUser);
	}

	protected AppUser testResetPassword_addAppUser(AppUser appUser)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	@Test
	public void testChangePassword() throws Exception {
		AppUser randomAppUser = randomAppUser();

		AppUser postAppUser = testChangePassword_addAppUser(randomAppUser);

		assertEquals(randomAppUser, postAppUser);
		assertValid(postAppUser);
	}

	protected AppUser testChangePassword_addAppUser(AppUser appUser)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	@Test
	public void testCheckPassword() throws Exception {
		AppUser randomAppUser = randomAppUser();

		AppUser postAppUser = testCheckPassword_addAppUser(randomAppUser);

		assertEquals(randomAppUser, postAppUser);
		assertValid(postAppUser);
	}

	protected AppUser testCheckPassword_addAppUser(AppUser appUser)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	@Test
	public void testForgotPassword() throws Exception {
		AppUser randomAppUser = randomAppUser();

		AppUser postAppUser = testForgotPassword_addAppUser(randomAppUser);

		assertEquals(randomAppUser, postAppUser);
		assertValid(postAppUser);
	}

	protected AppUser testForgotPassword_addAppUser(AppUser appUser)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	@Test
	public void testForgotPasswordPhone() throws Exception {
		AppUser randomAppUser = randomAppUser();

		AppUser postAppUser = testForgotPasswordPhone_addAppUser(randomAppUser);

		assertEquals(randomAppUser, postAppUser);
		assertValid(postAppUser);
	}

	protected AppUser testForgotPasswordPhone_addAppUser(AppUser appUser)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	@Test
	public void testSetEmailAddressVerified() throws Exception {
		AppUser randomAppUser = randomAppUser();

		AppUser postAppUser = testSetEmailAddressVerified_addAppUser(
			randomAppUser);

		assertEquals(randomAppUser, postAppUser);
		assertValid(postAppUser);
	}

	protected AppUser testSetEmailAddressVerified_addAppUser(AppUser appUser)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	@Test
	public void testSetPhoneNumberVerified() throws Exception {
		AppUser randomAppUser = randomAppUser();

		AppUser postAppUser = testSetPhoneNumberVerified_addAppUser(
			randomAppUser);

		assertEquals(randomAppUser, postAppUser);
		assertValid(postAppUser);
	}

	protected AppUser testSetPhoneNumberVerified_addAppUser(AppUser appUser)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	@Test
	public void testUpdateUserAccountDetails() throws Exception {
		AppUser randomAppUser = randomAppUser();

		AppUser postAppUser = testUpdateUserAccountDetails_addAppUser(
			randomAppUser);

		assertEquals(randomAppUser, postAppUser);
		assertValid(postAppUser);
	}

	protected AppUser testUpdateUserAccountDetails_addAppUser(AppUser appUser)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	@Test
	public void testResendOTP() throws Exception {
		AppUser randomAppUser = randomAppUser();

		AppUser postAppUser = testResendOTP_addAppUser(randomAppUser);

		assertEquals(randomAppUser, postAppUser);
		assertValid(postAppUser);
	}

	protected AppUser testResendOTP_addAppUser(AppUser appUser)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	@Test
	public void testVerifyOTP() throws Exception {
		AppUser randomAppUser = randomAppUser();

		AppUser postAppUser = testVerifyOTP_addAppUser(randomAppUser);

		assertEquals(randomAppUser, postAppUser);
		assertValid(postAppUser);
	}

	protected AppUser testVerifyOTP_addAppUser(AppUser appUser)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	@Test
	public void testAddDevice() throws Exception {
		AppUser randomAppUser = randomAppUser();

		AppUser postAppUser = testAddDevice_addAppUser(randomAppUser);

		assertEquals(randomAppUser, postAppUser);
		assertValid(postAppUser);
	}

	protected AppUser testAddDevice_addAppUser(AppUser appUser)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	@Test
	public void testDeleteAccount() throws Exception {
		Assert.assertTrue(false);
	}

	protected void assertContains(AppUser appUser, List<AppUser> appUsers) {
		boolean contains = false;

		for (AppUser item : appUsers) {
			if (equals(appUser, item)) {
				contains = true;

				break;
			}
		}

		Assert.assertTrue(appUsers + " does not contain " + appUser, contains);
	}

	protected void assertHttpResponseStatusCode(
		int expectedHttpResponseStatusCode,
		HttpInvoker.HttpResponse actualHttpResponse) {

		Assert.assertEquals(
			expectedHttpResponseStatusCode, actualHttpResponse.getStatusCode());
	}

	protected void assertEquals(AppUser appUser1, AppUser appUser2) {
		Assert.assertTrue(
			appUser1 + " does not equal " + appUser2,
			equals(appUser1, appUser2));
	}

	protected void assertEquals(
		List<AppUser> appUsers1, List<AppUser> appUsers2) {

		Assert.assertEquals(appUsers1.size(), appUsers2.size());

		for (int i = 0; i < appUsers1.size(); i++) {
			AppUser appUser1 = appUsers1.get(i);
			AppUser appUser2 = appUsers2.get(i);

			assertEquals(appUser1, appUser2);
		}
	}

	protected void assertEqualsIgnoringOrder(
		List<AppUser> appUsers1, List<AppUser> appUsers2) {

		Assert.assertEquals(appUsers1.size(), appUsers2.size());

		for (AppUser appUser1 : appUsers1) {
			boolean contains = false;

			for (AppUser appUser2 : appUsers2) {
				if (equals(appUser1, appUser2)) {
					contains = true;

					break;
				}
			}

			Assert.assertTrue(
				appUsers2 + " does not contain " + appUser1, contains);
		}
	}

	protected void assertValid(AppUser appUser) throws Exception {
		boolean valid = true;

		for (String additionalAssertFieldName :
				getAdditionalAssertFieldNames()) {

			if (Objects.equals(
					"androidDeviceToken", additionalAssertFieldName)) {

				if (appUser.getAndroidDeviceToken() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("appUserId", additionalAssertFieldName)) {
				if (appUser.getAppUserId() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("appleUserId", additionalAssertFieldName)) {
				if (appUser.getAppleUserId() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("autoLogin", additionalAssertFieldName)) {
				if (appUser.getAutoLogin() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("confirmPassword", additionalAssertFieldName)) {
				if (appUser.getConfirmPassword() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("deactivateReason", additionalAssertFieldName)) {
				if (appUser.getDeactivateReason() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("deleteReason", additionalAssertFieldName)) {
				if (appUser.getDeleteReason() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("emailAddress", additionalAssertFieldName)) {
				if (appUser.getEmailAddress() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals(
					"emailAddressVerified", additionalAssertFieldName)) {

				if (appUser.getEmailAddressVerified() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("facebookId", additionalAssertFieldName)) {
				if (appUser.getFacebookId() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("fullName", additionalAssertFieldName)) {
				if (appUser.getFullName() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("gcpToken", additionalAssertFieldName)) {
				if (appUser.getGcpToken() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("googleUserId", additionalAssertFieldName)) {
				if (appUser.getGoogleUserId() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("iosDeviceToken", additionalAssertFieldName)) {
				if (appUser.getIosDeviceToken() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("isVerified", additionalAssertFieldName)) {
				if (appUser.getIsVerified() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("languageId", additionalAssertFieldName)) {
				if (appUser.getLanguageId() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("newPassword", additionalAssertFieldName)) {
				if (appUser.getNewPassword() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("passcode", additionalAssertFieldName)) {
				if (appUser.getPasscode() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("password", additionalAssertFieldName)) {
				if (appUser.getPassword() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals(
					"passwordResetToken", additionalAssertFieldName)) {

				if (appUser.getPasswordResetToken() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("phoneNumber", additionalAssertFieldName)) {
				if (appUser.getPhoneNumber() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals(
					"phoneNumberVerified", additionalAssertFieldName)) {

				if (appUser.getPhoneNumberVerified() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals(
					"profileBannerImage", additionalAssertFieldName)) {

				if (appUser.getProfileBannerImage() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("profileImage", additionalAssertFieldName)) {
				if (appUser.getProfileImage() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("requestStatus", additionalAssertFieldName)) {
				if (appUser.getRequestStatus() == null) {
					valid = false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		Assert.assertTrue(valid);
	}

	protected void assertValid(
			AppUser appUser, Map<String, File> multipartFiles)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	protected void assertValid(Page<AppUser> page) {
		boolean valid = false;

		java.util.Collection<AppUser> appUsers = page.getItems();

		int size = appUsers.size();

		if ((page.getLastPage() > 0) && (page.getPage() > 0) &&
			(page.getPageSize() > 0) && (page.getTotalCount() > 0) &&
			(size > 0)) {

			valid = true;
		}

		Assert.assertTrue(valid);
	}

	protected String[] getAdditionalAssertFieldNames() {
		return new String[0];
	}

	protected List<GraphQLField> getGraphQLFields() throws Exception {
		List<GraphQLField> graphQLFields = new ArrayList<>();

		for (Field field :
				getDeclaredFields(misk.headless.dto.v1_0.AppUser.class)) {

			if (!ArrayUtil.contains(
					getAdditionalAssertFieldNames(), field.getName())) {

				continue;
			}

			graphQLFields.addAll(getGraphQLFields(field));
		}

		return graphQLFields;
	}

	protected List<GraphQLField> getGraphQLFields(Field... fields)
		throws Exception {

		List<GraphQLField> graphQLFields = new ArrayList<>();

		for (Field field : fields) {
			com.liferay.portal.vulcan.graphql.annotation.GraphQLField
				vulcanGraphQLField = field.getAnnotation(
					com.liferay.portal.vulcan.graphql.annotation.GraphQLField.
						class);

			if (vulcanGraphQLField != null) {
				Class<?> clazz = field.getType();

				if (clazz.isArray()) {
					clazz = clazz.getComponentType();
				}

				List<GraphQLField> childrenGraphQLFields = getGraphQLFields(
					getDeclaredFields(clazz));

				graphQLFields.add(
					new GraphQLField(field.getName(), childrenGraphQLFields));
			}
		}

		return graphQLFields;
	}

	protected String[] getIgnoredEntityFieldNames() {
		return new String[0];
	}

	protected boolean equals(AppUser appUser1, AppUser appUser2) {
		if (appUser1 == appUser2) {
			return true;
		}

		for (String additionalAssertFieldName :
				getAdditionalAssertFieldNames()) {

			if (Objects.equals(
					"androidDeviceToken", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						appUser1.getAndroidDeviceToken(),
						appUser2.getAndroidDeviceToken())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("appUserId", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getAppUserId(), appUser2.getAppUserId())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("appleUserId", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getAppleUserId(), appUser2.getAppleUserId())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("autoLogin", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getAutoLogin(), appUser2.getAutoLogin())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("confirmPassword", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getConfirmPassword(),
						appUser2.getConfirmPassword())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("deactivateReason", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getDeactivateReason(),
						appUser2.getDeactivateReason())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("deleteReason", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getDeleteReason(),
						appUser2.getDeleteReason())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("emailAddress", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getEmailAddress(),
						appUser2.getEmailAddress())) {

					return false;
				}

				continue;
			}

			if (Objects.equals(
					"emailAddressVerified", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						appUser1.getEmailAddressVerified(),
						appUser2.getEmailAddressVerified())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("facebookId", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getFacebookId(), appUser2.getFacebookId())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("fullName", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getFullName(), appUser2.getFullName())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("gcpToken", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getGcpToken(), appUser2.getGcpToken())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("googleUserId", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getGoogleUserId(),
						appUser2.getGoogleUserId())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("iosDeviceToken", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getIosDeviceToken(),
						appUser2.getIosDeviceToken())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("isVerified", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getIsVerified(), appUser2.getIsVerified())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("languageId", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getLanguageId(), appUser2.getLanguageId())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("newPassword", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getNewPassword(), appUser2.getNewPassword())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("passcode", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getPasscode(), appUser2.getPasscode())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("password", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getPassword(), appUser2.getPassword())) {

					return false;
				}

				continue;
			}

			if (Objects.equals(
					"passwordResetToken", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						appUser1.getPasswordResetToken(),
						appUser2.getPasswordResetToken())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("phoneNumber", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getPhoneNumber(), appUser2.getPhoneNumber())) {

					return false;
				}

				continue;
			}

			if (Objects.equals(
					"phoneNumberVerified", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						appUser1.getPhoneNumberVerified(),
						appUser2.getPhoneNumberVerified())) {

					return false;
				}

				continue;
			}

			if (Objects.equals(
					"profileBannerImage", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						appUser1.getProfileBannerImage(),
						appUser2.getProfileBannerImage())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("profileImage", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getProfileImage(),
						appUser2.getProfileImage())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("requestStatus", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						appUser1.getRequestStatus(),
						appUser2.getRequestStatus())) {

					return false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		return true;
	}

	protected boolean equals(
		Map<String, Object> map1, Map<String, Object> map2) {

		if (Objects.equals(map1.keySet(), map2.keySet())) {
			for (Map.Entry<String, Object> entry : map1.entrySet()) {
				if (entry.getValue() instanceof Map) {
					if (!equals(
							(Map)entry.getValue(),
							(Map)map2.get(entry.getKey()))) {

						return false;
					}
				}
				else if (!Objects.deepEquals(
							entry.getValue(), map2.get(entry.getKey()))) {

					return false;
				}
			}

			return true;
		}

		return false;
	}

	protected Field[] getDeclaredFields(Class clazz) throws Exception {
		Stream<Field> stream = Stream.of(
			ReflectionUtil.getDeclaredFields(clazz));

		return stream.filter(
			field -> !field.isSynthetic()
		).toArray(
			Field[]::new
		);
	}

	protected java.util.Collection<EntityField> getEntityFields()
		throws Exception {

		if (!(_appUserResource instanceof EntityModelResource)) {
			throw new UnsupportedOperationException(
				"Resource is not an instance of EntityModelResource");
		}

		EntityModelResource entityModelResource =
			(EntityModelResource)_appUserResource;

		EntityModel entityModel = entityModelResource.getEntityModel(
			new MultivaluedHashMap());

		Map<String, EntityField> entityFieldsMap =
			entityModel.getEntityFieldsMap();

		return entityFieldsMap.values();
	}

	protected List<EntityField> getEntityFields(EntityField.Type type)
		throws Exception {

		java.util.Collection<EntityField> entityFields = getEntityFields();

		Stream<EntityField> stream = entityFields.stream();

		return stream.filter(
			entityField ->
				Objects.equals(entityField.getType(), type) &&
				!ArrayUtil.contains(
					getIgnoredEntityFieldNames(), entityField.getName())
		).collect(
			Collectors.toList()
		);
	}

	protected String getFilterString(
		EntityField entityField, String operator, AppUser appUser) {

		StringBundler sb = new StringBundler();

		String entityFieldName = entityField.getName();

		sb.append(entityFieldName);

		sb.append(" ");
		sb.append(operator);
		sb.append(" ");

		if (entityFieldName.equals("androidDeviceToken")) {
			sb.append("'");
			sb.append(String.valueOf(appUser.getAndroidDeviceToken()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("appUserId")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("appleUserId")) {
			sb.append("'");
			sb.append(String.valueOf(appUser.getAppleUserId()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("autoLogin")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("confirmPassword")) {
			sb.append("'");
			sb.append(String.valueOf(appUser.getConfirmPassword()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("deactivateReason")) {
			sb.append("'");
			sb.append(String.valueOf(appUser.getDeactivateReason()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("deleteReason")) {
			sb.append("'");
			sb.append(String.valueOf(appUser.getDeleteReason()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("emailAddress")) {
			sb.append("'");
			sb.append(String.valueOf(appUser.getEmailAddress()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("emailAddressVerified")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("facebookId")) {
			sb.append("'");
			sb.append(String.valueOf(appUser.getFacebookId()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("fullName")) {
			sb.append("'");
			sb.append(String.valueOf(appUser.getFullName()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("gcpToken")) {
			sb.append("'");
			sb.append(String.valueOf(appUser.getGcpToken()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("googleUserId")) {
			sb.append("'");
			sb.append(String.valueOf(appUser.getGoogleUserId()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("iosDeviceToken")) {
			sb.append("'");
			sb.append(String.valueOf(appUser.getIosDeviceToken()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("isVerified")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("languageId")) {
			sb.append("'");
			sb.append(String.valueOf(appUser.getLanguageId()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("newPassword")) {
			sb.append("'");
			sb.append(String.valueOf(appUser.getNewPassword()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("passcode")) {
			sb.append("'");
			sb.append(String.valueOf(appUser.getPasscode()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("password")) {
			sb.append("'");
			sb.append(String.valueOf(appUser.getPassword()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("passwordResetToken")) {
			sb.append("'");
			sb.append(String.valueOf(appUser.getPasswordResetToken()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("phoneNumber")) {
			sb.append("'");
			sb.append(String.valueOf(appUser.getPhoneNumber()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("phoneNumberVerified")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("profileBannerImage")) {
			sb.append("'");
			sb.append(String.valueOf(appUser.getProfileBannerImage()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("profileImage")) {
			sb.append("'");
			sb.append(String.valueOf(appUser.getProfileImage()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("requestStatus")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		throw new IllegalArgumentException(
			"Invalid entity field " + entityFieldName);
	}

	protected Map<String, File> getMultipartFiles() throws Exception {
		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	protected String invoke(String query) throws Exception {
		HttpInvoker httpInvoker = HttpInvoker.newHttpInvoker();

		httpInvoker.body(
			JSONUtil.put(
				"query", query
			).toString(),
			"application/json");
		httpInvoker.httpMethod(HttpInvoker.HttpMethod.POST);
		httpInvoker.path("http://localhost:8080/o/graphql");
		httpInvoker.userNameAndPassword("test@liferay.com:test");

		HttpInvoker.HttpResponse httpResponse = httpInvoker.invoke();

		return httpResponse.getContent();
	}

	protected JSONObject invokeGraphQLMutation(GraphQLField graphQLField)
		throws Exception {

		GraphQLField mutationGraphQLField = new GraphQLField(
			"mutation", graphQLField);

		return JSONFactoryUtil.createJSONObject(
			invoke(mutationGraphQLField.toString()));
	}

	protected JSONObject invokeGraphQLQuery(GraphQLField graphQLField)
		throws Exception {

		GraphQLField queryGraphQLField = new GraphQLField(
			"query", graphQLField);

		return JSONFactoryUtil.createJSONObject(
			invoke(queryGraphQLField.toString()));
	}

	protected AppUser randomAppUser() throws Exception {
		return new AppUser() {
			{
				androidDeviceToken = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				appUserId = RandomTestUtil.randomLong();
				appleUserId = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				autoLogin = RandomTestUtil.randomBoolean();
				confirmPassword = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				deactivateReason = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				deleteReason = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				emailAddress =
					StringUtil.toLowerCase(RandomTestUtil.randomString()) +
						"@liferay.com";
				emailAddressVerified = RandomTestUtil.randomBoolean();
				facebookId = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				fullName = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				gcpToken = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				googleUserId = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				iosDeviceToken = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				isVerified = RandomTestUtil.randomBoolean();
				languageId = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				newPassword = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				passcode = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				password = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				passwordResetToken = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				phoneNumber = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				phoneNumberVerified = RandomTestUtil.randomBoolean();
				profileBannerImage = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				profileImage = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
			}
		};
	}

	protected AppUser randomIrrelevantAppUser() throws Exception {
		AppUser randomIrrelevantAppUser = randomAppUser();

		return randomIrrelevantAppUser;
	}

	protected AppUser randomPatchAppUser() throws Exception {
		return randomAppUser();
	}

	protected AppUserResource appUserResource;
	protected Group irrelevantGroup;
	protected Company testCompany;
	protected Group testGroup;

	protected class GraphQLField {

		public GraphQLField(String key, GraphQLField... graphQLFields) {
			this(key, new HashMap<>(), graphQLFields);
		}

		public GraphQLField(String key, List<GraphQLField> graphQLFields) {
			this(key, new HashMap<>(), graphQLFields);
		}

		public GraphQLField(
			String key, Map<String, Object> parameterMap,
			GraphQLField... graphQLFields) {

			_key = key;
			_parameterMap = parameterMap;
			_graphQLFields = Arrays.asList(graphQLFields);
		}

		public GraphQLField(
			String key, Map<String, Object> parameterMap,
			List<GraphQLField> graphQLFields) {

			_key = key;
			_parameterMap = parameterMap;
			_graphQLFields = graphQLFields;
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder(_key);

			if (!_parameterMap.isEmpty()) {
				sb.append("(");

				for (Map.Entry<String, Object> entry :
						_parameterMap.entrySet()) {

					sb.append(entry.getKey());
					sb.append(": ");
					sb.append(entry.getValue());
					sb.append(", ");
				}

				sb.setLength(sb.length() - 2);

				sb.append(")");
			}

			if (!_graphQLFields.isEmpty()) {
				sb.append("{");

				for (GraphQLField graphQLField : _graphQLFields) {
					sb.append(graphQLField.toString());
					sb.append(", ");
				}

				sb.setLength(sb.length() - 2);

				sb.append("}");
			}

			return sb.toString();
		}

		private final List<GraphQLField> _graphQLFields;
		private final String _key;
		private final Map<String, Object> _parameterMap;

	}

	private static final com.liferay.portal.kernel.log.Log _log =
		LogFactoryUtil.getLog(BaseAppUserResourceTestCase.class);

	private static BeanUtilsBean _beanUtilsBean = new BeanUtilsBean() {

		@Override
		public void copyProperty(Object bean, String name, Object value)
			throws IllegalAccessException, InvocationTargetException {

			if (value != null) {
				super.copyProperty(bean, name, value);
			}
		}

	};
	private static DateFormat _dateFormat;

	@Inject
	private misk.headless.resource.v1_0.AppUserResource _appUserResource;

}