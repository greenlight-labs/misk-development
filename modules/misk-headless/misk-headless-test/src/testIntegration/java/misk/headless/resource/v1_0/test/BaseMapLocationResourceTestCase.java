package misk.headless.resource.v1_0.test;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;

import com.liferay.petra.reflect.ReflectionUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.json.JSONUtil;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.CompanyLocalServiceUtil;
import com.liferay.portal.kernel.test.util.GroupTestUtil;
import com.liferay.portal.kernel.test.util.RandomTestUtil;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.DateFormatFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.odata.entity.EntityField;
import com.liferay.portal.odata.entity.EntityModel;
import com.liferay.portal.test.rule.Inject;
import com.liferay.portal.test.rule.LiferayIntegrationTestRule;
import com.liferay.portal.vulcan.resource.EntityModelResource;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import java.text.DateFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Generated;

import javax.ws.rs.core.MultivaluedHashMap;

import misk.headless.client.dto.v1_0.MapLocation;
import misk.headless.client.http.HttpInvoker;
import misk.headless.client.pagination.Page;
import misk.headless.client.pagination.Pagination;
import misk.headless.client.resource.v1_0.MapLocationResource;
import misk.headless.client.serdes.v1_0.MapLocationSerDes;

import org.apache.commons.beanutils.BeanUtilsBean;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public abstract class BaseMapLocationResourceTestCase {

	@ClassRule
	@Rule
	public static final LiferayIntegrationTestRule liferayIntegrationTestRule =
		new LiferayIntegrationTestRule();

	@BeforeClass
	public static void setUpClass() throws Exception {
		_dateFormat = DateFormatFactoryUtil.getSimpleDateFormat(
			"yyyy-MM-dd'T'HH:mm:ss'Z'");
	}

	@Before
	public void setUp() throws Exception {
		irrelevantGroup = GroupTestUtil.addGroup();
		testGroup = GroupTestUtil.addGroup();

		testCompany = CompanyLocalServiceUtil.getCompany(
			testGroup.getCompanyId());

		_mapLocationResource.setContextCompany(testCompany);

		MapLocationResource.Builder builder = MapLocationResource.builder();

		mapLocationResource = builder.authentication(
			"test@liferay.com", "test"
		).locale(
			LocaleUtil.getDefault()
		).build();
	}

	@After
	public void tearDown() throws Exception {
		GroupTestUtil.deleteGroup(irrelevantGroup);
		GroupTestUtil.deleteGroup(testGroup);
	}

	@Test
	public void testClientSerDesToDTO() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper() {
			{
				configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
				configure(
					SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
				enable(SerializationFeature.INDENT_OUTPUT);
				setDateFormat(new ISO8601DateFormat());
				setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
				setSerializationInclusion(JsonInclude.Include.NON_NULL);
				setVisibility(
					PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
				setVisibility(
					PropertyAccessor.GETTER, JsonAutoDetect.Visibility.NONE);
			}
		};

		MapLocation mapLocation1 = randomMapLocation();

		String json = objectMapper.writeValueAsString(mapLocation1);

		MapLocation mapLocation2 = MapLocationSerDes.toDTO(json);

		Assert.assertTrue(equals(mapLocation1, mapLocation2));
	}

	@Test
	public void testClientSerDesToJSON() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper() {
			{
				configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
				configure(
					SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
				setDateFormat(new ISO8601DateFormat());
				setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
				setSerializationInclusion(JsonInclude.Include.NON_NULL);
				setVisibility(
					PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
				setVisibility(
					PropertyAccessor.GETTER, JsonAutoDetect.Visibility.NONE);
			}
		};

		MapLocation mapLocation = randomMapLocation();

		String json1 = objectMapper.writeValueAsString(mapLocation);
		String json2 = MapLocationSerDes.toJSON(mapLocation);

		Assert.assertEquals(
			objectMapper.readTree(json1), objectMapper.readTree(json2));
	}

	@Test
	public void testEscapeRegexInStringFields() throws Exception {
		String regex = "^[0-9]+(\\.[0-9]{1,2})\"?";

		MapLocation mapLocation = randomMapLocation();

		mapLocation.setCallUs(regex);
		mapLocation.setEmail(regex);
		mapLocation.setLatitude(regex);
		mapLocation.setLongitude(regex);
		mapLocation.setTitle(regex);
		mapLocation.setWhatsapp(regex);

		String json = MapLocationSerDes.toJSON(mapLocation);

		Assert.assertFalse(json.contains(regex));

		mapLocation = MapLocationSerDes.toDTO(json);

		Assert.assertEquals(regex, mapLocation.getCallUs());
		Assert.assertEquals(regex, mapLocation.getEmail());
		Assert.assertEquals(regex, mapLocation.getLatitude());
		Assert.assertEquals(regex, mapLocation.getLongitude());
		Assert.assertEquals(regex, mapLocation.getTitle());
		Assert.assertEquals(regex, mapLocation.getWhatsapp());
	}

	@Test
	public void testGetMapLocations() throws Exception {
		Page<MapLocation> page = mapLocationResource.getMapLocations(
			RandomTestUtil.randomString(), Pagination.of(1, 10));

		long totalCount = page.getTotalCount();

		MapLocation mapLocation1 = testGetMapLocations_addMapLocation(
			randomMapLocation());

		MapLocation mapLocation2 = testGetMapLocations_addMapLocation(
			randomMapLocation());

		page = mapLocationResource.getMapLocations(null, Pagination.of(1, 10));

		Assert.assertEquals(totalCount + 2, page.getTotalCount());

		assertContains(mapLocation1, (List<MapLocation>)page.getItems());
		assertContains(mapLocation2, (List<MapLocation>)page.getItems());
		assertValid(page);
	}

	@Test
	public void testGetMapLocationsWithPagination() throws Exception {
		Page<MapLocation> totalPage = mapLocationResource.getMapLocations(
			null, null);

		int totalCount = GetterUtil.getInteger(totalPage.getTotalCount());

		MapLocation mapLocation1 = testGetMapLocations_addMapLocation(
			randomMapLocation());

		MapLocation mapLocation2 = testGetMapLocations_addMapLocation(
			randomMapLocation());

		MapLocation mapLocation3 = testGetMapLocations_addMapLocation(
			randomMapLocation());

		Page<MapLocation> page1 = mapLocationResource.getMapLocations(
			null, Pagination.of(1, totalCount + 2));

		List<MapLocation> mapLocations1 = (List<MapLocation>)page1.getItems();

		Assert.assertEquals(
			mapLocations1.toString(), totalCount + 2, mapLocations1.size());

		Page<MapLocation> page2 = mapLocationResource.getMapLocations(
			null, Pagination.of(2, totalCount + 2));

		Assert.assertEquals(totalCount + 3, page2.getTotalCount());

		List<MapLocation> mapLocations2 = (List<MapLocation>)page2.getItems();

		Assert.assertEquals(mapLocations2.toString(), 1, mapLocations2.size());

		Page<MapLocation> page3 = mapLocationResource.getMapLocations(
			null, Pagination.of(1, totalCount + 3));

		assertContains(mapLocation1, (List<MapLocation>)page3.getItems());
		assertContains(mapLocation2, (List<MapLocation>)page3.getItems());
		assertContains(mapLocation3, (List<MapLocation>)page3.getItems());
	}

	protected MapLocation testGetMapLocations_addMapLocation(
			MapLocation mapLocation)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	@Test
	public void testGraphQLGetMapLocations() throws Exception {
		Assert.assertTrue(false);
	}

	protected void assertContains(
		MapLocation mapLocation, List<MapLocation> mapLocations) {

		boolean contains = false;

		for (MapLocation item : mapLocations) {
			if (equals(mapLocation, item)) {
				contains = true;

				break;
			}
		}

		Assert.assertTrue(
			mapLocations + " does not contain " + mapLocation, contains);
	}

	protected void assertHttpResponseStatusCode(
		int expectedHttpResponseStatusCode,
		HttpInvoker.HttpResponse actualHttpResponse) {

		Assert.assertEquals(
			expectedHttpResponseStatusCode, actualHttpResponse.getStatusCode());
	}

	protected void assertEquals(
		MapLocation mapLocation1, MapLocation mapLocation2) {

		Assert.assertTrue(
			mapLocation1 + " does not equal " + mapLocation2,
			equals(mapLocation1, mapLocation2));
	}

	protected void assertEquals(
		List<MapLocation> mapLocations1, List<MapLocation> mapLocations2) {

		Assert.assertEquals(mapLocations1.size(), mapLocations2.size());

		for (int i = 0; i < mapLocations1.size(); i++) {
			MapLocation mapLocation1 = mapLocations1.get(i);
			MapLocation mapLocation2 = mapLocations2.get(i);

			assertEquals(mapLocation1, mapLocation2);
		}
	}

	protected void assertEqualsIgnoringOrder(
		List<MapLocation> mapLocations1, List<MapLocation> mapLocations2) {

		Assert.assertEquals(mapLocations1.size(), mapLocations2.size());

		for (MapLocation mapLocation1 : mapLocations1) {
			boolean contains = false;

			for (MapLocation mapLocation2 : mapLocations2) {
				if (equals(mapLocation1, mapLocation2)) {
					contains = true;

					break;
				}
			}

			Assert.assertTrue(
				mapLocations2 + " does not contain " + mapLocation1, contains);
		}
	}

	protected void assertValid(MapLocation mapLocation) throws Exception {
		boolean valid = true;

		for (String additionalAssertFieldName :
				getAdditionalAssertFieldNames()) {

			if (Objects.equals("callUs", additionalAssertFieldName)) {
				if (mapLocation.getCallUs() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("email", additionalAssertFieldName)) {
				if (mapLocation.getEmail() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("latitude", additionalAssertFieldName)) {
				if (mapLocation.getLatitude() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("longitude", additionalAssertFieldName)) {
				if (mapLocation.getLongitude() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("title", additionalAssertFieldName)) {
				if (mapLocation.getTitle() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("whatsapp", additionalAssertFieldName)) {
				if (mapLocation.getWhatsapp() == null) {
					valid = false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		Assert.assertTrue(valid);
	}

	protected void assertValid(Page<MapLocation> page) {
		boolean valid = false;

		java.util.Collection<MapLocation> mapLocations = page.getItems();

		int size = mapLocations.size();

		if ((page.getLastPage() > 0) && (page.getPage() > 0) &&
			(page.getPageSize() > 0) && (page.getTotalCount() > 0) &&
			(size > 0)) {

			valid = true;
		}

		Assert.assertTrue(valid);
	}

	protected String[] getAdditionalAssertFieldNames() {
		return new String[0];
	}

	protected List<GraphQLField> getGraphQLFields() throws Exception {
		List<GraphQLField> graphQLFields = new ArrayList<>();

		for (Field field :
				getDeclaredFields(misk.headless.dto.v1_0.MapLocation.class)) {

			if (!ArrayUtil.contains(
					getAdditionalAssertFieldNames(), field.getName())) {

				continue;
			}

			graphQLFields.addAll(getGraphQLFields(field));
		}

		return graphQLFields;
	}

	protected List<GraphQLField> getGraphQLFields(Field... fields)
		throws Exception {

		List<GraphQLField> graphQLFields = new ArrayList<>();

		for (Field field : fields) {
			com.liferay.portal.vulcan.graphql.annotation.GraphQLField
				vulcanGraphQLField = field.getAnnotation(
					com.liferay.portal.vulcan.graphql.annotation.GraphQLField.
						class);

			if (vulcanGraphQLField != null) {
				Class<?> clazz = field.getType();

				if (clazz.isArray()) {
					clazz = clazz.getComponentType();
				}

				List<GraphQLField> childrenGraphQLFields = getGraphQLFields(
					getDeclaredFields(clazz));

				graphQLFields.add(
					new GraphQLField(field.getName(), childrenGraphQLFields));
			}
		}

		return graphQLFields;
	}

	protected String[] getIgnoredEntityFieldNames() {
		return new String[0];
	}

	protected boolean equals(
		MapLocation mapLocation1, MapLocation mapLocation2) {

		if (mapLocation1 == mapLocation2) {
			return true;
		}

		for (String additionalAssertFieldName :
				getAdditionalAssertFieldNames()) {

			if (Objects.equals("callUs", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						mapLocation1.getCallUs(), mapLocation2.getCallUs())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("email", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						mapLocation1.getEmail(), mapLocation2.getEmail())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("latitude", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						mapLocation1.getLatitude(),
						mapLocation2.getLatitude())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("longitude", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						mapLocation1.getLongitude(),
						mapLocation2.getLongitude())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("title", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						mapLocation1.getTitle(), mapLocation2.getTitle())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("whatsapp", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						mapLocation1.getWhatsapp(),
						mapLocation2.getWhatsapp())) {

					return false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		return true;
	}

	protected boolean equals(
		Map<String, Object> map1, Map<String, Object> map2) {

		if (Objects.equals(map1.keySet(), map2.keySet())) {
			for (Map.Entry<String, Object> entry : map1.entrySet()) {
				if (entry.getValue() instanceof Map) {
					if (!equals(
							(Map)entry.getValue(),
							(Map)map2.get(entry.getKey()))) {

						return false;
					}
				}
				else if (!Objects.deepEquals(
							entry.getValue(), map2.get(entry.getKey()))) {

					return false;
				}
			}

			return true;
		}

		return false;
	}

	protected Field[] getDeclaredFields(Class clazz) throws Exception {
		Stream<Field> stream = Stream.of(
			ReflectionUtil.getDeclaredFields(clazz));

		return stream.filter(
			field -> !field.isSynthetic()
		).toArray(
			Field[]::new
		);
	}

	protected java.util.Collection<EntityField> getEntityFields()
		throws Exception {

		if (!(_mapLocationResource instanceof EntityModelResource)) {
			throw new UnsupportedOperationException(
				"Resource is not an instance of EntityModelResource");
		}

		EntityModelResource entityModelResource =
			(EntityModelResource)_mapLocationResource;

		EntityModel entityModel = entityModelResource.getEntityModel(
			new MultivaluedHashMap());

		Map<String, EntityField> entityFieldsMap =
			entityModel.getEntityFieldsMap();

		return entityFieldsMap.values();
	}

	protected List<EntityField> getEntityFields(EntityField.Type type)
		throws Exception {

		java.util.Collection<EntityField> entityFields = getEntityFields();

		Stream<EntityField> stream = entityFields.stream();

		return stream.filter(
			entityField ->
				Objects.equals(entityField.getType(), type) &&
				!ArrayUtil.contains(
					getIgnoredEntityFieldNames(), entityField.getName())
		).collect(
			Collectors.toList()
		);
	}

	protected String getFilterString(
		EntityField entityField, String operator, MapLocation mapLocation) {

		StringBundler sb = new StringBundler();

		String entityFieldName = entityField.getName();

		sb.append(entityFieldName);

		sb.append(" ");
		sb.append(operator);
		sb.append(" ");

		if (entityFieldName.equals("callUs")) {
			sb.append("'");
			sb.append(String.valueOf(mapLocation.getCallUs()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("email")) {
			sb.append("'");
			sb.append(String.valueOf(mapLocation.getEmail()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("latitude")) {
			sb.append("'");
			sb.append(String.valueOf(mapLocation.getLatitude()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("longitude")) {
			sb.append("'");
			sb.append(String.valueOf(mapLocation.getLongitude()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("title")) {
			sb.append("'");
			sb.append(String.valueOf(mapLocation.getTitle()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("whatsapp")) {
			sb.append("'");
			sb.append(String.valueOf(mapLocation.getWhatsapp()));
			sb.append("'");

			return sb.toString();
		}

		throw new IllegalArgumentException(
			"Invalid entity field " + entityFieldName);
	}

	protected String invoke(String query) throws Exception {
		HttpInvoker httpInvoker = HttpInvoker.newHttpInvoker();

		httpInvoker.body(
			JSONUtil.put(
				"query", query
			).toString(),
			"application/json");
		httpInvoker.httpMethod(HttpInvoker.HttpMethod.POST);
		httpInvoker.path("http://localhost:8080/o/graphql");
		httpInvoker.userNameAndPassword("test@liferay.com:test");

		HttpInvoker.HttpResponse httpResponse = httpInvoker.invoke();

		return httpResponse.getContent();
	}

	protected JSONObject invokeGraphQLMutation(GraphQLField graphQLField)
		throws Exception {

		GraphQLField mutationGraphQLField = new GraphQLField(
			"mutation", graphQLField);

		return JSONFactoryUtil.createJSONObject(
			invoke(mutationGraphQLField.toString()));
	}

	protected JSONObject invokeGraphQLQuery(GraphQLField graphQLField)
		throws Exception {

		GraphQLField queryGraphQLField = new GraphQLField(
			"query", graphQLField);

		return JSONFactoryUtil.createJSONObject(
			invoke(queryGraphQLField.toString()));
	}

	protected MapLocation randomMapLocation() throws Exception {
		return new MapLocation() {
			{
				callUs = StringUtil.toLowerCase(RandomTestUtil.randomString());
				email =
					StringUtil.toLowerCase(RandomTestUtil.randomString()) +
						"@liferay.com";
				latitude = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				longitude = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				title = StringUtil.toLowerCase(RandomTestUtil.randomString());
				whatsapp = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
			}
		};
	}

	protected MapLocation randomIrrelevantMapLocation() throws Exception {
		MapLocation randomIrrelevantMapLocation = randomMapLocation();

		return randomIrrelevantMapLocation;
	}

	protected MapLocation randomPatchMapLocation() throws Exception {
		return randomMapLocation();
	}

	protected MapLocationResource mapLocationResource;
	protected Group irrelevantGroup;
	protected Company testCompany;
	protected Group testGroup;

	protected class GraphQLField {

		public GraphQLField(String key, GraphQLField... graphQLFields) {
			this(key, new HashMap<>(), graphQLFields);
		}

		public GraphQLField(String key, List<GraphQLField> graphQLFields) {
			this(key, new HashMap<>(), graphQLFields);
		}

		public GraphQLField(
			String key, Map<String, Object> parameterMap,
			GraphQLField... graphQLFields) {

			_key = key;
			_parameterMap = parameterMap;
			_graphQLFields = Arrays.asList(graphQLFields);
		}

		public GraphQLField(
			String key, Map<String, Object> parameterMap,
			List<GraphQLField> graphQLFields) {

			_key = key;
			_parameterMap = parameterMap;
			_graphQLFields = graphQLFields;
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder(_key);

			if (!_parameterMap.isEmpty()) {
				sb.append("(");

				for (Map.Entry<String, Object> entry :
						_parameterMap.entrySet()) {

					sb.append(entry.getKey());
					sb.append(": ");
					sb.append(entry.getValue());
					sb.append(", ");
				}

				sb.setLength(sb.length() - 2);

				sb.append(")");
			}

			if (!_graphQLFields.isEmpty()) {
				sb.append("{");

				for (GraphQLField graphQLField : _graphQLFields) {
					sb.append(graphQLField.toString());
					sb.append(", ");
				}

				sb.setLength(sb.length() - 2);

				sb.append("}");
			}

			return sb.toString();
		}

		private final List<GraphQLField> _graphQLFields;
		private final String _key;
		private final Map<String, Object> _parameterMap;

	}

	private static final com.liferay.portal.kernel.log.Log _log =
		LogFactoryUtil.getLog(BaseMapLocationResourceTestCase.class);

	private static BeanUtilsBean _beanUtilsBean = new BeanUtilsBean() {

		@Override
		public void copyProperty(Object bean, String name, Object value)
			throws IllegalAccessException, InvocationTargetException {

			if (value != null) {
				super.copyProperty(bean, name, value);
			}
		}

	};
	private static DateFormat _dateFormat;

	@Inject
	private misk.headless.resource.v1_0.MapLocationResource
		_mapLocationResource;

}