package misk.headless.resource.v1_0.test;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;

import com.liferay.petra.reflect.ReflectionUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.json.JSONUtil;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.CompanyLocalServiceUtil;
import com.liferay.portal.kernel.test.util.GroupTestUtil;
import com.liferay.portal.kernel.test.util.RandomTestUtil;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.DateFormatFactoryUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.odata.entity.EntityField;
import com.liferay.portal.odata.entity.EntityModel;
import com.liferay.portal.test.rule.Inject;
import com.liferay.portal.test.rule.LiferayIntegrationTestRule;
import com.liferay.portal.vulcan.resource.EntityModelResource;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import java.text.DateFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Generated;

import javax.ws.rs.core.MultivaluedHashMap;

import misk.headless.client.dto.v1_0.HenaCafe;
import misk.headless.client.http.HttpInvoker;
import misk.headless.client.pagination.Page;
import misk.headless.client.resource.v1_0.HenaCafeResource;
import misk.headless.client.serdes.v1_0.HenaCafeSerDes;

import org.apache.commons.beanutils.BeanUtilsBean;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public abstract class BaseHenaCafeResourceTestCase {

	@ClassRule
	@Rule
	public static final LiferayIntegrationTestRule liferayIntegrationTestRule =
		new LiferayIntegrationTestRule();

	@BeforeClass
	public static void setUpClass() throws Exception {
		_dateFormat = DateFormatFactoryUtil.getSimpleDateFormat(
			"yyyy-MM-dd'T'HH:mm:ss'Z'");
	}

	@Before
	public void setUp() throws Exception {
		irrelevantGroup = GroupTestUtil.addGroup();
		testGroup = GroupTestUtil.addGroup();

		testCompany = CompanyLocalServiceUtil.getCompany(
			testGroup.getCompanyId());

		_henaCafeResource.setContextCompany(testCompany);

		HenaCafeResource.Builder builder = HenaCafeResource.builder();

		henaCafeResource = builder.authentication(
			"test@liferay.com", "test"
		).locale(
			LocaleUtil.getDefault()
		).build();
	}

	@After
	public void tearDown() throws Exception {
		GroupTestUtil.deleteGroup(irrelevantGroup);
		GroupTestUtil.deleteGroup(testGroup);
	}

	@Test
	public void testClientSerDesToDTO() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper() {
			{
				configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
				configure(
					SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
				enable(SerializationFeature.INDENT_OUTPUT);
				setDateFormat(new ISO8601DateFormat());
				setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
				setSerializationInclusion(JsonInclude.Include.NON_NULL);
				setVisibility(
					PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
				setVisibility(
					PropertyAccessor.GETTER, JsonAutoDetect.Visibility.NONE);
			}
		};

		HenaCafe henaCafe1 = randomHenaCafe();

		String json = objectMapper.writeValueAsString(henaCafe1);

		HenaCafe henaCafe2 = HenaCafeSerDes.toDTO(json);

		Assert.assertTrue(equals(henaCafe1, henaCafe2));
	}

	@Test
	public void testClientSerDesToJSON() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper() {
			{
				configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
				configure(
					SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
				setDateFormat(new ISO8601DateFormat());
				setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
				setSerializationInclusion(JsonInclude.Include.NON_NULL);
				setVisibility(
					PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
				setVisibility(
					PropertyAccessor.GETTER, JsonAutoDetect.Visibility.NONE);
			}
		};

		HenaCafe henaCafe = randomHenaCafe();

		String json1 = objectMapper.writeValueAsString(henaCafe);
		String json2 = HenaCafeSerDes.toJSON(henaCafe);

		Assert.assertEquals(
			objectMapper.readTree(json1), objectMapper.readTree(json2));
	}

	@Test
	public void testEscapeRegexInStringFields() throws Exception {
		String regex = "^[0-9]+(\\.[0-9]{1,2})\"?";

		HenaCafe henaCafe = randomHenaCafe();

		henaCafe.setDescription(regex);
		henaCafe.setLatitude(regex);
		henaCafe.setLocation(regex);
		henaCafe.setLongitude(regex);
		henaCafe.setSubtitle(regex);
		henaCafe.setTitle(regex);
		henaCafe.setWorkingdays(regex);
		henaCafe.setWorkinghours(regex);
		henaCafe.setWorkingsummary(regex);

		String json = HenaCafeSerDes.toJSON(henaCafe);

		Assert.assertFalse(json.contains(regex));

		henaCafe = HenaCafeSerDes.toDTO(json);

		Assert.assertEquals(regex, henaCafe.getDescription());
		Assert.assertEquals(regex, henaCafe.getLatitude());
		Assert.assertEquals(regex, henaCafe.getLocation());
		Assert.assertEquals(regex, henaCafe.getLongitude());
		Assert.assertEquals(regex, henaCafe.getSubtitle());
		Assert.assertEquals(regex, henaCafe.getTitle());
		Assert.assertEquals(regex, henaCafe.getWorkingdays());
		Assert.assertEquals(regex, henaCafe.getWorkinghours());
		Assert.assertEquals(regex, henaCafe.getWorkingsummary());
	}

	@Test
	public void testGetHenaCafe() throws Exception {
		Assert.assertTrue(false);
	}

	@Test
	public void testGraphQLGetHenaCafe() throws Exception {
		Assert.assertTrue(true);
	}

	@Test
	public void testGraphQLGetHenaCafeNotFound() throws Exception {
		Assert.assertTrue(true);
	}

	protected void assertContains(HenaCafe henaCafe, List<HenaCafe> henaCafes) {
		boolean contains = false;

		for (HenaCafe item : henaCafes) {
			if (equals(henaCafe, item)) {
				contains = true;

				break;
			}
		}

		Assert.assertTrue(
			henaCafes + " does not contain " + henaCafe, contains);
	}

	protected void assertHttpResponseStatusCode(
		int expectedHttpResponseStatusCode,
		HttpInvoker.HttpResponse actualHttpResponse) {

		Assert.assertEquals(
			expectedHttpResponseStatusCode, actualHttpResponse.getStatusCode());
	}

	protected void assertEquals(HenaCafe henaCafe1, HenaCafe henaCafe2) {
		Assert.assertTrue(
			henaCafe1 + " does not equal " + henaCafe2,
			equals(henaCafe1, henaCafe2));
	}

	protected void assertEquals(
		List<HenaCafe> henaCafes1, List<HenaCafe> henaCafes2) {

		Assert.assertEquals(henaCafes1.size(), henaCafes2.size());

		for (int i = 0; i < henaCafes1.size(); i++) {
			HenaCafe henaCafe1 = henaCafes1.get(i);
			HenaCafe henaCafe2 = henaCafes2.get(i);

			assertEquals(henaCafe1, henaCafe2);
		}
	}

	protected void assertEqualsIgnoringOrder(
		List<HenaCafe> henaCafes1, List<HenaCafe> henaCafes2) {

		Assert.assertEquals(henaCafes1.size(), henaCafes2.size());

		for (HenaCafe henaCafe1 : henaCafes1) {
			boolean contains = false;

			for (HenaCafe henaCafe2 : henaCafes2) {
				if (equals(henaCafe1, henaCafe2)) {
					contains = true;

					break;
				}
			}

			Assert.assertTrue(
				henaCafes2 + " does not contain " + henaCafe1, contains);
		}
	}

	protected void assertValid(HenaCafe henaCafe) throws Exception {
		boolean valid = true;

		for (String additionalAssertFieldName :
				getAdditionalAssertFieldNames()) {

			if (Objects.equals("categories", additionalAssertFieldName)) {
				if (henaCafe.getCategories() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("description", additionalAssertFieldName)) {
				if (henaCafe.getDescription() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("latitude", additionalAssertFieldName)) {
				if (henaCafe.getLatitude() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("location", additionalAssertFieldName)) {
				if (henaCafe.getLocation() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("longitude", additionalAssertFieldName)) {
				if (henaCafe.getLongitude() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("sliderimage", additionalAssertFieldName)) {
				if (henaCafe.getSliderimage() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("subtitle", additionalAssertFieldName)) {
				if (henaCafe.getSubtitle() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("title", additionalAssertFieldName)) {
				if (henaCafe.getTitle() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("workingdays", additionalAssertFieldName)) {
				if (henaCafe.getWorkingdays() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("workinghours", additionalAssertFieldName)) {
				if (henaCafe.getWorkinghours() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("workingsummary", additionalAssertFieldName)) {
				if (henaCafe.getWorkingsummary() == null) {
					valid = false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		Assert.assertTrue(valid);
	}

	protected void assertValid(Page<HenaCafe> page) {
		boolean valid = false;

		java.util.Collection<HenaCafe> henaCafes = page.getItems();

		int size = henaCafes.size();

		if ((page.getLastPage() > 0) && (page.getPage() > 0) &&
			(page.getPageSize() > 0) && (page.getTotalCount() > 0) &&
			(size > 0)) {

			valid = true;
		}

		Assert.assertTrue(valid);
	}

	protected String[] getAdditionalAssertFieldNames() {
		return new String[0];
	}

	protected List<GraphQLField> getGraphQLFields() throws Exception {
		List<GraphQLField> graphQLFields = new ArrayList<>();

		for (Field field :
				getDeclaredFields(misk.headless.dto.v1_0.HenaCafe.class)) {

			if (!ArrayUtil.contains(
					getAdditionalAssertFieldNames(), field.getName())) {

				continue;
			}

			graphQLFields.addAll(getGraphQLFields(field));
		}

		return graphQLFields;
	}

	protected List<GraphQLField> getGraphQLFields(Field... fields)
		throws Exception {

		List<GraphQLField> graphQLFields = new ArrayList<>();

		for (Field field : fields) {
			com.liferay.portal.vulcan.graphql.annotation.GraphQLField
				vulcanGraphQLField = field.getAnnotation(
					com.liferay.portal.vulcan.graphql.annotation.GraphQLField.
						class);

			if (vulcanGraphQLField != null) {
				Class<?> clazz = field.getType();

				if (clazz.isArray()) {
					clazz = clazz.getComponentType();
				}

				List<GraphQLField> childrenGraphQLFields = getGraphQLFields(
					getDeclaredFields(clazz));

				graphQLFields.add(
					new GraphQLField(field.getName(), childrenGraphQLFields));
			}
		}

		return graphQLFields;
	}

	protected String[] getIgnoredEntityFieldNames() {
		return new String[0];
	}

	protected boolean equals(HenaCafe henaCafe1, HenaCafe henaCafe2) {
		if (henaCafe1 == henaCafe2) {
			return true;
		}

		for (String additionalAssertFieldName :
				getAdditionalAssertFieldNames()) {

			if (Objects.equals("categories", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						henaCafe1.getCategories(), henaCafe2.getCategories())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("description", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						henaCafe1.getDescription(),
						henaCafe2.getDescription())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("latitude", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						henaCafe1.getLatitude(), henaCafe2.getLatitude())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("location", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						henaCafe1.getLocation(), henaCafe2.getLocation())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("longitude", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						henaCafe1.getLongitude(), henaCafe2.getLongitude())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("sliderimage", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						henaCafe1.getSliderimage(),
						henaCafe2.getSliderimage())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("subtitle", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						henaCafe1.getSubtitle(), henaCafe2.getSubtitle())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("title", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						henaCafe1.getTitle(), henaCafe2.getTitle())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("workingdays", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						henaCafe1.getWorkingdays(),
						henaCafe2.getWorkingdays())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("workinghours", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						henaCafe1.getWorkinghours(),
						henaCafe2.getWorkinghours())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("workingsummary", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						henaCafe1.getWorkingsummary(),
						henaCafe2.getWorkingsummary())) {

					return false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		return true;
	}

	protected boolean equals(
		Map<String, Object> map1, Map<String, Object> map2) {

		if (Objects.equals(map1.keySet(), map2.keySet())) {
			for (Map.Entry<String, Object> entry : map1.entrySet()) {
				if (entry.getValue() instanceof Map) {
					if (!equals(
							(Map)entry.getValue(),
							(Map)map2.get(entry.getKey()))) {

						return false;
					}
				}
				else if (!Objects.deepEquals(
							entry.getValue(), map2.get(entry.getKey()))) {

					return false;
				}
			}

			return true;
		}

		return false;
	}

	protected Field[] getDeclaredFields(Class clazz) throws Exception {
		Stream<Field> stream = Stream.of(
			ReflectionUtil.getDeclaredFields(clazz));

		return stream.filter(
			field -> !field.isSynthetic()
		).toArray(
			Field[]::new
		);
	}

	protected java.util.Collection<EntityField> getEntityFields()
		throws Exception {

		if (!(_henaCafeResource instanceof EntityModelResource)) {
			throw new UnsupportedOperationException(
				"Resource is not an instance of EntityModelResource");
		}

		EntityModelResource entityModelResource =
			(EntityModelResource)_henaCafeResource;

		EntityModel entityModel = entityModelResource.getEntityModel(
			new MultivaluedHashMap());

		Map<String, EntityField> entityFieldsMap =
			entityModel.getEntityFieldsMap();

		return entityFieldsMap.values();
	}

	protected List<EntityField> getEntityFields(EntityField.Type type)
		throws Exception {

		java.util.Collection<EntityField> entityFields = getEntityFields();

		Stream<EntityField> stream = entityFields.stream();

		return stream.filter(
			entityField ->
				Objects.equals(entityField.getType(), type) &&
				!ArrayUtil.contains(
					getIgnoredEntityFieldNames(), entityField.getName())
		).collect(
			Collectors.toList()
		);
	}

	protected String getFilterString(
		EntityField entityField, String operator, HenaCafe henaCafe) {

		StringBundler sb = new StringBundler();

		String entityFieldName = entityField.getName();

		sb.append(entityFieldName);

		sb.append(" ");
		sb.append(operator);
		sb.append(" ");

		if (entityFieldName.equals("categories")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("description")) {
			sb.append("'");
			sb.append(String.valueOf(henaCafe.getDescription()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("latitude")) {
			sb.append("'");
			sb.append(String.valueOf(henaCafe.getLatitude()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("location")) {
			sb.append("'");
			sb.append(String.valueOf(henaCafe.getLocation()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("longitude")) {
			sb.append("'");
			sb.append(String.valueOf(henaCafe.getLongitude()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("sliderimage")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("subtitle")) {
			sb.append("'");
			sb.append(String.valueOf(henaCafe.getSubtitle()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("title")) {
			sb.append("'");
			sb.append(String.valueOf(henaCafe.getTitle()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("workingdays")) {
			sb.append("'");
			sb.append(String.valueOf(henaCafe.getWorkingdays()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("workinghours")) {
			sb.append("'");
			sb.append(String.valueOf(henaCafe.getWorkinghours()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("workingsummary")) {
			sb.append("'");
			sb.append(String.valueOf(henaCafe.getWorkingsummary()));
			sb.append("'");

			return sb.toString();
		}

		throw new IllegalArgumentException(
			"Invalid entity field " + entityFieldName);
	}

	protected String invoke(String query) throws Exception {
		HttpInvoker httpInvoker = HttpInvoker.newHttpInvoker();

		httpInvoker.body(
			JSONUtil.put(
				"query", query
			).toString(),
			"application/json");
		httpInvoker.httpMethod(HttpInvoker.HttpMethod.POST);
		httpInvoker.path("http://localhost:8080/o/graphql");
		httpInvoker.userNameAndPassword("test@liferay.com:test");

		HttpInvoker.HttpResponse httpResponse = httpInvoker.invoke();

		return httpResponse.getContent();
	}

	protected JSONObject invokeGraphQLMutation(GraphQLField graphQLField)
		throws Exception {

		GraphQLField mutationGraphQLField = new GraphQLField(
			"mutation", graphQLField);

		return JSONFactoryUtil.createJSONObject(
			invoke(mutationGraphQLField.toString()));
	}

	protected JSONObject invokeGraphQLQuery(GraphQLField graphQLField)
		throws Exception {

		GraphQLField queryGraphQLField = new GraphQLField(
			"query", graphQLField);

		return JSONFactoryUtil.createJSONObject(
			invoke(queryGraphQLField.toString()));
	}

	protected HenaCafe randomHenaCafe() throws Exception {
		return new HenaCafe() {
			{
				description = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				latitude = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				location = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				longitude = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				subtitle = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				title = StringUtil.toLowerCase(RandomTestUtil.randomString());
				workingdays = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				workinghours = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				workingsummary = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
			}
		};
	}

	protected HenaCafe randomIrrelevantHenaCafe() throws Exception {
		HenaCafe randomIrrelevantHenaCafe = randomHenaCafe();

		return randomIrrelevantHenaCafe;
	}

	protected HenaCafe randomPatchHenaCafe() throws Exception {
		return randomHenaCafe();
	}

	protected HenaCafeResource henaCafeResource;
	protected Group irrelevantGroup;
	protected Company testCompany;
	protected Group testGroup;

	protected class GraphQLField {

		public GraphQLField(String key, GraphQLField... graphQLFields) {
			this(key, new HashMap<>(), graphQLFields);
		}

		public GraphQLField(String key, List<GraphQLField> graphQLFields) {
			this(key, new HashMap<>(), graphQLFields);
		}

		public GraphQLField(
			String key, Map<String, Object> parameterMap,
			GraphQLField... graphQLFields) {

			_key = key;
			_parameterMap = parameterMap;
			_graphQLFields = Arrays.asList(graphQLFields);
		}

		public GraphQLField(
			String key, Map<String, Object> parameterMap,
			List<GraphQLField> graphQLFields) {

			_key = key;
			_parameterMap = parameterMap;
			_graphQLFields = graphQLFields;
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder(_key);

			if (!_parameterMap.isEmpty()) {
				sb.append("(");

				for (Map.Entry<String, Object> entry :
						_parameterMap.entrySet()) {

					sb.append(entry.getKey());
					sb.append(": ");
					sb.append(entry.getValue());
					sb.append(", ");
				}

				sb.setLength(sb.length() - 2);

				sb.append(")");
			}

			if (!_graphQLFields.isEmpty()) {
				sb.append("{");

				for (GraphQLField graphQLField : _graphQLFields) {
					sb.append(graphQLField.toString());
					sb.append(", ");
				}

				sb.setLength(sb.length() - 2);

				sb.append("}");
			}

			return sb.toString();
		}

		private final List<GraphQLField> _graphQLFields;
		private final String _key;
		private final Map<String, Object> _parameterMap;

	}

	private static final com.liferay.portal.kernel.log.Log _log =
		LogFactoryUtil.getLog(BaseHenaCafeResourceTestCase.class);

	private static BeanUtilsBean _beanUtilsBean = new BeanUtilsBean() {

		@Override
		public void copyProperty(Object bean, String name, Object value)
			throws IllegalAccessException, InvocationTargetException {

			if (value != null) {
				super.copyProperty(bean, name, value);
			}
		}

	};
	private static DateFormat _dateFormat;

	@Inject
	private misk.headless.resource.v1_0.HenaCafeResource _henaCafeResource;

}