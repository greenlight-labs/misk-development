/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.notification.settings.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link NotificationSettingsLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see NotificationSettingsLocalService
 * @generated
 */
public class NotificationSettingsLocalServiceWrapper
	implements NotificationSettingsLocalService,
			   ServiceWrapper<NotificationSettingsLocalService> {

	public NotificationSettingsLocalServiceWrapper(
		NotificationSettingsLocalService notificationSettingsLocalService) {

		_notificationSettingsLocalService = notificationSettingsLocalService;
	}

	/**
	 * Adds the notification settings to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect NotificationSettingsLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param notificationSettings the notification settings
	 * @return the notification settings that was added
	 */
	@Override
	public com.notification.settings.model.NotificationSettings
		addNotificationSettings(
			com.notification.settings.model.NotificationSettings
				notificationSettings) {

		return _notificationSettingsLocalService.addNotificationSettings(
			notificationSettings);
	}

	/**
	 * Creates a new notification settings with the primary key. Does not add the notification settings to the database.
	 *
	 * @param notificationSettingsId the primary key for the new notification settings
	 * @return the new notification settings
	 */
	@Override
	public com.notification.settings.model.NotificationSettings
		createNotificationSettings(long notificationSettingsId) {

		return _notificationSettingsLocalService.createNotificationSettings(
			notificationSettingsId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _notificationSettingsLocalService.createPersistedModel(
			primaryKeyObj);
	}

	/**
	 * Deletes the notification settings with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect NotificationSettingsLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param notificationSettingsId the primary key of the notification settings
	 * @return the notification settings that was removed
	 * @throws PortalException if a notification settings with the primary key could not be found
	 */
	@Override
	public com.notification.settings.model.NotificationSettings
			deleteNotificationSettings(long notificationSettingsId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _notificationSettingsLocalService.deleteNotificationSettings(
			notificationSettingsId);
	}

	/**
	 * Deletes the notification settings from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect NotificationSettingsLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param notificationSettings the notification settings
	 * @return the notification settings that was removed
	 */
	@Override
	public com.notification.settings.model.NotificationSettings
		deleteNotificationSettings(
			com.notification.settings.model.NotificationSettings
				notificationSettings) {

		return _notificationSettingsLocalService.deleteNotificationSettings(
			notificationSettings);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _notificationSettingsLocalService.deletePersistedModel(
			persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _notificationSettingsLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _notificationSettingsLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.notification.settings.model.impl.NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _notificationSettingsLocalService.dynamicQuery(
			dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.notification.settings.model.impl.NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _notificationSettingsLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _notificationSettingsLocalService.dynamicQueryCount(
			dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _notificationSettingsLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public com.notification.settings.model.NotificationSettings
		fetchByAppUserId(int appUserId) {

		return _notificationSettingsLocalService.fetchByAppUserId(appUserId);
	}

	@Override
	public com.notification.settings.model.NotificationSettings
		fetchNotificationSettings(long notificationSettingsId) {

		return _notificationSettingsLocalService.fetchNotificationSettings(
			notificationSettingsId);
	}

	/**
	 * Returns the notification settings matching the UUID and group.
	 *
	 * @param uuid the notification settings's UUID
	 * @param groupId the primary key of the group
	 * @return the matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	@Override
	public com.notification.settings.model.NotificationSettings
		fetchNotificationSettingsByUuidAndGroupId(String uuid, long groupId) {

		return _notificationSettingsLocalService.
			fetchNotificationSettingsByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _notificationSettingsLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _notificationSettingsLocalService.
			getExportActionableDynamicQuery(portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _notificationSettingsLocalService.
			getIndexableActionableDynamicQuery();
	}

	@Override
	public java.util.List<com.notification.settings.model.NotificationSettings>
		getNotificationEnabledUsers(String notificationType, boolean status) {

		return _notificationSettingsLocalService.getNotificationEnabledUsers(
			notificationType, status);
	}

	@Override
	public java.util.List<com.notification.settings.model.NotificationSettings>
		getNotificationEnabledUsers(
			String notificationType, long typeId, boolean status) {

		return _notificationSettingsLocalService.getNotificationEnabledUsers(
			notificationType, typeId, status);
	}

	/**
	 * Returns the notification settings with the primary key.
	 *
	 * @param notificationSettingsId the primary key of the notification settings
	 * @return the notification settings
	 * @throws PortalException if a notification settings with the primary key could not be found
	 */
	@Override
	public com.notification.settings.model.NotificationSettings
			getNotificationSettings(long notificationSettingsId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _notificationSettingsLocalService.getNotificationSettings(
			notificationSettingsId);
	}

	/**
	 * Returns the notification settings matching the UUID and group.
	 *
	 * @param uuid the notification settings's UUID
	 * @param groupId the primary key of the group
	 * @return the matching notification settings
	 * @throws PortalException if a matching notification settings could not be found
	 */
	@Override
	public com.notification.settings.model.NotificationSettings
			getNotificationSettingsByUuidAndGroupId(String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _notificationSettingsLocalService.
			getNotificationSettingsByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Returns a range of all the notification settingses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.notification.settings.model.impl.NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @return the range of notification settingses
	 */
	@Override
	public java.util.List<com.notification.settings.model.NotificationSettings>
		getNotificationSettingses(int start, int end) {

		return _notificationSettingsLocalService.getNotificationSettingses(
			start, end);
	}

	/**
	 * Returns all the notification settingses matching the UUID and company.
	 *
	 * @param uuid the UUID of the notification settingses
	 * @param companyId the primary key of the company
	 * @return the matching notification settingses, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<com.notification.settings.model.NotificationSettings>
		getNotificationSettingsesByUuidAndCompanyId(
			String uuid, long companyId) {

		return _notificationSettingsLocalService.
			getNotificationSettingsesByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of notification settingses matching the UUID and company.
	 *
	 * @param uuid the UUID of the notification settingses
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching notification settingses, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<com.notification.settings.model.NotificationSettings>
		getNotificationSettingsesByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<com.notification.settings.model.NotificationSettings>
					orderByComparator) {

		return _notificationSettingsLocalService.
			getNotificationSettingsesByUuidAndCompanyId(
				uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of notification settingses.
	 *
	 * @return the number of notification settingses
	 */
	@Override
	public int getNotificationSettingsesCount() {
		return _notificationSettingsLocalService.
			getNotificationSettingsesCount();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _notificationSettingsLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _notificationSettingsLocalService.getPersistedModel(
			primaryKeyObj);
	}

	@Override
	public com.notification.settings.model.NotificationSettings
		setDefaultNotificationSettingsForUser(
			int appUserId, long companyId, long groupId) {

		return _notificationSettingsLocalService.
			setDefaultNotificationSettingsForUser(
				appUserId, companyId, groupId);
	}

	/**
	 * Updates the notification settings in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect NotificationSettingsLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param notificationSettings the notification settings
	 * @return the notification settings that was updated
	 */
	@Override
	public com.notification.settings.model.NotificationSettings
		updateNotificationSettings(
			com.notification.settings.model.NotificationSettings
				notificationSettings) {

		return _notificationSettingsLocalService.updateNotificationSettings(
			notificationSettings);
	}

	@Override
	public NotificationSettingsLocalService getWrappedService() {
		return _notificationSettingsLocalService;
	}

	@Override
	public void setWrappedService(
		NotificationSettingsLocalService notificationSettingsLocalService) {

		_notificationSettingsLocalService = notificationSettingsLocalService;
	}

	private NotificationSettingsLocalService _notificationSettingsLocalService;

}