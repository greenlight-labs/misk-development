/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.notification.settings.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.notification.settings.model.NotificationSettings;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence utility for the notification settings service. This utility wraps <code>com.notification.settings.service.persistence.impl.NotificationSettingsPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see NotificationSettingsPersistence
 * @generated
 */
public class NotificationSettingsUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(NotificationSettings notificationSettings) {
		getPersistence().clearCache(notificationSettings);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, NotificationSettings> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<NotificationSettings> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<NotificationSettings> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<NotificationSettings> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<NotificationSettings> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static NotificationSettings update(
		NotificationSettings notificationSettings) {

		return getPersistence().update(notificationSettings);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static NotificationSettings update(
		NotificationSettings notificationSettings,
		ServiceContext serviceContext) {

		return getPersistence().update(notificationSettings, serviceContext);
	}

	/**
	 * Returns all the notification settingses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching notification settingses
	 */
	public static List<NotificationSettings> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the notification settingses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @return the range of matching notification settingses
	 */
	public static List<NotificationSettings> findByUuid(
		String uuid, int start, int end) {

		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the notification settingses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching notification settingses
	 */
	public static List<NotificationSettings> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<NotificationSettings> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the notification settingses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching notification settingses
	 */
	public static List<NotificationSettings> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<NotificationSettings> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first notification settings in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	public static NotificationSettings findByUuid_First(
			String uuid,
			OrderByComparator<NotificationSettings> orderByComparator)
		throws com.notification.settings.exception.
			NoSuchNotificationSettingsException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first notification settings in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	public static NotificationSettings fetchByUuid_First(
		String uuid,
		OrderByComparator<NotificationSettings> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last notification settings in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	public static NotificationSettings findByUuid_Last(
			String uuid,
			OrderByComparator<NotificationSettings> orderByComparator)
		throws com.notification.settings.exception.
			NoSuchNotificationSettingsException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last notification settings in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	public static NotificationSettings fetchByUuid_Last(
		String uuid,
		OrderByComparator<NotificationSettings> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the notification settingses before and after the current notification settings in the ordered set where uuid = &#63;.
	 *
	 * @param notificationSettingsId the primary key of the current notification settings
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next notification settings
	 * @throws NoSuchNotificationSettingsException if a notification settings with the primary key could not be found
	 */
	public static NotificationSettings[] findByUuid_PrevAndNext(
			long notificationSettingsId, String uuid,
			OrderByComparator<NotificationSettings> orderByComparator)
		throws com.notification.settings.exception.
			NoSuchNotificationSettingsException {

		return getPersistence().findByUuid_PrevAndNext(
			notificationSettingsId, uuid, orderByComparator);
	}

	/**
	 * Removes all the notification settingses where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of notification settingses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching notification settingses
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the notification settings where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchNotificationSettingsException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	public static NotificationSettings findByUUID_G(String uuid, long groupId)
		throws com.notification.settings.exception.
			NoSuchNotificationSettingsException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the notification settings where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	public static NotificationSettings fetchByUUID_G(
		String uuid, long groupId) {

		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the notification settings where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	public static NotificationSettings fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, useFinderCache);
	}

	/**
	 * Removes the notification settings where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the notification settings that was removed
	 */
	public static NotificationSettings removeByUUID_G(String uuid, long groupId)
		throws com.notification.settings.exception.
			NoSuchNotificationSettingsException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of notification settingses where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching notification settingses
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the notification settingses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching notification settingses
	 */
	public static List<NotificationSettings> findByUuid_C(
		String uuid, long companyId) {

		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the notification settingses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @return the range of matching notification settingses
	 */
	public static List<NotificationSettings> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the notification settingses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching notification settingses
	 */
	public static List<NotificationSettings> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<NotificationSettings> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the notification settingses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching notification settingses
	 */
	public static List<NotificationSettings> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<NotificationSettings> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first notification settings in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	public static NotificationSettings findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<NotificationSettings> orderByComparator)
		throws com.notification.settings.exception.
			NoSuchNotificationSettingsException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first notification settings in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	public static NotificationSettings fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<NotificationSettings> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last notification settings in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	public static NotificationSettings findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<NotificationSettings> orderByComparator)
		throws com.notification.settings.exception.
			NoSuchNotificationSettingsException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last notification settings in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	public static NotificationSettings fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<NotificationSettings> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the notification settingses before and after the current notification settings in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param notificationSettingsId the primary key of the current notification settings
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next notification settings
	 * @throws NoSuchNotificationSettingsException if a notification settings with the primary key could not be found
	 */
	public static NotificationSettings[] findByUuid_C_PrevAndNext(
			long notificationSettingsId, String uuid, long companyId,
			OrderByComparator<NotificationSettings> orderByComparator)
		throws com.notification.settings.exception.
			NoSuchNotificationSettingsException {

		return getPersistence().findByUuid_C_PrevAndNext(
			notificationSettingsId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the notification settingses where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of notification settingses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching notification settingses
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the notification settings where appUserId = &#63; or throws a <code>NoSuchNotificationSettingsException</code> if it could not be found.
	 *
	 * @param appUserId the app user ID
	 * @return the matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	public static NotificationSettings findByAppUserId(int appUserId)
		throws com.notification.settings.exception.
			NoSuchNotificationSettingsException {

		return getPersistence().findByAppUserId(appUserId);
	}

	/**
	 * Returns the notification settings where appUserId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param appUserId the app user ID
	 * @return the matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	public static NotificationSettings fetchByAppUserId(int appUserId) {
		return getPersistence().fetchByAppUserId(appUserId);
	}

	/**
	 * Returns the notification settings where appUserId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param appUserId the app user ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	public static NotificationSettings fetchByAppUserId(
		int appUserId, boolean useFinderCache) {

		return getPersistence().fetchByAppUserId(appUserId, useFinderCache);
	}

	/**
	 * Removes the notification settings where appUserId = &#63; from the database.
	 *
	 * @param appUserId the app user ID
	 * @return the notification settings that was removed
	 */
	public static NotificationSettings removeByAppUserId(int appUserId)
		throws com.notification.settings.exception.
			NoSuchNotificationSettingsException {

		return getPersistence().removeByAppUserId(appUserId);
	}

	/**
	 * Returns the number of notification settingses where appUserId = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @return the number of matching notification settingses
	 */
	public static int countByAppUserId(int appUserId) {
		return getPersistence().countByAppUserId(appUserId);
	}

	/**
	 * Returns all the notification settingses where news = &#63;.
	 *
	 * @param news the news
	 * @return the matching notification settingses
	 */
	public static List<NotificationSettings> findByNews(boolean news) {
		return getPersistence().findByNews(news);
	}

	/**
	 * Returns a range of all the notification settingses where news = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param news the news
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @return the range of matching notification settingses
	 */
	public static List<NotificationSettings> findByNews(
		boolean news, int start, int end) {

		return getPersistence().findByNews(news, start, end);
	}

	/**
	 * Returns an ordered range of all the notification settingses where news = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param news the news
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching notification settingses
	 */
	public static List<NotificationSettings> findByNews(
		boolean news, int start, int end,
		OrderByComparator<NotificationSettings> orderByComparator) {

		return getPersistence().findByNews(news, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the notification settingses where news = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param news the news
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching notification settingses
	 */
	public static List<NotificationSettings> findByNews(
		boolean news, int start, int end,
		OrderByComparator<NotificationSettings> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByNews(
			news, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first notification settings in the ordered set where news = &#63;.
	 *
	 * @param news the news
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	public static NotificationSettings findByNews_First(
			boolean news,
			OrderByComparator<NotificationSettings> orderByComparator)
		throws com.notification.settings.exception.
			NoSuchNotificationSettingsException {

		return getPersistence().findByNews_First(news, orderByComparator);
	}

	/**
	 * Returns the first notification settings in the ordered set where news = &#63;.
	 *
	 * @param news the news
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	public static NotificationSettings fetchByNews_First(
		boolean news,
		OrderByComparator<NotificationSettings> orderByComparator) {

		return getPersistence().fetchByNews_First(news, orderByComparator);
	}

	/**
	 * Returns the last notification settings in the ordered set where news = &#63;.
	 *
	 * @param news the news
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	public static NotificationSettings findByNews_Last(
			boolean news,
			OrderByComparator<NotificationSettings> orderByComparator)
		throws com.notification.settings.exception.
			NoSuchNotificationSettingsException {

		return getPersistence().findByNews_Last(news, orderByComparator);
	}

	/**
	 * Returns the last notification settings in the ordered set where news = &#63;.
	 *
	 * @param news the news
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	public static NotificationSettings fetchByNews_Last(
		boolean news,
		OrderByComparator<NotificationSettings> orderByComparator) {

		return getPersistence().fetchByNews_Last(news, orderByComparator);
	}

	/**
	 * Returns the notification settingses before and after the current notification settings in the ordered set where news = &#63;.
	 *
	 * @param notificationSettingsId the primary key of the current notification settings
	 * @param news the news
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next notification settings
	 * @throws NoSuchNotificationSettingsException if a notification settings with the primary key could not be found
	 */
	public static NotificationSettings[] findByNews_PrevAndNext(
			long notificationSettingsId, boolean news,
			OrderByComparator<NotificationSettings> orderByComparator)
		throws com.notification.settings.exception.
			NoSuchNotificationSettingsException {

		return getPersistence().findByNews_PrevAndNext(
			notificationSettingsId, news, orderByComparator);
	}

	/**
	 * Removes all the notification settingses where news = &#63; from the database.
	 *
	 * @param news the news
	 */
	public static void removeByNews(boolean news) {
		getPersistence().removeByNews(news);
	}

	/**
	 * Returns the number of notification settingses where news = &#63;.
	 *
	 * @param news the news
	 * @return the number of matching notification settingses
	 */
	public static int countByNews(boolean news) {
		return getPersistence().countByNews(news);
	}

	/**
	 * Returns all the notification settingses where events = &#63;.
	 *
	 * @param events the events
	 * @return the matching notification settingses
	 */
	public static List<NotificationSettings> findByEvents(boolean events) {
		return getPersistence().findByEvents(events);
	}

	/**
	 * Returns a range of all the notification settingses where events = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param events the events
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @return the range of matching notification settingses
	 */
	public static List<NotificationSettings> findByEvents(
		boolean events, int start, int end) {

		return getPersistence().findByEvents(events, start, end);
	}

	/**
	 * Returns an ordered range of all the notification settingses where events = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param events the events
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching notification settingses
	 */
	public static List<NotificationSettings> findByEvents(
		boolean events, int start, int end,
		OrderByComparator<NotificationSettings> orderByComparator) {

		return getPersistence().findByEvents(
			events, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the notification settingses where events = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param events the events
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching notification settingses
	 */
	public static List<NotificationSettings> findByEvents(
		boolean events, int start, int end,
		OrderByComparator<NotificationSettings> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByEvents(
			events, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first notification settings in the ordered set where events = &#63;.
	 *
	 * @param events the events
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	public static NotificationSettings findByEvents_First(
			boolean events,
			OrderByComparator<NotificationSettings> orderByComparator)
		throws com.notification.settings.exception.
			NoSuchNotificationSettingsException {

		return getPersistence().findByEvents_First(events, orderByComparator);
	}

	/**
	 * Returns the first notification settings in the ordered set where events = &#63;.
	 *
	 * @param events the events
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	public static NotificationSettings fetchByEvents_First(
		boolean events,
		OrderByComparator<NotificationSettings> orderByComparator) {

		return getPersistence().fetchByEvents_First(events, orderByComparator);
	}

	/**
	 * Returns the last notification settings in the ordered set where events = &#63;.
	 *
	 * @param events the events
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	public static NotificationSettings findByEvents_Last(
			boolean events,
			OrderByComparator<NotificationSettings> orderByComparator)
		throws com.notification.settings.exception.
			NoSuchNotificationSettingsException {

		return getPersistence().findByEvents_Last(events, orderByComparator);
	}

	/**
	 * Returns the last notification settings in the ordered set where events = &#63;.
	 *
	 * @param events the events
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	public static NotificationSettings fetchByEvents_Last(
		boolean events,
		OrderByComparator<NotificationSettings> orderByComparator) {

		return getPersistence().fetchByEvents_Last(events, orderByComparator);
	}

	/**
	 * Returns the notification settingses before and after the current notification settings in the ordered set where events = &#63;.
	 *
	 * @param notificationSettingsId the primary key of the current notification settings
	 * @param events the events
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next notification settings
	 * @throws NoSuchNotificationSettingsException if a notification settings with the primary key could not be found
	 */
	public static NotificationSettings[] findByEvents_PrevAndNext(
			long notificationSettingsId, boolean events,
			OrderByComparator<NotificationSettings> orderByComparator)
		throws com.notification.settings.exception.
			NoSuchNotificationSettingsException {

		return getPersistence().findByEvents_PrevAndNext(
			notificationSettingsId, events, orderByComparator);
	}

	/**
	 * Removes all the notification settingses where events = &#63; from the database.
	 *
	 * @param events the events
	 */
	public static void removeByEvents(boolean events) {
		getPersistence().removeByEvents(events);
	}

	/**
	 * Returns the number of notification settingses where events = &#63;.
	 *
	 * @param events the events
	 * @return the number of matching notification settingses
	 */
	public static int countByEvents(boolean events) {
		return getPersistence().countByEvents(events);
	}

	/**
	 * Returns all the notification settingses where highlights = &#63;.
	 *
	 * @param highlights the highlights
	 * @return the matching notification settingses
	 */
	public static List<NotificationSettings> findByHighlights(
		boolean highlights) {

		return getPersistence().findByHighlights(highlights);
	}

	/**
	 * Returns a range of all the notification settingses where highlights = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param highlights the highlights
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @return the range of matching notification settingses
	 */
	public static List<NotificationSettings> findByHighlights(
		boolean highlights, int start, int end) {

		return getPersistence().findByHighlights(highlights, start, end);
	}

	/**
	 * Returns an ordered range of all the notification settingses where highlights = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param highlights the highlights
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching notification settingses
	 */
	public static List<NotificationSettings> findByHighlights(
		boolean highlights, int start, int end,
		OrderByComparator<NotificationSettings> orderByComparator) {

		return getPersistence().findByHighlights(
			highlights, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the notification settingses where highlights = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param highlights the highlights
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching notification settingses
	 */
	public static List<NotificationSettings> findByHighlights(
		boolean highlights, int start, int end,
		OrderByComparator<NotificationSettings> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByHighlights(
			highlights, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first notification settings in the ordered set where highlights = &#63;.
	 *
	 * @param highlights the highlights
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	public static NotificationSettings findByHighlights_First(
			boolean highlights,
			OrderByComparator<NotificationSettings> orderByComparator)
		throws com.notification.settings.exception.
			NoSuchNotificationSettingsException {

		return getPersistence().findByHighlights_First(
			highlights, orderByComparator);
	}

	/**
	 * Returns the first notification settings in the ordered set where highlights = &#63;.
	 *
	 * @param highlights the highlights
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	public static NotificationSettings fetchByHighlights_First(
		boolean highlights,
		OrderByComparator<NotificationSettings> orderByComparator) {

		return getPersistence().fetchByHighlights_First(
			highlights, orderByComparator);
	}

	/**
	 * Returns the last notification settings in the ordered set where highlights = &#63;.
	 *
	 * @param highlights the highlights
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	public static NotificationSettings findByHighlights_Last(
			boolean highlights,
			OrderByComparator<NotificationSettings> orderByComparator)
		throws com.notification.settings.exception.
			NoSuchNotificationSettingsException {

		return getPersistence().findByHighlights_Last(
			highlights, orderByComparator);
	}

	/**
	 * Returns the last notification settings in the ordered set where highlights = &#63;.
	 *
	 * @param highlights the highlights
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	public static NotificationSettings fetchByHighlights_Last(
		boolean highlights,
		OrderByComparator<NotificationSettings> orderByComparator) {

		return getPersistence().fetchByHighlights_Last(
			highlights, orderByComparator);
	}

	/**
	 * Returns the notification settingses before and after the current notification settings in the ordered set where highlights = &#63;.
	 *
	 * @param notificationSettingsId the primary key of the current notification settings
	 * @param highlights the highlights
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next notification settings
	 * @throws NoSuchNotificationSettingsException if a notification settings with the primary key could not be found
	 */
	public static NotificationSettings[] findByHighlights_PrevAndNext(
			long notificationSettingsId, boolean highlights,
			OrderByComparator<NotificationSettings> orderByComparator)
		throws com.notification.settings.exception.
			NoSuchNotificationSettingsException {

		return getPersistence().findByHighlights_PrevAndNext(
			notificationSettingsId, highlights, orderByComparator);
	}

	/**
	 * Removes all the notification settingses where highlights = &#63; from the database.
	 *
	 * @param highlights the highlights
	 */
	public static void removeByHighlights(boolean highlights) {
		getPersistence().removeByHighlights(highlights);
	}

	/**
	 * Returns the number of notification settingses where highlights = &#63;.
	 *
	 * @param highlights the highlights
	 * @return the number of matching notification settingses
	 */
	public static int countByHighlights(boolean highlights) {
		return getPersistence().countByHighlights(highlights);
	}

	/**
	 * Returns all the notification settingses where broadcastMessage = &#63;.
	 *
	 * @param broadcastMessage the broadcast message
	 * @return the matching notification settingses
	 */
	public static List<NotificationSettings> findByBroadcastMessage(
		boolean broadcastMessage) {

		return getPersistence().findByBroadcastMessage(broadcastMessage);
	}

	/**
	 * Returns a range of all the notification settingses where broadcastMessage = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param broadcastMessage the broadcast message
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @return the range of matching notification settingses
	 */
	public static List<NotificationSettings> findByBroadcastMessage(
		boolean broadcastMessage, int start, int end) {

		return getPersistence().findByBroadcastMessage(
			broadcastMessage, start, end);
	}

	/**
	 * Returns an ordered range of all the notification settingses where broadcastMessage = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param broadcastMessage the broadcast message
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching notification settingses
	 */
	public static List<NotificationSettings> findByBroadcastMessage(
		boolean broadcastMessage, int start, int end,
		OrderByComparator<NotificationSettings> orderByComparator) {

		return getPersistence().findByBroadcastMessage(
			broadcastMessage, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the notification settingses where broadcastMessage = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param broadcastMessage the broadcast message
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching notification settingses
	 */
	public static List<NotificationSettings> findByBroadcastMessage(
		boolean broadcastMessage, int start, int end,
		OrderByComparator<NotificationSettings> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByBroadcastMessage(
			broadcastMessage, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first notification settings in the ordered set where broadcastMessage = &#63;.
	 *
	 * @param broadcastMessage the broadcast message
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	public static NotificationSettings findByBroadcastMessage_First(
			boolean broadcastMessage,
			OrderByComparator<NotificationSettings> orderByComparator)
		throws com.notification.settings.exception.
			NoSuchNotificationSettingsException {

		return getPersistence().findByBroadcastMessage_First(
			broadcastMessage, orderByComparator);
	}

	/**
	 * Returns the first notification settings in the ordered set where broadcastMessage = &#63;.
	 *
	 * @param broadcastMessage the broadcast message
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	public static NotificationSettings fetchByBroadcastMessage_First(
		boolean broadcastMessage,
		OrderByComparator<NotificationSettings> orderByComparator) {

		return getPersistence().fetchByBroadcastMessage_First(
			broadcastMessage, orderByComparator);
	}

	/**
	 * Returns the last notification settings in the ordered set where broadcastMessage = &#63;.
	 *
	 * @param broadcastMessage the broadcast message
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	public static NotificationSettings findByBroadcastMessage_Last(
			boolean broadcastMessage,
			OrderByComparator<NotificationSettings> orderByComparator)
		throws com.notification.settings.exception.
			NoSuchNotificationSettingsException {

		return getPersistence().findByBroadcastMessage_Last(
			broadcastMessage, orderByComparator);
	}

	/**
	 * Returns the last notification settings in the ordered set where broadcastMessage = &#63;.
	 *
	 * @param broadcastMessage the broadcast message
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	public static NotificationSettings fetchByBroadcastMessage_Last(
		boolean broadcastMessage,
		OrderByComparator<NotificationSettings> orderByComparator) {

		return getPersistence().fetchByBroadcastMessage_Last(
			broadcastMessage, orderByComparator);
	}

	/**
	 * Returns the notification settingses before and after the current notification settings in the ordered set where broadcastMessage = &#63;.
	 *
	 * @param notificationSettingsId the primary key of the current notification settings
	 * @param broadcastMessage the broadcast message
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next notification settings
	 * @throws NoSuchNotificationSettingsException if a notification settings with the primary key could not be found
	 */
	public static NotificationSettings[] findByBroadcastMessage_PrevAndNext(
			long notificationSettingsId, boolean broadcastMessage,
			OrderByComparator<NotificationSettings> orderByComparator)
		throws com.notification.settings.exception.
			NoSuchNotificationSettingsException {

		return getPersistence().findByBroadcastMessage_PrevAndNext(
			notificationSettingsId, broadcastMessage, orderByComparator);
	}

	/**
	 * Removes all the notification settingses where broadcastMessage = &#63; from the database.
	 *
	 * @param broadcastMessage the broadcast message
	 */
	public static void removeByBroadcastMessage(boolean broadcastMessage) {
		getPersistence().removeByBroadcastMessage(broadcastMessage);
	}

	/**
	 * Returns the number of notification settingses where broadcastMessage = &#63;.
	 *
	 * @param broadcastMessage the broadcast message
	 * @return the number of matching notification settingses
	 */
	public static int countByBroadcastMessage(boolean broadcastMessage) {
		return getPersistence().countByBroadcastMessage(broadcastMessage);
	}

	/**
	 * Caches the notification settings in the entity cache if it is enabled.
	 *
	 * @param notificationSettings the notification settings
	 */
	public static void cacheResult(NotificationSettings notificationSettings) {
		getPersistence().cacheResult(notificationSettings);
	}

	/**
	 * Caches the notification settingses in the entity cache if it is enabled.
	 *
	 * @param notificationSettingses the notification settingses
	 */
	public static void cacheResult(
		List<NotificationSettings> notificationSettingses) {

		getPersistence().cacheResult(notificationSettingses);
	}

	/**
	 * Creates a new notification settings with the primary key. Does not add the notification settings to the database.
	 *
	 * @param notificationSettingsId the primary key for the new notification settings
	 * @return the new notification settings
	 */
	public static NotificationSettings create(long notificationSettingsId) {
		return getPersistence().create(notificationSettingsId);
	}

	/**
	 * Removes the notification settings with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param notificationSettingsId the primary key of the notification settings
	 * @return the notification settings that was removed
	 * @throws NoSuchNotificationSettingsException if a notification settings with the primary key could not be found
	 */
	public static NotificationSettings remove(long notificationSettingsId)
		throws com.notification.settings.exception.
			NoSuchNotificationSettingsException {

		return getPersistence().remove(notificationSettingsId);
	}

	public static NotificationSettings updateImpl(
		NotificationSettings notificationSettings) {

		return getPersistence().updateImpl(notificationSettings);
	}

	/**
	 * Returns the notification settings with the primary key or throws a <code>NoSuchNotificationSettingsException</code> if it could not be found.
	 *
	 * @param notificationSettingsId the primary key of the notification settings
	 * @return the notification settings
	 * @throws NoSuchNotificationSettingsException if a notification settings with the primary key could not be found
	 */
	public static NotificationSettings findByPrimaryKey(
			long notificationSettingsId)
		throws com.notification.settings.exception.
			NoSuchNotificationSettingsException {

		return getPersistence().findByPrimaryKey(notificationSettingsId);
	}

	/**
	 * Returns the notification settings with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param notificationSettingsId the primary key of the notification settings
	 * @return the notification settings, or <code>null</code> if a notification settings with the primary key could not be found
	 */
	public static NotificationSettings fetchByPrimaryKey(
		long notificationSettingsId) {

		return getPersistence().fetchByPrimaryKey(notificationSettingsId);
	}

	/**
	 * Returns all the notification settingses.
	 *
	 * @return the notification settingses
	 */
	public static List<NotificationSettings> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the notification settingses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @return the range of notification settingses
	 */
	public static List<NotificationSettings> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the notification settingses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of notification settingses
	 */
	public static List<NotificationSettings> findAll(
		int start, int end,
		OrderByComparator<NotificationSettings> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the notification settingses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of notification settingses
	 */
	public static List<NotificationSettings> findAll(
		int start, int end,
		OrderByComparator<NotificationSettings> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the notification settingses from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of notification settingses.
	 *
	 * @return the number of notification settingses
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static NotificationSettingsPersistence getPersistence() {
		return _persistence;
	}

	private static volatile NotificationSettingsPersistence _persistence;

}