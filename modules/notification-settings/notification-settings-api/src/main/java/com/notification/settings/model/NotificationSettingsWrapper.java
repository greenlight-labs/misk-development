/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.notification.settings.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link NotificationSettings}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see NotificationSettings
 * @generated
 */
public class NotificationSettingsWrapper
	extends BaseModelWrapper<NotificationSettings>
	implements ModelWrapper<NotificationSettings>, NotificationSettings {

	public NotificationSettingsWrapper(
		NotificationSettings notificationSettings) {

		super(notificationSettings);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("notificationSettingsId", getNotificationSettingsId());
		attributes.put("groupId", getGroupId());
		attributes.put("appUserId", getAppUserId());
		attributes.put("companyId", getCompanyId());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("news", isNews());
		attributes.put("events", isEvents());
		attributes.put("highlights", isHighlights());
		attributes.put("broadcastMessage", isBroadcastMessage());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long notificationSettingsId = (Long)attributes.get(
			"notificationSettingsId");

		if (notificationSettingsId != null) {
			setNotificationSettingsId(notificationSettingsId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Integer appUserId = (Integer)attributes.get("appUserId");

		if (appUserId != null) {
			setAppUserId(appUserId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Boolean news = (Boolean)attributes.get("news");

		if (news != null) {
			setNews(news);
		}

		Boolean events = (Boolean)attributes.get("events");

		if (events != null) {
			setEvents(events);
		}

		Boolean highlights = (Boolean)attributes.get("highlights");

		if (highlights != null) {
			setHighlights(highlights);
		}

		Boolean broadcastMessage = (Boolean)attributes.get("broadcastMessage");

		if (broadcastMessage != null) {
			setBroadcastMessage(broadcastMessage);
		}
	}

	/**
	 * Returns the app user ID of this notification settings.
	 *
	 * @return the app user ID of this notification settings
	 */
	@Override
	public int getAppUserId() {
		return model.getAppUserId();
	}

	/**
	 * Returns the broadcast message of this notification settings.
	 *
	 * @return the broadcast message of this notification settings
	 */
	@Override
	public boolean getBroadcastMessage() {
		return model.getBroadcastMessage();
	}

	/**
	 * Returns the company ID of this notification settings.
	 *
	 * @return the company ID of this notification settings
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this notification settings.
	 *
	 * @return the create date of this notification settings
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	/**
	 * Returns the events of this notification settings.
	 *
	 * @return the events of this notification settings
	 */
	@Override
	public boolean getEvents() {
		return model.getEvents();
	}

	/**
	 * Returns the group ID of this notification settings.
	 *
	 * @return the group ID of this notification settings
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the highlights of this notification settings.
	 *
	 * @return the highlights of this notification settings
	 */
	@Override
	public boolean getHighlights() {
		return model.getHighlights();
	}

	/**
	 * Returns the modified date of this notification settings.
	 *
	 * @return the modified date of this notification settings
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the news of this notification settings.
	 *
	 * @return the news of this notification settings
	 */
	@Override
	public boolean getNews() {
		return model.getNews();
	}

	/**
	 * Returns the notification settings ID of this notification settings.
	 *
	 * @return the notification settings ID of this notification settings
	 */
	@Override
	public long getNotificationSettingsId() {
		return model.getNotificationSettingsId();
	}

	/**
	 * Returns the primary key of this notification settings.
	 *
	 * @return the primary key of this notification settings
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the uuid of this notification settings.
	 *
	 * @return the uuid of this notification settings
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	/**
	 * Returns <code>true</code> if this notification settings is broadcast message.
	 *
	 * @return <code>true</code> if this notification settings is broadcast message; <code>false</code> otherwise
	 */
	@Override
	public boolean isBroadcastMessage() {
		return model.isBroadcastMessage();
	}

	/**
	 * Returns <code>true</code> if this notification settings is events.
	 *
	 * @return <code>true</code> if this notification settings is events; <code>false</code> otherwise
	 */
	@Override
	public boolean isEvents() {
		return model.isEvents();
	}

	/**
	 * Returns <code>true</code> if this notification settings is highlights.
	 *
	 * @return <code>true</code> if this notification settings is highlights; <code>false</code> otherwise
	 */
	@Override
	public boolean isHighlights() {
		return model.isHighlights();
	}

	/**
	 * Returns <code>true</code> if this notification settings is news.
	 *
	 * @return <code>true</code> if this notification settings is news; <code>false</code> otherwise
	 */
	@Override
	public boolean isNews() {
		return model.isNews();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the app user ID of this notification settings.
	 *
	 * @param appUserId the app user ID of this notification settings
	 */
	@Override
	public void setAppUserId(int appUserId) {
		model.setAppUserId(appUserId);
	}

	/**
	 * Sets whether this notification settings is broadcast message.
	 *
	 * @param broadcastMessage the broadcast message of this notification settings
	 */
	@Override
	public void setBroadcastMessage(boolean broadcastMessage) {
		model.setBroadcastMessage(broadcastMessage);
	}

	/**
	 * Sets the company ID of this notification settings.
	 *
	 * @param companyId the company ID of this notification settings
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this notification settings.
	 *
	 * @param createDate the create date of this notification settings
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets whether this notification settings is events.
	 *
	 * @param events the events of this notification settings
	 */
	@Override
	public void setEvents(boolean events) {
		model.setEvents(events);
	}

	/**
	 * Sets the group ID of this notification settings.
	 *
	 * @param groupId the group ID of this notification settings
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets whether this notification settings is highlights.
	 *
	 * @param highlights the highlights of this notification settings
	 */
	@Override
	public void setHighlights(boolean highlights) {
		model.setHighlights(highlights);
	}

	/**
	 * Sets the modified date of this notification settings.
	 *
	 * @param modifiedDate the modified date of this notification settings
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets whether this notification settings is news.
	 *
	 * @param news the news of this notification settings
	 */
	@Override
	public void setNews(boolean news) {
		model.setNews(news);
	}

	/**
	 * Sets the notification settings ID of this notification settings.
	 *
	 * @param notificationSettingsId the notification settings ID of this notification settings
	 */
	@Override
	public void setNotificationSettingsId(long notificationSettingsId) {
		model.setNotificationSettingsId(notificationSettingsId);
	}

	/**
	 * Sets the primary key of this notification settings.
	 *
	 * @param primaryKey the primary key of this notification settings
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the uuid of this notification settings.
	 *
	 * @param uuid the uuid of this notification settings
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected NotificationSettingsWrapper wrap(
		NotificationSettings notificationSettings) {

		return new NotificationSettingsWrapper(notificationSettings);
	}

}