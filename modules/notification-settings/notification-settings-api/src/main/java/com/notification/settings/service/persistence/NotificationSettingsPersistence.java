/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.notification.settings.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import com.notification.settings.exception.NoSuchNotificationSettingsException;
import com.notification.settings.model.NotificationSettings;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the notification settings service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see NotificationSettingsUtil
 * @generated
 */
@ProviderType
public interface NotificationSettingsPersistence
	extends BasePersistence<NotificationSettings> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link NotificationSettingsUtil} to access the notification settings persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the notification settingses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching notification settingses
	 */
	public java.util.List<NotificationSettings> findByUuid(String uuid);

	/**
	 * Returns a range of all the notification settingses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @return the range of matching notification settingses
	 */
	public java.util.List<NotificationSettings> findByUuid(
		String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the notification settingses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching notification settingses
	 */
	public java.util.List<NotificationSettings> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<NotificationSettings>
			orderByComparator);

	/**
	 * Returns an ordered range of all the notification settingses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching notification settingses
	 */
	public java.util.List<NotificationSettings> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<NotificationSettings>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first notification settings in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	public NotificationSettings findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator
				<NotificationSettings> orderByComparator)
		throws NoSuchNotificationSettingsException;

	/**
	 * Returns the first notification settings in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	public NotificationSettings fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<NotificationSettings>
			orderByComparator);

	/**
	 * Returns the last notification settings in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	public NotificationSettings findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator
				<NotificationSettings> orderByComparator)
		throws NoSuchNotificationSettingsException;

	/**
	 * Returns the last notification settings in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	public NotificationSettings fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<NotificationSettings>
			orderByComparator);

	/**
	 * Returns the notification settingses before and after the current notification settings in the ordered set where uuid = &#63;.
	 *
	 * @param notificationSettingsId the primary key of the current notification settings
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next notification settings
	 * @throws NoSuchNotificationSettingsException if a notification settings with the primary key could not be found
	 */
	public NotificationSettings[] findByUuid_PrevAndNext(
			long notificationSettingsId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator
				<NotificationSettings> orderByComparator)
		throws NoSuchNotificationSettingsException;

	/**
	 * Removes all the notification settingses where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of notification settingses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching notification settingses
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the notification settings where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchNotificationSettingsException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	public NotificationSettings findByUUID_G(String uuid, long groupId)
		throws NoSuchNotificationSettingsException;

	/**
	 * Returns the notification settings where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	public NotificationSettings fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the notification settings where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	public NotificationSettings fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Removes the notification settings where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the notification settings that was removed
	 */
	public NotificationSettings removeByUUID_G(String uuid, long groupId)
		throws NoSuchNotificationSettingsException;

	/**
	 * Returns the number of notification settingses where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching notification settingses
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the notification settingses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching notification settingses
	 */
	public java.util.List<NotificationSettings> findByUuid_C(
		String uuid, long companyId);

	/**
	 * Returns a range of all the notification settingses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @return the range of matching notification settingses
	 */
	public java.util.List<NotificationSettings> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the notification settingses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching notification settingses
	 */
	public java.util.List<NotificationSettings> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<NotificationSettings>
			orderByComparator);

	/**
	 * Returns an ordered range of all the notification settingses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching notification settingses
	 */
	public java.util.List<NotificationSettings> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<NotificationSettings>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first notification settings in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	public NotificationSettings findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator
				<NotificationSettings> orderByComparator)
		throws NoSuchNotificationSettingsException;

	/**
	 * Returns the first notification settings in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	public NotificationSettings fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<NotificationSettings>
			orderByComparator);

	/**
	 * Returns the last notification settings in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	public NotificationSettings findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator
				<NotificationSettings> orderByComparator)
		throws NoSuchNotificationSettingsException;

	/**
	 * Returns the last notification settings in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	public NotificationSettings fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<NotificationSettings>
			orderByComparator);

	/**
	 * Returns the notification settingses before and after the current notification settings in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param notificationSettingsId the primary key of the current notification settings
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next notification settings
	 * @throws NoSuchNotificationSettingsException if a notification settings with the primary key could not be found
	 */
	public NotificationSettings[] findByUuid_C_PrevAndNext(
			long notificationSettingsId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator
				<NotificationSettings> orderByComparator)
		throws NoSuchNotificationSettingsException;

	/**
	 * Removes all the notification settingses where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of notification settingses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching notification settingses
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns the notification settings where appUserId = &#63; or throws a <code>NoSuchNotificationSettingsException</code> if it could not be found.
	 *
	 * @param appUserId the app user ID
	 * @return the matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	public NotificationSettings findByAppUserId(int appUserId)
		throws NoSuchNotificationSettingsException;

	/**
	 * Returns the notification settings where appUserId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param appUserId the app user ID
	 * @return the matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	public NotificationSettings fetchByAppUserId(int appUserId);

	/**
	 * Returns the notification settings where appUserId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param appUserId the app user ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	public NotificationSettings fetchByAppUserId(
		int appUserId, boolean useFinderCache);

	/**
	 * Removes the notification settings where appUserId = &#63; from the database.
	 *
	 * @param appUserId the app user ID
	 * @return the notification settings that was removed
	 */
	public NotificationSettings removeByAppUserId(int appUserId)
		throws NoSuchNotificationSettingsException;

	/**
	 * Returns the number of notification settingses where appUserId = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @return the number of matching notification settingses
	 */
	public int countByAppUserId(int appUserId);

	/**
	 * Returns all the notification settingses where news = &#63;.
	 *
	 * @param news the news
	 * @return the matching notification settingses
	 */
	public java.util.List<NotificationSettings> findByNews(boolean news);

	/**
	 * Returns a range of all the notification settingses where news = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param news the news
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @return the range of matching notification settingses
	 */
	public java.util.List<NotificationSettings> findByNews(
		boolean news, int start, int end);

	/**
	 * Returns an ordered range of all the notification settingses where news = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param news the news
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching notification settingses
	 */
	public java.util.List<NotificationSettings> findByNews(
		boolean news, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<NotificationSettings>
			orderByComparator);

	/**
	 * Returns an ordered range of all the notification settingses where news = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param news the news
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching notification settingses
	 */
	public java.util.List<NotificationSettings> findByNews(
		boolean news, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<NotificationSettings>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first notification settings in the ordered set where news = &#63;.
	 *
	 * @param news the news
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	public NotificationSettings findByNews_First(
			boolean news,
			com.liferay.portal.kernel.util.OrderByComparator
				<NotificationSettings> orderByComparator)
		throws NoSuchNotificationSettingsException;

	/**
	 * Returns the first notification settings in the ordered set where news = &#63;.
	 *
	 * @param news the news
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	public NotificationSettings fetchByNews_First(
		boolean news,
		com.liferay.portal.kernel.util.OrderByComparator<NotificationSettings>
			orderByComparator);

	/**
	 * Returns the last notification settings in the ordered set where news = &#63;.
	 *
	 * @param news the news
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	public NotificationSettings findByNews_Last(
			boolean news,
			com.liferay.portal.kernel.util.OrderByComparator
				<NotificationSettings> orderByComparator)
		throws NoSuchNotificationSettingsException;

	/**
	 * Returns the last notification settings in the ordered set where news = &#63;.
	 *
	 * @param news the news
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	public NotificationSettings fetchByNews_Last(
		boolean news,
		com.liferay.portal.kernel.util.OrderByComparator<NotificationSettings>
			orderByComparator);

	/**
	 * Returns the notification settingses before and after the current notification settings in the ordered set where news = &#63;.
	 *
	 * @param notificationSettingsId the primary key of the current notification settings
	 * @param news the news
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next notification settings
	 * @throws NoSuchNotificationSettingsException if a notification settings with the primary key could not be found
	 */
	public NotificationSettings[] findByNews_PrevAndNext(
			long notificationSettingsId, boolean news,
			com.liferay.portal.kernel.util.OrderByComparator
				<NotificationSettings> orderByComparator)
		throws NoSuchNotificationSettingsException;

	/**
	 * Removes all the notification settingses where news = &#63; from the database.
	 *
	 * @param news the news
	 */
	public void removeByNews(boolean news);

	/**
	 * Returns the number of notification settingses where news = &#63;.
	 *
	 * @param news the news
	 * @return the number of matching notification settingses
	 */
	public int countByNews(boolean news);

	/**
	 * Returns all the notification settingses where events = &#63;.
	 *
	 * @param events the events
	 * @return the matching notification settingses
	 */
	public java.util.List<NotificationSettings> findByEvents(boolean events);

	/**
	 * Returns a range of all the notification settingses where events = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param events the events
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @return the range of matching notification settingses
	 */
	public java.util.List<NotificationSettings> findByEvents(
		boolean events, int start, int end);

	/**
	 * Returns an ordered range of all the notification settingses where events = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param events the events
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching notification settingses
	 */
	public java.util.List<NotificationSettings> findByEvents(
		boolean events, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<NotificationSettings>
			orderByComparator);

	/**
	 * Returns an ordered range of all the notification settingses where events = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param events the events
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching notification settingses
	 */
	public java.util.List<NotificationSettings> findByEvents(
		boolean events, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<NotificationSettings>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first notification settings in the ordered set where events = &#63;.
	 *
	 * @param events the events
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	public NotificationSettings findByEvents_First(
			boolean events,
			com.liferay.portal.kernel.util.OrderByComparator
				<NotificationSettings> orderByComparator)
		throws NoSuchNotificationSettingsException;

	/**
	 * Returns the first notification settings in the ordered set where events = &#63;.
	 *
	 * @param events the events
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	public NotificationSettings fetchByEvents_First(
		boolean events,
		com.liferay.portal.kernel.util.OrderByComparator<NotificationSettings>
			orderByComparator);

	/**
	 * Returns the last notification settings in the ordered set where events = &#63;.
	 *
	 * @param events the events
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	public NotificationSettings findByEvents_Last(
			boolean events,
			com.liferay.portal.kernel.util.OrderByComparator
				<NotificationSettings> orderByComparator)
		throws NoSuchNotificationSettingsException;

	/**
	 * Returns the last notification settings in the ordered set where events = &#63;.
	 *
	 * @param events the events
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	public NotificationSettings fetchByEvents_Last(
		boolean events,
		com.liferay.portal.kernel.util.OrderByComparator<NotificationSettings>
			orderByComparator);

	/**
	 * Returns the notification settingses before and after the current notification settings in the ordered set where events = &#63;.
	 *
	 * @param notificationSettingsId the primary key of the current notification settings
	 * @param events the events
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next notification settings
	 * @throws NoSuchNotificationSettingsException if a notification settings with the primary key could not be found
	 */
	public NotificationSettings[] findByEvents_PrevAndNext(
			long notificationSettingsId, boolean events,
			com.liferay.portal.kernel.util.OrderByComparator
				<NotificationSettings> orderByComparator)
		throws NoSuchNotificationSettingsException;

	/**
	 * Removes all the notification settingses where events = &#63; from the database.
	 *
	 * @param events the events
	 */
	public void removeByEvents(boolean events);

	/**
	 * Returns the number of notification settingses where events = &#63;.
	 *
	 * @param events the events
	 * @return the number of matching notification settingses
	 */
	public int countByEvents(boolean events);

	/**
	 * Returns all the notification settingses where highlights = &#63;.
	 *
	 * @param highlights the highlights
	 * @return the matching notification settingses
	 */
	public java.util.List<NotificationSettings> findByHighlights(
		boolean highlights);

	/**
	 * Returns a range of all the notification settingses where highlights = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param highlights the highlights
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @return the range of matching notification settingses
	 */
	public java.util.List<NotificationSettings> findByHighlights(
		boolean highlights, int start, int end);

	/**
	 * Returns an ordered range of all the notification settingses where highlights = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param highlights the highlights
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching notification settingses
	 */
	public java.util.List<NotificationSettings> findByHighlights(
		boolean highlights, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<NotificationSettings>
			orderByComparator);

	/**
	 * Returns an ordered range of all the notification settingses where highlights = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param highlights the highlights
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching notification settingses
	 */
	public java.util.List<NotificationSettings> findByHighlights(
		boolean highlights, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<NotificationSettings>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first notification settings in the ordered set where highlights = &#63;.
	 *
	 * @param highlights the highlights
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	public NotificationSettings findByHighlights_First(
			boolean highlights,
			com.liferay.portal.kernel.util.OrderByComparator
				<NotificationSettings> orderByComparator)
		throws NoSuchNotificationSettingsException;

	/**
	 * Returns the first notification settings in the ordered set where highlights = &#63;.
	 *
	 * @param highlights the highlights
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	public NotificationSettings fetchByHighlights_First(
		boolean highlights,
		com.liferay.portal.kernel.util.OrderByComparator<NotificationSettings>
			orderByComparator);

	/**
	 * Returns the last notification settings in the ordered set where highlights = &#63;.
	 *
	 * @param highlights the highlights
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	public NotificationSettings findByHighlights_Last(
			boolean highlights,
			com.liferay.portal.kernel.util.OrderByComparator
				<NotificationSettings> orderByComparator)
		throws NoSuchNotificationSettingsException;

	/**
	 * Returns the last notification settings in the ordered set where highlights = &#63;.
	 *
	 * @param highlights the highlights
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	public NotificationSettings fetchByHighlights_Last(
		boolean highlights,
		com.liferay.portal.kernel.util.OrderByComparator<NotificationSettings>
			orderByComparator);

	/**
	 * Returns the notification settingses before and after the current notification settings in the ordered set where highlights = &#63;.
	 *
	 * @param notificationSettingsId the primary key of the current notification settings
	 * @param highlights the highlights
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next notification settings
	 * @throws NoSuchNotificationSettingsException if a notification settings with the primary key could not be found
	 */
	public NotificationSettings[] findByHighlights_PrevAndNext(
			long notificationSettingsId, boolean highlights,
			com.liferay.portal.kernel.util.OrderByComparator
				<NotificationSettings> orderByComparator)
		throws NoSuchNotificationSettingsException;

	/**
	 * Removes all the notification settingses where highlights = &#63; from the database.
	 *
	 * @param highlights the highlights
	 */
	public void removeByHighlights(boolean highlights);

	/**
	 * Returns the number of notification settingses where highlights = &#63;.
	 *
	 * @param highlights the highlights
	 * @return the number of matching notification settingses
	 */
	public int countByHighlights(boolean highlights);

	/**
	 * Returns all the notification settingses where broadcastMessage = &#63;.
	 *
	 * @param broadcastMessage the broadcast message
	 * @return the matching notification settingses
	 */
	public java.util.List<NotificationSettings> findByBroadcastMessage(
		boolean broadcastMessage);

	/**
	 * Returns a range of all the notification settingses where broadcastMessage = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param broadcastMessage the broadcast message
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @return the range of matching notification settingses
	 */
	public java.util.List<NotificationSettings> findByBroadcastMessage(
		boolean broadcastMessage, int start, int end);

	/**
	 * Returns an ordered range of all the notification settingses where broadcastMessage = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param broadcastMessage the broadcast message
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching notification settingses
	 */
	public java.util.List<NotificationSettings> findByBroadcastMessage(
		boolean broadcastMessage, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<NotificationSettings>
			orderByComparator);

	/**
	 * Returns an ordered range of all the notification settingses where broadcastMessage = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param broadcastMessage the broadcast message
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching notification settingses
	 */
	public java.util.List<NotificationSettings> findByBroadcastMessage(
		boolean broadcastMessage, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<NotificationSettings>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first notification settings in the ordered set where broadcastMessage = &#63;.
	 *
	 * @param broadcastMessage the broadcast message
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	public NotificationSettings findByBroadcastMessage_First(
			boolean broadcastMessage,
			com.liferay.portal.kernel.util.OrderByComparator
				<NotificationSettings> orderByComparator)
		throws NoSuchNotificationSettingsException;

	/**
	 * Returns the first notification settings in the ordered set where broadcastMessage = &#63;.
	 *
	 * @param broadcastMessage the broadcast message
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	public NotificationSettings fetchByBroadcastMessage_First(
		boolean broadcastMessage,
		com.liferay.portal.kernel.util.OrderByComparator<NotificationSettings>
			orderByComparator);

	/**
	 * Returns the last notification settings in the ordered set where broadcastMessage = &#63;.
	 *
	 * @param broadcastMessage the broadcast message
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	public NotificationSettings findByBroadcastMessage_Last(
			boolean broadcastMessage,
			com.liferay.portal.kernel.util.OrderByComparator
				<NotificationSettings> orderByComparator)
		throws NoSuchNotificationSettingsException;

	/**
	 * Returns the last notification settings in the ordered set where broadcastMessage = &#63;.
	 *
	 * @param broadcastMessage the broadcast message
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	public NotificationSettings fetchByBroadcastMessage_Last(
		boolean broadcastMessage,
		com.liferay.portal.kernel.util.OrderByComparator<NotificationSettings>
			orderByComparator);

	/**
	 * Returns the notification settingses before and after the current notification settings in the ordered set where broadcastMessage = &#63;.
	 *
	 * @param notificationSettingsId the primary key of the current notification settings
	 * @param broadcastMessage the broadcast message
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next notification settings
	 * @throws NoSuchNotificationSettingsException if a notification settings with the primary key could not be found
	 */
	public NotificationSettings[] findByBroadcastMessage_PrevAndNext(
			long notificationSettingsId, boolean broadcastMessage,
			com.liferay.portal.kernel.util.OrderByComparator
				<NotificationSettings> orderByComparator)
		throws NoSuchNotificationSettingsException;

	/**
	 * Removes all the notification settingses where broadcastMessage = &#63; from the database.
	 *
	 * @param broadcastMessage the broadcast message
	 */
	public void removeByBroadcastMessage(boolean broadcastMessage);

	/**
	 * Returns the number of notification settingses where broadcastMessage = &#63;.
	 *
	 * @param broadcastMessage the broadcast message
	 * @return the number of matching notification settingses
	 */
	public int countByBroadcastMessage(boolean broadcastMessage);

	/**
	 * Caches the notification settings in the entity cache if it is enabled.
	 *
	 * @param notificationSettings the notification settings
	 */
	public void cacheResult(NotificationSettings notificationSettings);

	/**
	 * Caches the notification settingses in the entity cache if it is enabled.
	 *
	 * @param notificationSettingses the notification settingses
	 */
	public void cacheResult(
		java.util.List<NotificationSettings> notificationSettingses);

	/**
	 * Creates a new notification settings with the primary key. Does not add the notification settings to the database.
	 *
	 * @param notificationSettingsId the primary key for the new notification settings
	 * @return the new notification settings
	 */
	public NotificationSettings create(long notificationSettingsId);

	/**
	 * Removes the notification settings with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param notificationSettingsId the primary key of the notification settings
	 * @return the notification settings that was removed
	 * @throws NoSuchNotificationSettingsException if a notification settings with the primary key could not be found
	 */
	public NotificationSettings remove(long notificationSettingsId)
		throws NoSuchNotificationSettingsException;

	public NotificationSettings updateImpl(
		NotificationSettings notificationSettings);

	/**
	 * Returns the notification settings with the primary key or throws a <code>NoSuchNotificationSettingsException</code> if it could not be found.
	 *
	 * @param notificationSettingsId the primary key of the notification settings
	 * @return the notification settings
	 * @throws NoSuchNotificationSettingsException if a notification settings with the primary key could not be found
	 */
	public NotificationSettings findByPrimaryKey(long notificationSettingsId)
		throws NoSuchNotificationSettingsException;

	/**
	 * Returns the notification settings with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param notificationSettingsId the primary key of the notification settings
	 * @return the notification settings, or <code>null</code> if a notification settings with the primary key could not be found
	 */
	public NotificationSettings fetchByPrimaryKey(long notificationSettingsId);

	/**
	 * Returns all the notification settingses.
	 *
	 * @return the notification settingses
	 */
	public java.util.List<NotificationSettings> findAll();

	/**
	 * Returns a range of all the notification settingses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @return the range of notification settingses
	 */
	public java.util.List<NotificationSettings> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the notification settingses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of notification settingses
	 */
	public java.util.List<NotificationSettings> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<NotificationSettings>
			orderByComparator);

	/**
	 * Returns an ordered range of all the notification settingses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of notification settingses
	 */
	public java.util.List<NotificationSettings> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<NotificationSettings>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the notification settingses from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of notification settingses.
	 *
	 * @return the number of notification settingses
	 */
	public int countAll();

}