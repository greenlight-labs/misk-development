/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.notification.settings.service;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.notification.settings.model.NotificationSettings;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service utility for NotificationSettings. This utility wraps
 * <code>com.notification.settings.service.impl.NotificationSettingsLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see NotificationSettingsLocalService
 * @generated
 */
public class NotificationSettingsLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>com.notification.settings.service.impl.NotificationSettingsLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the notification settings to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect NotificationSettingsLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param notificationSettings the notification settings
	 * @return the notification settings that was added
	 */
	public static NotificationSettings addNotificationSettings(
		NotificationSettings notificationSettings) {

		return getService().addNotificationSettings(notificationSettings);
	}

	/**
	 * Creates a new notification settings with the primary key. Does not add the notification settings to the database.
	 *
	 * @param notificationSettingsId the primary key for the new notification settings
	 * @return the new notification settings
	 */
	public static NotificationSettings createNotificationSettings(
		long notificationSettingsId) {

		return getService().createNotificationSettings(notificationSettingsId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the notification settings with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect NotificationSettingsLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param notificationSettingsId the primary key of the notification settings
	 * @return the notification settings that was removed
	 * @throws PortalException if a notification settings with the primary key could not be found
	 */
	public static NotificationSettings deleteNotificationSettings(
			long notificationSettingsId)
		throws PortalException {

		return getService().deleteNotificationSettings(notificationSettingsId);
	}

	/**
	 * Deletes the notification settings from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect NotificationSettingsLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param notificationSettings the notification settings
	 * @return the notification settings that was removed
	 */
	public static NotificationSettings deleteNotificationSettings(
		NotificationSettings notificationSettings) {

		return getService().deleteNotificationSettings(notificationSettings);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.notification.settings.model.impl.NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.notification.settings.model.impl.NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static NotificationSettings fetchByAppUserId(int appUserId) {
		return getService().fetchByAppUserId(appUserId);
	}

	public static NotificationSettings fetchNotificationSettings(
		long notificationSettingsId) {

		return getService().fetchNotificationSettings(notificationSettingsId);
	}

	/**
	 * Returns the notification settings matching the UUID and group.
	 *
	 * @param uuid the notification settings's UUID
	 * @param groupId the primary key of the group
	 * @return the matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	public static NotificationSettings
		fetchNotificationSettingsByUuidAndGroupId(String uuid, long groupId) {

		return getService().fetchNotificationSettingsByUuidAndGroupId(
			uuid, groupId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	public static List<NotificationSettings> getNotificationEnabledUsers(
		String notificationType, boolean status) {

		return getService().getNotificationEnabledUsers(
			notificationType, status);
	}

	public static List<NotificationSettings> getNotificationEnabledUsers(
		String notificationType, long typeId, boolean status) {

		return getService().getNotificationEnabledUsers(
			notificationType, typeId, status);
	}

	/**
	 * Returns the notification settings with the primary key.
	 *
	 * @param notificationSettingsId the primary key of the notification settings
	 * @return the notification settings
	 * @throws PortalException if a notification settings with the primary key could not be found
	 */
	public static NotificationSettings getNotificationSettings(
			long notificationSettingsId)
		throws PortalException {

		return getService().getNotificationSettings(notificationSettingsId);
	}

	/**
	 * Returns the notification settings matching the UUID and group.
	 *
	 * @param uuid the notification settings's UUID
	 * @param groupId the primary key of the group
	 * @return the matching notification settings
	 * @throws PortalException if a matching notification settings could not be found
	 */
	public static NotificationSettings getNotificationSettingsByUuidAndGroupId(
			String uuid, long groupId)
		throws PortalException {

		return getService().getNotificationSettingsByUuidAndGroupId(
			uuid, groupId);
	}

	/**
	 * Returns a range of all the notification settingses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.notification.settings.model.impl.NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @return the range of notification settingses
	 */
	public static List<NotificationSettings> getNotificationSettingses(
		int start, int end) {

		return getService().getNotificationSettingses(start, end);
	}

	/**
	 * Returns all the notification settingses matching the UUID and company.
	 *
	 * @param uuid the UUID of the notification settingses
	 * @param companyId the primary key of the company
	 * @return the matching notification settingses, or an empty list if no matches were found
	 */
	public static List<NotificationSettings>
		getNotificationSettingsesByUuidAndCompanyId(
			String uuid, long companyId) {

		return getService().getNotificationSettingsesByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of notification settingses matching the UUID and company.
	 *
	 * @param uuid the UUID of the notification settingses
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching notification settingses, or an empty list if no matches were found
	 */
	public static List<NotificationSettings>
		getNotificationSettingsesByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			OrderByComparator<NotificationSettings> orderByComparator) {

		return getService().getNotificationSettingsesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of notification settingses.
	 *
	 * @return the number of notification settingses
	 */
	public static int getNotificationSettingsesCount() {
		return getService().getNotificationSettingsesCount();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	public static NotificationSettings setDefaultNotificationSettingsForUser(
		int appUserId, long companyId, long groupId) {

		return getService().setDefaultNotificationSettingsForUser(
			appUserId, companyId, groupId);
	}

	/**
	 * Updates the notification settings in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect NotificationSettingsLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param notificationSettings the notification settings
	 * @return the notification settings that was updated
	 */
	public static NotificationSettings updateNotificationSettings(
		NotificationSettings notificationSettings) {

		return getService().updateNotificationSettings(notificationSettings);
	}

	public static NotificationSettingsLocalService getService() {
		return _service;
	}

	private static volatile NotificationSettingsLocalService _service;

}