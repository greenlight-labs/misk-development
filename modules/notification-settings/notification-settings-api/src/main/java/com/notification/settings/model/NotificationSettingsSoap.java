/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.notification.settings.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.notification.settings.service.http.NotificationSettingsServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @deprecated As of Athanasius (7.3.x), with no direct replacement
 * @generated
 */
@Deprecated
public class NotificationSettingsSoap implements Serializable {

	public static NotificationSettingsSoap toSoapModel(
		NotificationSettings model) {

		NotificationSettingsSoap soapModel = new NotificationSettingsSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setNotificationSettingsId(model.getNotificationSettingsId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setAppUserId(model.getAppUserId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setNews(model.isNews());
		soapModel.setEvents(model.isEvents());
		soapModel.setHighlights(model.isHighlights());
		soapModel.setBroadcastMessage(model.isBroadcastMessage());

		return soapModel;
	}

	public static NotificationSettingsSoap[] toSoapModels(
		NotificationSettings[] models) {

		NotificationSettingsSoap[] soapModels =
			new NotificationSettingsSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static NotificationSettingsSoap[][] toSoapModels(
		NotificationSettings[][] models) {

		NotificationSettingsSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels =
				new NotificationSettingsSoap[models.length][models[0].length];
		}
		else {
			soapModels = new NotificationSettingsSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static NotificationSettingsSoap[] toSoapModels(
		List<NotificationSettings> models) {

		List<NotificationSettingsSoap> soapModels =
			new ArrayList<NotificationSettingsSoap>(models.size());

		for (NotificationSettings model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(
			new NotificationSettingsSoap[soapModels.size()]);
	}

	public NotificationSettingsSoap() {
	}

	public long getPrimaryKey() {
		return _notificationSettingsId;
	}

	public void setPrimaryKey(long pk) {
		setNotificationSettingsId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getNotificationSettingsId() {
		return _notificationSettingsId;
	}

	public void setNotificationSettingsId(long notificationSettingsId) {
		_notificationSettingsId = notificationSettingsId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public int getAppUserId() {
		return _appUserId;
	}

	public void setAppUserId(int appUserId) {
		_appUserId = appUserId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public boolean getNews() {
		return _news;
	}

	public boolean isNews() {
		return _news;
	}

	public void setNews(boolean news) {
		_news = news;
	}

	public boolean getEvents() {
		return _events;
	}

	public boolean isEvents() {
		return _events;
	}

	public void setEvents(boolean events) {
		_events = events;
	}

	public boolean getHighlights() {
		return _highlights;
	}

	public boolean isHighlights() {
		return _highlights;
	}

	public void setHighlights(boolean highlights) {
		_highlights = highlights;
	}

	public boolean getBroadcastMessage() {
		return _broadcastMessage;
	}

	public boolean isBroadcastMessage() {
		return _broadcastMessage;
	}

	public void setBroadcastMessage(boolean broadcastMessage) {
		_broadcastMessage = broadcastMessage;
	}

	private String _uuid;
	private long _notificationSettingsId;
	private long _groupId;
	private int _appUserId;
	private long _companyId;
	private Date _createDate;
	private Date _modifiedDate;
	private boolean _news;
	private boolean _events;
	private boolean _highlights;
	private boolean _broadcastMessage;

}