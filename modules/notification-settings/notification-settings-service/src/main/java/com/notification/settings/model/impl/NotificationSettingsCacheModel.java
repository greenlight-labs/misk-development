/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.notification.settings.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import com.notification.settings.model.NotificationSettings;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing NotificationSettings in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class NotificationSettingsCacheModel
	implements CacheModel<NotificationSettings>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof NotificationSettingsCacheModel)) {
			return false;
		}

		NotificationSettingsCacheModel notificationSettingsCacheModel =
			(NotificationSettingsCacheModel)object;

		if (notificationSettingsId ==
				notificationSettingsCacheModel.notificationSettingsId) {

			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, notificationSettingsId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", notificationSettingsId=");
		sb.append(notificationSettingsId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", appUserId=");
		sb.append(appUserId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", news=");
		sb.append(news);
		sb.append(", events=");
		sb.append(events);
		sb.append(", highlights=");
		sb.append(highlights);
		sb.append(", broadcastMessage=");
		sb.append(broadcastMessage);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public NotificationSettings toEntityModel() {
		NotificationSettingsImpl notificationSettingsImpl =
			new NotificationSettingsImpl();

		if (uuid == null) {
			notificationSettingsImpl.setUuid("");
		}
		else {
			notificationSettingsImpl.setUuid(uuid);
		}

		notificationSettingsImpl.setNotificationSettingsId(
			notificationSettingsId);
		notificationSettingsImpl.setGroupId(groupId);
		notificationSettingsImpl.setAppUserId(appUserId);
		notificationSettingsImpl.setCompanyId(companyId);

		if (createDate == Long.MIN_VALUE) {
			notificationSettingsImpl.setCreateDate(null);
		}
		else {
			notificationSettingsImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			notificationSettingsImpl.setModifiedDate(null);
		}
		else {
			notificationSettingsImpl.setModifiedDate(new Date(modifiedDate));
		}

		notificationSettingsImpl.setNews(news);
		notificationSettingsImpl.setEvents(events);
		notificationSettingsImpl.setHighlights(highlights);
		notificationSettingsImpl.setBroadcastMessage(broadcastMessage);

		notificationSettingsImpl.resetOriginalValues();

		return notificationSettingsImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		notificationSettingsId = objectInput.readLong();

		groupId = objectInput.readLong();

		appUserId = objectInput.readInt();

		companyId = objectInput.readLong();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		news = objectInput.readBoolean();

		events = objectInput.readBoolean();

		highlights = objectInput.readBoolean();

		broadcastMessage = objectInput.readBoolean();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(notificationSettingsId);

		objectOutput.writeLong(groupId);

		objectOutput.writeInt(appUserId);

		objectOutput.writeLong(companyId);
		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeBoolean(news);

		objectOutput.writeBoolean(events);

		objectOutput.writeBoolean(highlights);

		objectOutput.writeBoolean(broadcastMessage);
	}

	public String uuid;
	public long notificationSettingsId;
	public long groupId;
	public int appUserId;
	public long companyId;
	public long createDate;
	public long modifiedDate;
	public boolean news;
	public boolean events;
	public boolean highlights;
	public boolean broadcastMessage;

}