package com.notification.settings.constants;

public class NotificationSettingsKeys {
	public static final String NOTIFICATION_TYPE_NEWS = "news";
	public static final String NOTIFICATION_TYPE_EVENT = "event";
	public static final String NOTIFICATION_TYPE_EVENT_UPDATE = "eventUpdate";
	public static final String NOTIFICATION_TYPE_HIGHLIGHT = "highlight";
	public static final String NOTIFICATION_TYPE_BROADCAST_MESSAGE = "broadcastMessage";

}
