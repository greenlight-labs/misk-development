/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.notification.settings.service.persistence.impl;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.configuration.Configuration;
import com.liferay.portal.kernel.dao.orm.ArgumentsResolver;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.MapUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;

import com.notification.settings.exception.NoSuchNotificationSettingsException;
import com.notification.settings.model.NotificationSettings;
import com.notification.settings.model.impl.NotificationSettingsImpl;
import com.notification.settings.model.impl.NotificationSettingsModelImpl;
import com.notification.settings.service.persistence.NotificationSettingsPersistence;
import com.notification.settings.service.persistence.NotificationSettingsUtil;
import com.notification.settings.service.persistence.impl.constants.NotificationSettingsPersistenceConstants;

import java.io.Serializable;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.sql.DataSource;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 * The persistence implementation for the notification settings service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(service = NotificationSettingsPersistence.class)
public class NotificationSettingsPersistenceImpl
	extends BasePersistenceImpl<NotificationSettings>
	implements NotificationSettingsPersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>NotificationSettingsUtil</code> to access the notification settings persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		NotificationSettingsImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathWithPaginationFindByUuid;
	private FinderPath _finderPathWithoutPaginationFindByUuid;
	private FinderPath _finderPathCountByUuid;

	/**
	 * Returns all the notification settingses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching notification settingses
	 */
	@Override
	public List<NotificationSettings> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the notification settingses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @return the range of matching notification settingses
	 */
	@Override
	public List<NotificationSettings> findByUuid(
		String uuid, int start, int end) {

		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the notification settingses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching notification settingses
	 */
	@Override
	public List<NotificationSettings> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<NotificationSettings> orderByComparator) {

		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the notification settingses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching notification settingses
	 */
	@Override
	public List<NotificationSettings> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<NotificationSettings> orderByComparator,
		boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid;
				finderArgs = new Object[] {uuid};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid;
			finderArgs = new Object[] {uuid, start, end, orderByComparator};
		}

		List<NotificationSettings> list = null;

		if (useFinderCache) {
			list = (List<NotificationSettings>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (NotificationSettings notificationSettings : list) {
					if (!uuid.equals(notificationSettings.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_NOTIFICATIONSETTINGS_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(NotificationSettingsModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				list = (List<NotificationSettings>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first notification settings in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	@Override
	public NotificationSettings findByUuid_First(
			String uuid,
			OrderByComparator<NotificationSettings> orderByComparator)
		throws NoSuchNotificationSettingsException {

		NotificationSettings notificationSettings = fetchByUuid_First(
			uuid, orderByComparator);

		if (notificationSettings != null) {
			return notificationSettings;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchNotificationSettingsException(sb.toString());
	}

	/**
	 * Returns the first notification settings in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	@Override
	public NotificationSettings fetchByUuid_First(
		String uuid,
		OrderByComparator<NotificationSettings> orderByComparator) {

		List<NotificationSettings> list = findByUuid(
			uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last notification settings in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	@Override
	public NotificationSettings findByUuid_Last(
			String uuid,
			OrderByComparator<NotificationSettings> orderByComparator)
		throws NoSuchNotificationSettingsException {

		NotificationSettings notificationSettings = fetchByUuid_Last(
			uuid, orderByComparator);

		if (notificationSettings != null) {
			return notificationSettings;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchNotificationSettingsException(sb.toString());
	}

	/**
	 * Returns the last notification settings in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	@Override
	public NotificationSettings fetchByUuid_Last(
		String uuid,
		OrderByComparator<NotificationSettings> orderByComparator) {

		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<NotificationSettings> list = findByUuid(
			uuid, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the notification settingses before and after the current notification settings in the ordered set where uuid = &#63;.
	 *
	 * @param notificationSettingsId the primary key of the current notification settings
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next notification settings
	 * @throws NoSuchNotificationSettingsException if a notification settings with the primary key could not be found
	 */
	@Override
	public NotificationSettings[] findByUuid_PrevAndNext(
			long notificationSettingsId, String uuid,
			OrderByComparator<NotificationSettings> orderByComparator)
		throws NoSuchNotificationSettingsException {

		uuid = Objects.toString(uuid, "");

		NotificationSettings notificationSettings = findByPrimaryKey(
			notificationSettingsId);

		Session session = null;

		try {
			session = openSession();

			NotificationSettings[] array = new NotificationSettingsImpl[3];

			array[0] = getByUuid_PrevAndNext(
				session, notificationSettings, uuid, orderByComparator, true);

			array[1] = notificationSettings;

			array[2] = getByUuid_PrevAndNext(
				session, notificationSettings, uuid, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected NotificationSettings getByUuid_PrevAndNext(
		Session session, NotificationSettings notificationSettings, String uuid,
		OrderByComparator<NotificationSettings> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_NOTIFICATIONSETTINGS_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(NotificationSettingsModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						notificationSettings)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<NotificationSettings> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the notification settingses where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (NotificationSettings notificationSettings :
				findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(notificationSettings);
		}
	}

	/**
	 * Returns the number of notification settingses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching notification settingses
	 */
	@Override
	public int countByUuid(String uuid) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid;

		Object[] finderArgs = new Object[] {uuid};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_NOTIFICATIONSETTINGS_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_2 =
		"notificationSettings.uuid = ?";

	private static final String _FINDER_COLUMN_UUID_UUID_3 =
		"(notificationSettings.uuid IS NULL OR notificationSettings.uuid = '')";

	private FinderPath _finderPathFetchByUUID_G;
	private FinderPath _finderPathCountByUUID_G;

	/**
	 * Returns the notification settings where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchNotificationSettingsException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	@Override
	public NotificationSettings findByUUID_G(String uuid, long groupId)
		throws NoSuchNotificationSettingsException {

		NotificationSettings notificationSettings = fetchByUUID_G(
			uuid, groupId);

		if (notificationSettings == null) {
			StringBundler sb = new StringBundler(6);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("uuid=");
			sb.append(uuid);

			sb.append(", groupId=");
			sb.append(groupId);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchNotificationSettingsException(sb.toString());
		}

		return notificationSettings;
	}

	/**
	 * Returns the notification settings where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	@Override
	public NotificationSettings fetchByUUID_G(String uuid, long groupId) {
		return fetchByUUID_G(uuid, groupId, true);
	}

	/**
	 * Returns the notification settings where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	@Override
	public NotificationSettings fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {uuid, groupId};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByUUID_G, finderArgs, this);
		}

		if (result instanceof NotificationSettings) {
			NotificationSettings notificationSettings =
				(NotificationSettings)result;

			if (!Objects.equals(uuid, notificationSettings.getUuid()) ||
				(groupId != notificationSettings.getGroupId())) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_SQL_SELECT_NOTIFICATIONSETTINGS_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(groupId);

				List<NotificationSettings> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache) {
						finderCache.putResult(
							_finderPathFetchByUUID_G, finderArgs, list);
					}
				}
				else {
					NotificationSettings notificationSettings = list.get(0);

					result = notificationSettings;

					cacheResult(notificationSettings);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (NotificationSettings)result;
		}
	}

	/**
	 * Removes the notification settings where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the notification settings that was removed
	 */
	@Override
	public NotificationSettings removeByUUID_G(String uuid, long groupId)
		throws NoSuchNotificationSettingsException {

		NotificationSettings notificationSettings = findByUUID_G(uuid, groupId);

		return remove(notificationSettings);
	}

	/**
	 * Returns the number of notification settingses where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching notification settingses
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUUID_G;

		Object[] finderArgs = new Object[] {uuid, groupId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_NOTIFICATIONSETTINGS_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(groupId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_2 =
		"notificationSettings.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_G_UUID_3 =
		"(notificationSettings.uuid IS NULL OR notificationSettings.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 =
		"notificationSettings.groupId = ?";

	private FinderPath _finderPathWithPaginationFindByUuid_C;
	private FinderPath _finderPathWithoutPaginationFindByUuid_C;
	private FinderPath _finderPathCountByUuid_C;

	/**
	 * Returns all the notification settingses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching notification settingses
	 */
	@Override
	public List<NotificationSettings> findByUuid_C(
		String uuid, long companyId) {

		return findByUuid_C(
			uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the notification settingses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @return the range of matching notification settingses
	 */
	@Override
	public List<NotificationSettings> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the notification settingses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching notification settingses
	 */
	@Override
	public List<NotificationSettings> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<NotificationSettings> orderByComparator) {

		return findByUuid_C(
			uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the notification settingses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching notification settingses
	 */
	@Override
	public List<NotificationSettings> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<NotificationSettings> orderByComparator,
		boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid_C;
				finderArgs = new Object[] {uuid, companyId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid_C;
			finderArgs = new Object[] {
				uuid, companyId, start, end, orderByComparator
			};
		}

		List<NotificationSettings> list = null;

		if (useFinderCache) {
			list = (List<NotificationSettings>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (NotificationSettings notificationSettings : list) {
					if (!uuid.equals(notificationSettings.getUuid()) ||
						(companyId != notificationSettings.getCompanyId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_NOTIFICATIONSETTINGS_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(NotificationSettingsModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				list = (List<NotificationSettings>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first notification settings in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	@Override
	public NotificationSettings findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<NotificationSettings> orderByComparator)
		throws NoSuchNotificationSettingsException {

		NotificationSettings notificationSettings = fetchByUuid_C_First(
			uuid, companyId, orderByComparator);

		if (notificationSettings != null) {
			return notificationSettings;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchNotificationSettingsException(sb.toString());
	}

	/**
	 * Returns the first notification settings in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	@Override
	public NotificationSettings fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<NotificationSettings> orderByComparator) {

		List<NotificationSettings> list = findByUuid_C(
			uuid, companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last notification settings in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	@Override
	public NotificationSettings findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<NotificationSettings> orderByComparator)
		throws NoSuchNotificationSettingsException {

		NotificationSettings notificationSettings = fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);

		if (notificationSettings != null) {
			return notificationSettings;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchNotificationSettingsException(sb.toString());
	}

	/**
	 * Returns the last notification settings in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	@Override
	public NotificationSettings fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<NotificationSettings> orderByComparator) {

		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<NotificationSettings> list = findByUuid_C(
			uuid, companyId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the notification settingses before and after the current notification settings in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param notificationSettingsId the primary key of the current notification settings
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next notification settings
	 * @throws NoSuchNotificationSettingsException if a notification settings with the primary key could not be found
	 */
	@Override
	public NotificationSettings[] findByUuid_C_PrevAndNext(
			long notificationSettingsId, String uuid, long companyId,
			OrderByComparator<NotificationSettings> orderByComparator)
		throws NoSuchNotificationSettingsException {

		uuid = Objects.toString(uuid, "");

		NotificationSettings notificationSettings = findByPrimaryKey(
			notificationSettingsId);

		Session session = null;

		try {
			session = openSession();

			NotificationSettings[] array = new NotificationSettingsImpl[3];

			array[0] = getByUuid_C_PrevAndNext(
				session, notificationSettings, uuid, companyId,
				orderByComparator, true);

			array[1] = notificationSettings;

			array[2] = getByUuid_C_PrevAndNext(
				session, notificationSettings, uuid, companyId,
				orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected NotificationSettings getByUuid_C_PrevAndNext(
		Session session, NotificationSettings notificationSettings, String uuid,
		long companyId,
		OrderByComparator<NotificationSettings> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_NOTIFICATIONSETTINGS_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(NotificationSettingsModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		queryPos.add(companyId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						notificationSettings)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<NotificationSettings> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the notification settingses where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (NotificationSettings notificationSettings :
				findByUuid_C(
					uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(notificationSettings);
		}
	}

	/**
	 * Returns the number of notification settingses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching notification settingses
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid_C;

		Object[] finderArgs = new Object[] {uuid, companyId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_NOTIFICATIONSETTINGS_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_2 =
		"notificationSettings.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_C_UUID_3 =
		"(notificationSettings.uuid IS NULL OR notificationSettings.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 =
		"notificationSettings.companyId = ?";

	private FinderPath _finderPathFetchByAppUserId;
	private FinderPath _finderPathCountByAppUserId;

	/**
	 * Returns the notification settings where appUserId = &#63; or throws a <code>NoSuchNotificationSettingsException</code> if it could not be found.
	 *
	 * @param appUserId the app user ID
	 * @return the matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	@Override
	public NotificationSettings findByAppUserId(int appUserId)
		throws NoSuchNotificationSettingsException {

		NotificationSettings notificationSettings = fetchByAppUserId(appUserId);

		if (notificationSettings == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("appUserId=");
			sb.append(appUserId);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchNotificationSettingsException(sb.toString());
		}

		return notificationSettings;
	}

	/**
	 * Returns the notification settings where appUserId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param appUserId the app user ID
	 * @return the matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	@Override
	public NotificationSettings fetchByAppUserId(int appUserId) {
		return fetchByAppUserId(appUserId, true);
	}

	/**
	 * Returns the notification settings where appUserId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param appUserId the app user ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	@Override
	public NotificationSettings fetchByAppUserId(
		int appUserId, boolean useFinderCache) {

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {appUserId};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByAppUserId, finderArgs, this);
		}

		if (result instanceof NotificationSettings) {
			NotificationSettings notificationSettings =
				(NotificationSettings)result;

			if (appUserId != notificationSettings.getAppUserId()) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_SELECT_NOTIFICATIONSETTINGS_WHERE);

			sb.append(_FINDER_COLUMN_APPUSERID_APPUSERID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(appUserId);

				List<NotificationSettings> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache) {
						finderCache.putResult(
							_finderPathFetchByAppUserId, finderArgs, list);
					}
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							if (!useFinderCache) {
								finderArgs = new Object[] {appUserId};
							}

							_log.warn(
								"NotificationSettingsPersistenceImpl.fetchByAppUserId(int, boolean) with parameters (" +
									StringUtil.merge(finderArgs) +
										") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					NotificationSettings notificationSettings = list.get(0);

					result = notificationSettings;

					cacheResult(notificationSettings);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (NotificationSettings)result;
		}
	}

	/**
	 * Removes the notification settings where appUserId = &#63; from the database.
	 *
	 * @param appUserId the app user ID
	 * @return the notification settings that was removed
	 */
	@Override
	public NotificationSettings removeByAppUserId(int appUserId)
		throws NoSuchNotificationSettingsException {

		NotificationSettings notificationSettings = findByAppUserId(appUserId);

		return remove(notificationSettings);
	}

	/**
	 * Returns the number of notification settingses where appUserId = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @return the number of matching notification settingses
	 */
	@Override
	public int countByAppUserId(int appUserId) {
		FinderPath finderPath = _finderPathCountByAppUserId;

		Object[] finderArgs = new Object[] {appUserId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_NOTIFICATIONSETTINGS_WHERE);

			sb.append(_FINDER_COLUMN_APPUSERID_APPUSERID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(appUserId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_APPUSERID_APPUSERID_2 =
		"notificationSettings.appUserId = ?";

	private FinderPath _finderPathWithPaginationFindByNews;
	private FinderPath _finderPathWithoutPaginationFindByNews;
	private FinderPath _finderPathCountByNews;

	/**
	 * Returns all the notification settingses where news = &#63;.
	 *
	 * @param news the news
	 * @return the matching notification settingses
	 */
	@Override
	public List<NotificationSettings> findByNews(boolean news) {
		return findByNews(news, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the notification settingses where news = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param news the news
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @return the range of matching notification settingses
	 */
	@Override
	public List<NotificationSettings> findByNews(
		boolean news, int start, int end) {

		return findByNews(news, start, end, null);
	}

	/**
	 * Returns an ordered range of all the notification settingses where news = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param news the news
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching notification settingses
	 */
	@Override
	public List<NotificationSettings> findByNews(
		boolean news, int start, int end,
		OrderByComparator<NotificationSettings> orderByComparator) {

		return findByNews(news, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the notification settingses where news = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param news the news
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching notification settingses
	 */
	@Override
	public List<NotificationSettings> findByNews(
		boolean news, int start, int end,
		OrderByComparator<NotificationSettings> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByNews;
				finderArgs = new Object[] {news};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByNews;
			finderArgs = new Object[] {news, start, end, orderByComparator};
		}

		List<NotificationSettings> list = null;

		if (useFinderCache) {
			list = (List<NotificationSettings>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (NotificationSettings notificationSettings : list) {
					if (news != notificationSettings.isNews()) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_NOTIFICATIONSETTINGS_WHERE);

			sb.append(_FINDER_COLUMN_NEWS_NEWS_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(NotificationSettingsModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(news);

				list = (List<NotificationSettings>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first notification settings in the ordered set where news = &#63;.
	 *
	 * @param news the news
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	@Override
	public NotificationSettings findByNews_First(
			boolean news,
			OrderByComparator<NotificationSettings> orderByComparator)
		throws NoSuchNotificationSettingsException {

		NotificationSettings notificationSettings = fetchByNews_First(
			news, orderByComparator);

		if (notificationSettings != null) {
			return notificationSettings;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("news=");
		sb.append(news);

		sb.append("}");

		throw new NoSuchNotificationSettingsException(sb.toString());
	}

	/**
	 * Returns the first notification settings in the ordered set where news = &#63;.
	 *
	 * @param news the news
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	@Override
	public NotificationSettings fetchByNews_First(
		boolean news,
		OrderByComparator<NotificationSettings> orderByComparator) {

		List<NotificationSettings> list = findByNews(
			news, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last notification settings in the ordered set where news = &#63;.
	 *
	 * @param news the news
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	@Override
	public NotificationSettings findByNews_Last(
			boolean news,
			OrderByComparator<NotificationSettings> orderByComparator)
		throws NoSuchNotificationSettingsException {

		NotificationSettings notificationSettings = fetchByNews_Last(
			news, orderByComparator);

		if (notificationSettings != null) {
			return notificationSettings;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("news=");
		sb.append(news);

		sb.append("}");

		throw new NoSuchNotificationSettingsException(sb.toString());
	}

	/**
	 * Returns the last notification settings in the ordered set where news = &#63;.
	 *
	 * @param news the news
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	@Override
	public NotificationSettings fetchByNews_Last(
		boolean news,
		OrderByComparator<NotificationSettings> orderByComparator) {

		int count = countByNews(news);

		if (count == 0) {
			return null;
		}

		List<NotificationSettings> list = findByNews(
			news, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the notification settingses before and after the current notification settings in the ordered set where news = &#63;.
	 *
	 * @param notificationSettingsId the primary key of the current notification settings
	 * @param news the news
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next notification settings
	 * @throws NoSuchNotificationSettingsException if a notification settings with the primary key could not be found
	 */
	@Override
	public NotificationSettings[] findByNews_PrevAndNext(
			long notificationSettingsId, boolean news,
			OrderByComparator<NotificationSettings> orderByComparator)
		throws NoSuchNotificationSettingsException {

		NotificationSettings notificationSettings = findByPrimaryKey(
			notificationSettingsId);

		Session session = null;

		try {
			session = openSession();

			NotificationSettings[] array = new NotificationSettingsImpl[3];

			array[0] = getByNews_PrevAndNext(
				session, notificationSettings, news, orderByComparator, true);

			array[1] = notificationSettings;

			array[2] = getByNews_PrevAndNext(
				session, notificationSettings, news, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected NotificationSettings getByNews_PrevAndNext(
		Session session, NotificationSettings notificationSettings,
		boolean news, OrderByComparator<NotificationSettings> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_NOTIFICATIONSETTINGS_WHERE);

		sb.append(_FINDER_COLUMN_NEWS_NEWS_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(NotificationSettingsModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(news);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						notificationSettings)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<NotificationSettings> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the notification settingses where news = &#63; from the database.
	 *
	 * @param news the news
	 */
	@Override
	public void removeByNews(boolean news) {
		for (NotificationSettings notificationSettings :
				findByNews(news, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(notificationSettings);
		}
	}

	/**
	 * Returns the number of notification settingses where news = &#63;.
	 *
	 * @param news the news
	 * @return the number of matching notification settingses
	 */
	@Override
	public int countByNews(boolean news) {
		FinderPath finderPath = _finderPathCountByNews;

		Object[] finderArgs = new Object[] {news};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_NOTIFICATIONSETTINGS_WHERE);

			sb.append(_FINDER_COLUMN_NEWS_NEWS_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(news);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_NEWS_NEWS_2 =
		"notificationSettings.news = ?";

	private FinderPath _finderPathWithPaginationFindByEvents;
	private FinderPath _finderPathWithoutPaginationFindByEvents;
	private FinderPath _finderPathCountByEvents;

	/**
	 * Returns all the notification settingses where events = &#63;.
	 *
	 * @param events the events
	 * @return the matching notification settingses
	 */
	@Override
	public List<NotificationSettings> findByEvents(boolean events) {
		return findByEvents(events, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the notification settingses where events = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param events the events
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @return the range of matching notification settingses
	 */
	@Override
	public List<NotificationSettings> findByEvents(
		boolean events, int start, int end) {

		return findByEvents(events, start, end, null);
	}

	/**
	 * Returns an ordered range of all the notification settingses where events = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param events the events
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching notification settingses
	 */
	@Override
	public List<NotificationSettings> findByEvents(
		boolean events, int start, int end,
		OrderByComparator<NotificationSettings> orderByComparator) {

		return findByEvents(events, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the notification settingses where events = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param events the events
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching notification settingses
	 */
	@Override
	public List<NotificationSettings> findByEvents(
		boolean events, int start, int end,
		OrderByComparator<NotificationSettings> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByEvents;
				finderArgs = new Object[] {events};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByEvents;
			finderArgs = new Object[] {events, start, end, orderByComparator};
		}

		List<NotificationSettings> list = null;

		if (useFinderCache) {
			list = (List<NotificationSettings>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (NotificationSettings notificationSettings : list) {
					if (events != notificationSettings.isEvents()) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_NOTIFICATIONSETTINGS_WHERE);

			sb.append(_FINDER_COLUMN_EVENTS_EVENTS_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(NotificationSettingsModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(events);

				list = (List<NotificationSettings>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first notification settings in the ordered set where events = &#63;.
	 *
	 * @param events the events
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	@Override
	public NotificationSettings findByEvents_First(
			boolean events,
			OrderByComparator<NotificationSettings> orderByComparator)
		throws NoSuchNotificationSettingsException {

		NotificationSettings notificationSettings = fetchByEvents_First(
			events, orderByComparator);

		if (notificationSettings != null) {
			return notificationSettings;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("events=");
		sb.append(events);

		sb.append("}");

		throw new NoSuchNotificationSettingsException(sb.toString());
	}

	/**
	 * Returns the first notification settings in the ordered set where events = &#63;.
	 *
	 * @param events the events
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	@Override
	public NotificationSettings fetchByEvents_First(
		boolean events,
		OrderByComparator<NotificationSettings> orderByComparator) {

		List<NotificationSettings> list = findByEvents(
			events, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last notification settings in the ordered set where events = &#63;.
	 *
	 * @param events the events
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	@Override
	public NotificationSettings findByEvents_Last(
			boolean events,
			OrderByComparator<NotificationSettings> orderByComparator)
		throws NoSuchNotificationSettingsException {

		NotificationSettings notificationSettings = fetchByEvents_Last(
			events, orderByComparator);

		if (notificationSettings != null) {
			return notificationSettings;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("events=");
		sb.append(events);

		sb.append("}");

		throw new NoSuchNotificationSettingsException(sb.toString());
	}

	/**
	 * Returns the last notification settings in the ordered set where events = &#63;.
	 *
	 * @param events the events
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	@Override
	public NotificationSettings fetchByEvents_Last(
		boolean events,
		OrderByComparator<NotificationSettings> orderByComparator) {

		int count = countByEvents(events);

		if (count == 0) {
			return null;
		}

		List<NotificationSettings> list = findByEvents(
			events, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the notification settingses before and after the current notification settings in the ordered set where events = &#63;.
	 *
	 * @param notificationSettingsId the primary key of the current notification settings
	 * @param events the events
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next notification settings
	 * @throws NoSuchNotificationSettingsException if a notification settings with the primary key could not be found
	 */
	@Override
	public NotificationSettings[] findByEvents_PrevAndNext(
			long notificationSettingsId, boolean events,
			OrderByComparator<NotificationSettings> orderByComparator)
		throws NoSuchNotificationSettingsException {

		NotificationSettings notificationSettings = findByPrimaryKey(
			notificationSettingsId);

		Session session = null;

		try {
			session = openSession();

			NotificationSettings[] array = new NotificationSettingsImpl[3];

			array[0] = getByEvents_PrevAndNext(
				session, notificationSettings, events, orderByComparator, true);

			array[1] = notificationSettings;

			array[2] = getByEvents_PrevAndNext(
				session, notificationSettings, events, orderByComparator,
				false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected NotificationSettings getByEvents_PrevAndNext(
		Session session, NotificationSettings notificationSettings,
		boolean events,
		OrderByComparator<NotificationSettings> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_NOTIFICATIONSETTINGS_WHERE);

		sb.append(_FINDER_COLUMN_EVENTS_EVENTS_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(NotificationSettingsModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(events);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						notificationSettings)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<NotificationSettings> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the notification settingses where events = &#63; from the database.
	 *
	 * @param events the events
	 */
	@Override
	public void removeByEvents(boolean events) {
		for (NotificationSettings notificationSettings :
				findByEvents(
					events, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(notificationSettings);
		}
	}

	/**
	 * Returns the number of notification settingses where events = &#63;.
	 *
	 * @param events the events
	 * @return the number of matching notification settingses
	 */
	@Override
	public int countByEvents(boolean events) {
		FinderPath finderPath = _finderPathCountByEvents;

		Object[] finderArgs = new Object[] {events};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_NOTIFICATIONSETTINGS_WHERE);

			sb.append(_FINDER_COLUMN_EVENTS_EVENTS_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(events);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_EVENTS_EVENTS_2 =
		"notificationSettings.events = ?";

	private FinderPath _finderPathWithPaginationFindByHighlights;
	private FinderPath _finderPathWithoutPaginationFindByHighlights;
	private FinderPath _finderPathCountByHighlights;

	/**
	 * Returns all the notification settingses where highlights = &#63;.
	 *
	 * @param highlights the highlights
	 * @return the matching notification settingses
	 */
	@Override
	public List<NotificationSettings> findByHighlights(boolean highlights) {
		return findByHighlights(
			highlights, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the notification settingses where highlights = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param highlights the highlights
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @return the range of matching notification settingses
	 */
	@Override
	public List<NotificationSettings> findByHighlights(
		boolean highlights, int start, int end) {

		return findByHighlights(highlights, start, end, null);
	}

	/**
	 * Returns an ordered range of all the notification settingses where highlights = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param highlights the highlights
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching notification settingses
	 */
	@Override
	public List<NotificationSettings> findByHighlights(
		boolean highlights, int start, int end,
		OrderByComparator<NotificationSettings> orderByComparator) {

		return findByHighlights(
			highlights, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the notification settingses where highlights = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param highlights the highlights
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching notification settingses
	 */
	@Override
	public List<NotificationSettings> findByHighlights(
		boolean highlights, int start, int end,
		OrderByComparator<NotificationSettings> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByHighlights;
				finderArgs = new Object[] {highlights};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByHighlights;
			finderArgs = new Object[] {
				highlights, start, end, orderByComparator
			};
		}

		List<NotificationSettings> list = null;

		if (useFinderCache) {
			list = (List<NotificationSettings>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (NotificationSettings notificationSettings : list) {
					if (highlights != notificationSettings.isHighlights()) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_NOTIFICATIONSETTINGS_WHERE);

			sb.append(_FINDER_COLUMN_HIGHLIGHTS_HIGHLIGHTS_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(NotificationSettingsModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(highlights);

				list = (List<NotificationSettings>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first notification settings in the ordered set where highlights = &#63;.
	 *
	 * @param highlights the highlights
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	@Override
	public NotificationSettings findByHighlights_First(
			boolean highlights,
			OrderByComparator<NotificationSettings> orderByComparator)
		throws NoSuchNotificationSettingsException {

		NotificationSettings notificationSettings = fetchByHighlights_First(
			highlights, orderByComparator);

		if (notificationSettings != null) {
			return notificationSettings;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("highlights=");
		sb.append(highlights);

		sb.append("}");

		throw new NoSuchNotificationSettingsException(sb.toString());
	}

	/**
	 * Returns the first notification settings in the ordered set where highlights = &#63;.
	 *
	 * @param highlights the highlights
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	@Override
	public NotificationSettings fetchByHighlights_First(
		boolean highlights,
		OrderByComparator<NotificationSettings> orderByComparator) {

		List<NotificationSettings> list = findByHighlights(
			highlights, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last notification settings in the ordered set where highlights = &#63;.
	 *
	 * @param highlights the highlights
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	@Override
	public NotificationSettings findByHighlights_Last(
			boolean highlights,
			OrderByComparator<NotificationSettings> orderByComparator)
		throws NoSuchNotificationSettingsException {

		NotificationSettings notificationSettings = fetchByHighlights_Last(
			highlights, orderByComparator);

		if (notificationSettings != null) {
			return notificationSettings;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("highlights=");
		sb.append(highlights);

		sb.append("}");

		throw new NoSuchNotificationSettingsException(sb.toString());
	}

	/**
	 * Returns the last notification settings in the ordered set where highlights = &#63;.
	 *
	 * @param highlights the highlights
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	@Override
	public NotificationSettings fetchByHighlights_Last(
		boolean highlights,
		OrderByComparator<NotificationSettings> orderByComparator) {

		int count = countByHighlights(highlights);

		if (count == 0) {
			return null;
		}

		List<NotificationSettings> list = findByHighlights(
			highlights, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the notification settingses before and after the current notification settings in the ordered set where highlights = &#63;.
	 *
	 * @param notificationSettingsId the primary key of the current notification settings
	 * @param highlights the highlights
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next notification settings
	 * @throws NoSuchNotificationSettingsException if a notification settings with the primary key could not be found
	 */
	@Override
	public NotificationSettings[] findByHighlights_PrevAndNext(
			long notificationSettingsId, boolean highlights,
			OrderByComparator<NotificationSettings> orderByComparator)
		throws NoSuchNotificationSettingsException {

		NotificationSettings notificationSettings = findByPrimaryKey(
			notificationSettingsId);

		Session session = null;

		try {
			session = openSession();

			NotificationSettings[] array = new NotificationSettingsImpl[3];

			array[0] = getByHighlights_PrevAndNext(
				session, notificationSettings, highlights, orderByComparator,
				true);

			array[1] = notificationSettings;

			array[2] = getByHighlights_PrevAndNext(
				session, notificationSettings, highlights, orderByComparator,
				false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected NotificationSettings getByHighlights_PrevAndNext(
		Session session, NotificationSettings notificationSettings,
		boolean highlights,
		OrderByComparator<NotificationSettings> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_NOTIFICATIONSETTINGS_WHERE);

		sb.append(_FINDER_COLUMN_HIGHLIGHTS_HIGHLIGHTS_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(NotificationSettingsModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(highlights);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						notificationSettings)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<NotificationSettings> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the notification settingses where highlights = &#63; from the database.
	 *
	 * @param highlights the highlights
	 */
	@Override
	public void removeByHighlights(boolean highlights) {
		for (NotificationSettings notificationSettings :
				findByHighlights(
					highlights, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(notificationSettings);
		}
	}

	/**
	 * Returns the number of notification settingses where highlights = &#63;.
	 *
	 * @param highlights the highlights
	 * @return the number of matching notification settingses
	 */
	@Override
	public int countByHighlights(boolean highlights) {
		FinderPath finderPath = _finderPathCountByHighlights;

		Object[] finderArgs = new Object[] {highlights};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_NOTIFICATIONSETTINGS_WHERE);

			sb.append(_FINDER_COLUMN_HIGHLIGHTS_HIGHLIGHTS_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(highlights);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_HIGHLIGHTS_HIGHLIGHTS_2 =
		"notificationSettings.highlights = ?";

	private FinderPath _finderPathWithPaginationFindByBroadcastMessage;
	private FinderPath _finderPathWithoutPaginationFindByBroadcastMessage;
	private FinderPath _finderPathCountByBroadcastMessage;

	/**
	 * Returns all the notification settingses where broadcastMessage = &#63;.
	 *
	 * @param broadcastMessage the broadcast message
	 * @return the matching notification settingses
	 */
	@Override
	public List<NotificationSettings> findByBroadcastMessage(
		boolean broadcastMessage) {

		return findByBroadcastMessage(
			broadcastMessage, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the notification settingses where broadcastMessage = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param broadcastMessage the broadcast message
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @return the range of matching notification settingses
	 */
	@Override
	public List<NotificationSettings> findByBroadcastMessage(
		boolean broadcastMessage, int start, int end) {

		return findByBroadcastMessage(broadcastMessage, start, end, null);
	}

	/**
	 * Returns an ordered range of all the notification settingses where broadcastMessage = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param broadcastMessage the broadcast message
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching notification settingses
	 */
	@Override
	public List<NotificationSettings> findByBroadcastMessage(
		boolean broadcastMessage, int start, int end,
		OrderByComparator<NotificationSettings> orderByComparator) {

		return findByBroadcastMessage(
			broadcastMessage, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the notification settingses where broadcastMessage = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param broadcastMessage the broadcast message
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching notification settingses
	 */
	@Override
	public List<NotificationSettings> findByBroadcastMessage(
		boolean broadcastMessage, int start, int end,
		OrderByComparator<NotificationSettings> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByBroadcastMessage;
				finderArgs = new Object[] {broadcastMessage};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByBroadcastMessage;
			finderArgs = new Object[] {
				broadcastMessage, start, end, orderByComparator
			};
		}

		List<NotificationSettings> list = null;

		if (useFinderCache) {
			list = (List<NotificationSettings>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (NotificationSettings notificationSettings : list) {
					if (broadcastMessage !=
							notificationSettings.isBroadcastMessage()) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_NOTIFICATIONSETTINGS_WHERE);

			sb.append(_FINDER_COLUMN_BROADCASTMESSAGE_BROADCASTMESSAGE_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(NotificationSettingsModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(broadcastMessage);

				list = (List<NotificationSettings>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first notification settings in the ordered set where broadcastMessage = &#63;.
	 *
	 * @param broadcastMessage the broadcast message
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	@Override
	public NotificationSettings findByBroadcastMessage_First(
			boolean broadcastMessage,
			OrderByComparator<NotificationSettings> orderByComparator)
		throws NoSuchNotificationSettingsException {

		NotificationSettings notificationSettings =
			fetchByBroadcastMessage_First(broadcastMessage, orderByComparator);

		if (notificationSettings != null) {
			return notificationSettings;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("broadcastMessage=");
		sb.append(broadcastMessage);

		sb.append("}");

		throw new NoSuchNotificationSettingsException(sb.toString());
	}

	/**
	 * Returns the first notification settings in the ordered set where broadcastMessage = &#63;.
	 *
	 * @param broadcastMessage the broadcast message
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	@Override
	public NotificationSettings fetchByBroadcastMessage_First(
		boolean broadcastMessage,
		OrderByComparator<NotificationSettings> orderByComparator) {

		List<NotificationSettings> list = findByBroadcastMessage(
			broadcastMessage, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last notification settings in the ordered set where broadcastMessage = &#63;.
	 *
	 * @param broadcastMessage the broadcast message
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching notification settings
	 * @throws NoSuchNotificationSettingsException if a matching notification settings could not be found
	 */
	@Override
	public NotificationSettings findByBroadcastMessage_Last(
			boolean broadcastMessage,
			OrderByComparator<NotificationSettings> orderByComparator)
		throws NoSuchNotificationSettingsException {

		NotificationSettings notificationSettings =
			fetchByBroadcastMessage_Last(broadcastMessage, orderByComparator);

		if (notificationSettings != null) {
			return notificationSettings;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("broadcastMessage=");
		sb.append(broadcastMessage);

		sb.append("}");

		throw new NoSuchNotificationSettingsException(sb.toString());
	}

	/**
	 * Returns the last notification settings in the ordered set where broadcastMessage = &#63;.
	 *
	 * @param broadcastMessage the broadcast message
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching notification settings, or <code>null</code> if a matching notification settings could not be found
	 */
	@Override
	public NotificationSettings fetchByBroadcastMessage_Last(
		boolean broadcastMessage,
		OrderByComparator<NotificationSettings> orderByComparator) {

		int count = countByBroadcastMessage(broadcastMessage);

		if (count == 0) {
			return null;
		}

		List<NotificationSettings> list = findByBroadcastMessage(
			broadcastMessage, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the notification settingses before and after the current notification settings in the ordered set where broadcastMessage = &#63;.
	 *
	 * @param notificationSettingsId the primary key of the current notification settings
	 * @param broadcastMessage the broadcast message
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next notification settings
	 * @throws NoSuchNotificationSettingsException if a notification settings with the primary key could not be found
	 */
	@Override
	public NotificationSettings[] findByBroadcastMessage_PrevAndNext(
			long notificationSettingsId, boolean broadcastMessage,
			OrderByComparator<NotificationSettings> orderByComparator)
		throws NoSuchNotificationSettingsException {

		NotificationSettings notificationSettings = findByPrimaryKey(
			notificationSettingsId);

		Session session = null;

		try {
			session = openSession();

			NotificationSettings[] array = new NotificationSettingsImpl[3];

			array[0] = getByBroadcastMessage_PrevAndNext(
				session, notificationSettings, broadcastMessage,
				orderByComparator, true);

			array[1] = notificationSettings;

			array[2] = getByBroadcastMessage_PrevAndNext(
				session, notificationSettings, broadcastMessage,
				orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected NotificationSettings getByBroadcastMessage_PrevAndNext(
		Session session, NotificationSettings notificationSettings,
		boolean broadcastMessage,
		OrderByComparator<NotificationSettings> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_NOTIFICATIONSETTINGS_WHERE);

		sb.append(_FINDER_COLUMN_BROADCASTMESSAGE_BROADCASTMESSAGE_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(NotificationSettingsModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(broadcastMessage);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						notificationSettings)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<NotificationSettings> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the notification settingses where broadcastMessage = &#63; from the database.
	 *
	 * @param broadcastMessage the broadcast message
	 */
	@Override
	public void removeByBroadcastMessage(boolean broadcastMessage) {
		for (NotificationSettings notificationSettings :
				findByBroadcastMessage(
					broadcastMessage, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(notificationSettings);
		}
	}

	/**
	 * Returns the number of notification settingses where broadcastMessage = &#63;.
	 *
	 * @param broadcastMessage the broadcast message
	 * @return the number of matching notification settingses
	 */
	@Override
	public int countByBroadcastMessage(boolean broadcastMessage) {
		FinderPath finderPath = _finderPathCountByBroadcastMessage;

		Object[] finderArgs = new Object[] {broadcastMessage};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_NOTIFICATIONSETTINGS_WHERE);

			sb.append(_FINDER_COLUMN_BROADCASTMESSAGE_BROADCASTMESSAGE_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(broadcastMessage);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String
		_FINDER_COLUMN_BROADCASTMESSAGE_BROADCASTMESSAGE_2 =
			"notificationSettings.broadcastMessage = ?";

	public NotificationSettingsPersistenceImpl() {
		Map<String, String> dbColumnNames = new HashMap<String, String>();

		dbColumnNames.put("uuid", "uuid_");

		setDBColumnNames(dbColumnNames);

		setModelClass(NotificationSettings.class);

		setModelImplClass(NotificationSettingsImpl.class);
		setModelPKClass(long.class);
	}

	/**
	 * Caches the notification settings in the entity cache if it is enabled.
	 *
	 * @param notificationSettings the notification settings
	 */
	@Override
	public void cacheResult(NotificationSettings notificationSettings) {
		entityCache.putResult(
			NotificationSettingsImpl.class,
			notificationSettings.getPrimaryKey(), notificationSettings);

		finderCache.putResult(
			_finderPathFetchByUUID_G,
			new Object[] {
				notificationSettings.getUuid(),
				notificationSettings.getGroupId()
			},
			notificationSettings);

		finderCache.putResult(
			_finderPathFetchByAppUserId,
			new Object[] {notificationSettings.getAppUserId()},
			notificationSettings);
	}

	private int _valueObjectFinderCacheListThreshold;

	/**
	 * Caches the notification settingses in the entity cache if it is enabled.
	 *
	 * @param notificationSettingses the notification settingses
	 */
	@Override
	public void cacheResult(List<NotificationSettings> notificationSettingses) {
		if ((_valueObjectFinderCacheListThreshold == 0) ||
			((_valueObjectFinderCacheListThreshold > 0) &&
			 (notificationSettingses.size() >
				 _valueObjectFinderCacheListThreshold))) {

			return;
		}

		for (NotificationSettings notificationSettings :
				notificationSettingses) {

			if (entityCache.getResult(
					NotificationSettingsImpl.class,
					notificationSettings.getPrimaryKey()) == null) {

				cacheResult(notificationSettings);
			}
		}
	}

	/**
	 * Clears the cache for all notification settingses.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(NotificationSettingsImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the notification settings.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(NotificationSettings notificationSettings) {
		entityCache.removeResult(
			NotificationSettingsImpl.class, notificationSettings);
	}

	@Override
	public void clearCache(List<NotificationSettings> notificationSettingses) {
		for (NotificationSettings notificationSettings :
				notificationSettingses) {

			entityCache.removeResult(
				NotificationSettingsImpl.class, notificationSettings);
		}
	}

	@Override
	public void clearCache(Set<Serializable> primaryKeys) {
		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Serializable primaryKey : primaryKeys) {
			entityCache.removeResult(
				NotificationSettingsImpl.class, primaryKey);
		}
	}

	protected void cacheUniqueFindersCache(
		NotificationSettingsModelImpl notificationSettingsModelImpl) {

		Object[] args = new Object[] {
			notificationSettingsModelImpl.getUuid(),
			notificationSettingsModelImpl.getGroupId()
		};

		finderCache.putResult(
			_finderPathCountByUUID_G, args, Long.valueOf(1), false);
		finderCache.putResult(
			_finderPathFetchByUUID_G, args, notificationSettingsModelImpl,
			false);

		args = new Object[] {notificationSettingsModelImpl.getAppUserId()};

		finderCache.putResult(
			_finderPathCountByAppUserId, args, Long.valueOf(1), false);
		finderCache.putResult(
			_finderPathFetchByAppUserId, args, notificationSettingsModelImpl,
			false);
	}

	/**
	 * Creates a new notification settings with the primary key. Does not add the notification settings to the database.
	 *
	 * @param notificationSettingsId the primary key for the new notification settings
	 * @return the new notification settings
	 */
	@Override
	public NotificationSettings create(long notificationSettingsId) {
		NotificationSettings notificationSettings =
			new NotificationSettingsImpl();

		notificationSettings.setNew(true);
		notificationSettings.setPrimaryKey(notificationSettingsId);

		String uuid = PortalUUIDUtil.generate();

		notificationSettings.setUuid(uuid);

		notificationSettings.setCompanyId(CompanyThreadLocal.getCompanyId());

		return notificationSettings;
	}

	/**
	 * Removes the notification settings with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param notificationSettingsId the primary key of the notification settings
	 * @return the notification settings that was removed
	 * @throws NoSuchNotificationSettingsException if a notification settings with the primary key could not be found
	 */
	@Override
	public NotificationSettings remove(long notificationSettingsId)
		throws NoSuchNotificationSettingsException {

		return remove((Serializable)notificationSettingsId);
	}

	/**
	 * Removes the notification settings with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the notification settings
	 * @return the notification settings that was removed
	 * @throws NoSuchNotificationSettingsException if a notification settings with the primary key could not be found
	 */
	@Override
	public NotificationSettings remove(Serializable primaryKey)
		throws NoSuchNotificationSettingsException {

		Session session = null;

		try {
			session = openSession();

			NotificationSettings notificationSettings =
				(NotificationSettings)session.get(
					NotificationSettingsImpl.class, primaryKey);

			if (notificationSettings == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchNotificationSettingsException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(notificationSettings);
		}
		catch (NoSuchNotificationSettingsException noSuchEntityException) {
			throw noSuchEntityException;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected NotificationSettings removeImpl(
		NotificationSettings notificationSettings) {

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(notificationSettings)) {
				notificationSettings = (NotificationSettings)session.get(
					NotificationSettingsImpl.class,
					notificationSettings.getPrimaryKeyObj());
			}

			if (notificationSettings != null) {
				session.delete(notificationSettings);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		if (notificationSettings != null) {
			clearCache(notificationSettings);
		}

		return notificationSettings;
	}

	@Override
	public NotificationSettings updateImpl(
		NotificationSettings notificationSettings) {

		boolean isNew = notificationSettings.isNew();

		if (!(notificationSettings instanceof NotificationSettingsModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(notificationSettings.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(
					notificationSettings);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in notificationSettings proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom NotificationSettings implementation " +
					notificationSettings.getClass());
		}

		NotificationSettingsModelImpl notificationSettingsModelImpl =
			(NotificationSettingsModelImpl)notificationSettings;

		if (Validator.isNull(notificationSettings.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			notificationSettings.setUuid(uuid);
		}

		ServiceContext serviceContext =
			ServiceContextThreadLocal.getServiceContext();

		Date date = new Date();

		if (isNew && (notificationSettings.getCreateDate() == null)) {
			if (serviceContext == null) {
				notificationSettings.setCreateDate(date);
			}
			else {
				notificationSettings.setCreateDate(
					serviceContext.getCreateDate(date));
			}
		}

		if (!notificationSettingsModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				notificationSettings.setModifiedDate(date);
			}
			else {
				notificationSettings.setModifiedDate(
					serviceContext.getModifiedDate(date));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (isNew) {
				session.save(notificationSettings);
			}
			else {
				notificationSettings = (NotificationSettings)session.merge(
					notificationSettings);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		entityCache.putResult(
			NotificationSettingsImpl.class, notificationSettingsModelImpl,
			false, true);

		cacheUniqueFindersCache(notificationSettingsModelImpl);

		if (isNew) {
			notificationSettings.setNew(false);
		}

		notificationSettings.resetOriginalValues();

		return notificationSettings;
	}

	/**
	 * Returns the notification settings with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the notification settings
	 * @return the notification settings
	 * @throws NoSuchNotificationSettingsException if a notification settings with the primary key could not be found
	 */
	@Override
	public NotificationSettings findByPrimaryKey(Serializable primaryKey)
		throws NoSuchNotificationSettingsException {

		NotificationSettings notificationSettings = fetchByPrimaryKey(
			primaryKey);

		if (notificationSettings == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchNotificationSettingsException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return notificationSettings;
	}

	/**
	 * Returns the notification settings with the primary key or throws a <code>NoSuchNotificationSettingsException</code> if it could not be found.
	 *
	 * @param notificationSettingsId the primary key of the notification settings
	 * @return the notification settings
	 * @throws NoSuchNotificationSettingsException if a notification settings with the primary key could not be found
	 */
	@Override
	public NotificationSettings findByPrimaryKey(long notificationSettingsId)
		throws NoSuchNotificationSettingsException {

		return findByPrimaryKey((Serializable)notificationSettingsId);
	}

	/**
	 * Returns the notification settings with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param notificationSettingsId the primary key of the notification settings
	 * @return the notification settings, or <code>null</code> if a notification settings with the primary key could not be found
	 */
	@Override
	public NotificationSettings fetchByPrimaryKey(long notificationSettingsId) {
		return fetchByPrimaryKey((Serializable)notificationSettingsId);
	}

	/**
	 * Returns all the notification settingses.
	 *
	 * @return the notification settingses
	 */
	@Override
	public List<NotificationSettings> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the notification settingses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @return the range of notification settingses
	 */
	@Override
	public List<NotificationSettings> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the notification settingses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of notification settingses
	 */
	@Override
	public List<NotificationSettings> findAll(
		int start, int end,
		OrderByComparator<NotificationSettings> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the notification settingses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>NotificationSettingsModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of notification settingses
	 * @param end the upper bound of the range of notification settingses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of notification settingses
	 */
	@Override
	public List<NotificationSettings> findAll(
		int start, int end,
		OrderByComparator<NotificationSettings> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindAll;
				finderArgs = FINDER_ARGS_EMPTY;
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<NotificationSettings> list = null;

		if (useFinderCache) {
			list = (List<NotificationSettings>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler sb = null;
			String sql = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				sb.append(_SQL_SELECT_NOTIFICATIONSETTINGS);

				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = sb.toString();
			}
			else {
				sql = _SQL_SELECT_NOTIFICATIONSETTINGS;

				sql = sql.concat(NotificationSettingsModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				list = (List<NotificationSettings>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the notification settingses from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (NotificationSettings notificationSettings : findAll()) {
			remove(notificationSettings);
		}
	}

	/**
	 * Returns the number of notification settingses.
	 *
	 * @return the number of notification settingses
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(
					_SQL_COUNT_NOTIFICATIONSETTINGS);

				count = (Long)query.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected EntityCache getEntityCache() {
		return entityCache;
	}

	@Override
	protected String getPKDBName() {
		return "notificationSettingsId";
	}

	@Override
	protected String getSelectSQL() {
		return _SQL_SELECT_NOTIFICATIONSETTINGS;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return NotificationSettingsModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the notification settings persistence.
	 */
	@Activate
	public void activate(BundleContext bundleContext) {
		_bundleContext = bundleContext;

		_argumentsResolverServiceRegistration = _bundleContext.registerService(
			ArgumentsResolver.class,
			new NotificationSettingsModelArgumentsResolver(),
			MapUtil.singletonDictionary(
				"model.class.name", NotificationSettings.class.getName()));

		_valueObjectFinderCacheListThreshold = GetterUtil.getInteger(
			PropsUtil.get(PropsKeys.VALUE_OBJECT_FINDER_CACHE_LIST_THRESHOLD));

		_finderPathWithPaginationFindAll = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathWithoutPaginationFindAll = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathCountAll = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0], new String[0], false);

		_finderPathWithPaginationFindByUuid = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"uuid_"}, true);

		_finderPathWithoutPaginationFindByUuid = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			true);

		_finderPathCountByUuid = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			false);

		_finderPathFetchByUUID_G = _createFinderPath(
			FINDER_CLASS_NAME_ENTITY, "fetchByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "groupId"}, true);

		_finderPathCountByUUID_G = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "groupId"}, false);

		_finderPathWithPaginationFindByUuid_C = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathWithoutPaginationFindByUuid_C = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathCountByUuid_C = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, false);

		_finderPathFetchByAppUserId = _createFinderPath(
			FINDER_CLASS_NAME_ENTITY, "fetchByAppUserId",
			new String[] {Integer.class.getName()}, new String[] {"appUserId"},
			true);

		_finderPathCountByAppUserId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByAppUserId",
			new String[] {Integer.class.getName()}, new String[] {"appUserId"},
			false);

		_finderPathWithPaginationFindByNews = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByNews",
			new String[] {
				Boolean.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"news"}, true);

		_finderPathWithoutPaginationFindByNews = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByNews",
			new String[] {Boolean.class.getName()}, new String[] {"news"},
			true);

		_finderPathCountByNews = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByNews",
			new String[] {Boolean.class.getName()}, new String[] {"news"},
			false);

		_finderPathWithPaginationFindByEvents = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByEvents",
			new String[] {
				Boolean.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"events"}, true);

		_finderPathWithoutPaginationFindByEvents = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByEvents",
			new String[] {Boolean.class.getName()}, new String[] {"events"},
			true);

		_finderPathCountByEvents = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByEvents",
			new String[] {Boolean.class.getName()}, new String[] {"events"},
			false);

		_finderPathWithPaginationFindByHighlights = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByHighlights",
			new String[] {
				Boolean.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"highlights"}, true);

		_finderPathWithoutPaginationFindByHighlights = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByHighlights",
			new String[] {Boolean.class.getName()}, new String[] {"highlights"},
			true);

		_finderPathCountByHighlights = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByHighlights",
			new String[] {Boolean.class.getName()}, new String[] {"highlights"},
			false);

		_finderPathWithPaginationFindByBroadcastMessage = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByBroadcastMessage",
			new String[] {
				Boolean.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"broadcastMessage"}, true);

		_finderPathWithoutPaginationFindByBroadcastMessage = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByBroadcastMessage",
			new String[] {Boolean.class.getName()},
			new String[] {"broadcastMessage"}, true);

		_finderPathCountByBroadcastMessage = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByBroadcastMessage", new String[] {Boolean.class.getName()},
			new String[] {"broadcastMessage"}, false);

		_setNotificationSettingsUtilPersistence(this);
	}

	@Deactivate
	public void deactivate() {
		_setNotificationSettingsUtilPersistence(null);

		entityCache.removeCache(NotificationSettingsImpl.class.getName());

		_argumentsResolverServiceRegistration.unregister();

		for (ServiceRegistration<FinderPath> serviceRegistration :
				_serviceRegistrations) {

			serviceRegistration.unregister();
		}
	}

	private void _setNotificationSettingsUtilPersistence(
		NotificationSettingsPersistence notificationSettingsPersistence) {

		try {
			Field field = NotificationSettingsUtil.class.getDeclaredField(
				"_persistence");

			field.setAccessible(true);

			field.set(null, notificationSettingsPersistence);
		}
		catch (ReflectiveOperationException reflectiveOperationException) {
			throw new RuntimeException(reflectiveOperationException);
		}
	}

	@Override
	@Reference(
		target = NotificationSettingsPersistenceConstants.SERVICE_CONFIGURATION_FILTER,
		unbind = "-"
	)
	public void setConfiguration(Configuration configuration) {
	}

	@Override
	@Reference(
		target = NotificationSettingsPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	@Reference(
		target = NotificationSettingsPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	private BundleContext _bundleContext;

	@Reference
	protected EntityCache entityCache;

	@Reference
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_NOTIFICATIONSETTINGS =
		"SELECT notificationSettings FROM NotificationSettings notificationSettings";

	private static final String _SQL_SELECT_NOTIFICATIONSETTINGS_WHERE =
		"SELECT notificationSettings FROM NotificationSettings notificationSettings WHERE ";

	private static final String _SQL_COUNT_NOTIFICATIONSETTINGS =
		"SELECT COUNT(notificationSettings) FROM NotificationSettings notificationSettings";

	private static final String _SQL_COUNT_NOTIFICATIONSETTINGS_WHERE =
		"SELECT COUNT(notificationSettings) FROM NotificationSettings notificationSettings WHERE ";

	private static final String _ORDER_BY_ENTITY_ALIAS =
		"notificationSettings.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No NotificationSettings exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No NotificationSettings exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		NotificationSettingsPersistenceImpl.class);

	private static final Set<String> _badColumnNames = SetUtil.fromArray(
		new String[] {"uuid"});

	private FinderPath _createFinderPath(
		String cacheName, String methodName, String[] params,
		String[] columnNames, boolean baseModelResult) {

		FinderPath finderPath = new FinderPath(
			cacheName, methodName, params, columnNames, baseModelResult);

		if (!cacheName.equals(FINDER_CLASS_NAME_LIST_WITH_PAGINATION)) {
			_serviceRegistrations.add(
				_bundleContext.registerService(
					FinderPath.class, finderPath,
					MapUtil.singletonDictionary("cache.name", cacheName)));
		}

		return finderPath;
	}

	private Set<ServiceRegistration<FinderPath>> _serviceRegistrations =
		new HashSet<>();
	private ServiceRegistration<ArgumentsResolver>
		_argumentsResolverServiceRegistration;

	private static class NotificationSettingsModelArgumentsResolver
		implements ArgumentsResolver {

		@Override
		public Object[] getArguments(
			FinderPath finderPath, BaseModel<?> baseModel, boolean checkColumn,
			boolean original) {

			String[] columnNames = finderPath.getColumnNames();

			if ((columnNames == null) || (columnNames.length == 0)) {
				if (baseModel.isNew()) {
					return new Object[0];
				}

				return null;
			}

			NotificationSettingsModelImpl notificationSettingsModelImpl =
				(NotificationSettingsModelImpl)baseModel;

			long columnBitmask =
				notificationSettingsModelImpl.getColumnBitmask();

			if (!checkColumn || (columnBitmask == 0)) {
				return _getValue(
					notificationSettingsModelImpl, columnNames, original);
			}

			Long finderPathColumnBitmask = _finderPathColumnBitmasksCache.get(
				finderPath);

			if (finderPathColumnBitmask == null) {
				finderPathColumnBitmask = 0L;

				for (String columnName : columnNames) {
					finderPathColumnBitmask |=
						notificationSettingsModelImpl.getColumnBitmask(
							columnName);
				}

				_finderPathColumnBitmasksCache.put(
					finderPath, finderPathColumnBitmask);
			}

			if ((columnBitmask & finderPathColumnBitmask) != 0) {
				return _getValue(
					notificationSettingsModelImpl, columnNames, original);
			}

			return null;
		}

		private static Object[] _getValue(
			NotificationSettingsModelImpl notificationSettingsModelImpl,
			String[] columnNames, boolean original) {

			Object[] arguments = new Object[columnNames.length];

			for (int i = 0; i < arguments.length; i++) {
				String columnName = columnNames[i];

				if (original) {
					arguments[i] =
						notificationSettingsModelImpl.getColumnOriginalValue(
							columnName);
				}
				else {
					arguments[i] = notificationSettingsModelImpl.getColumnValue(
						columnName);
				}
			}

			return arguments;
		}

		private static final Map<FinderPath, Long>
			_finderPathColumnBitmasksCache = new ConcurrentHashMap<>();

	}

}