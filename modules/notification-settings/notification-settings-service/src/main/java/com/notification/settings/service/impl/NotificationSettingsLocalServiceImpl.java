/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.notification.settings.service.impl;

import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.portal.aop.AopService;
import com.notification.settings.model.NotificationSettings;
import com.notification.settings.service.base.NotificationSettingsLocalServiceBaseImpl;

import misk.app.users.model.AppUser;
import org.osgi.service.component.annotations.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * The implementation of the notification settings local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * <code>com.notification.settings.service.NotificationSettingsLocalService</code>
 * interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see NotificationSettingsLocalServiceBaseImpl
 */
@Component(property = "model.class.name=com.notification.settings.model.NotificationSettings", service = AopService.class)
public class NotificationSettingsLocalServiceImpl extends NotificationSettingsLocalServiceBaseImpl {


	public NotificationSettings fetchByAppUserId(int appUserId) {

		return notificationSettingsPersistence.fetchByAppUserId(appUserId);
	}

	public List<NotificationSettings> getNotificationEnabledUsers(String notificationType, boolean status){
		return  notificationSettingsFinder.findNotificationEnabledUsers(notificationType, status);
	}

	public List<NotificationSettings> getNotificationEnabledUsers(String notificationType, long typeId, boolean status){
		return  notificationSettingsFinder.findNotificationEnabledUsers(notificationType, typeId, status);
	}
	public NotificationSettings setDefaultNotificationSettingsForUser(int appUserId, long companyId, long groupId){
		NotificationSettings dbNotificationSettings = createNotificationSettings(CounterLocalServiceUtil.increment());

		dbNotificationSettings.setCompanyId(companyId);
		dbNotificationSettings.setGroupId(groupId);

		dbNotificationSettings.setAppUserId(appUserId);
		dbNotificationSettings.setNews(Boolean.TRUE);
		dbNotificationSettings.setEvents(Boolean.TRUE);
		dbNotificationSettings.setHighlights(Boolean.TRUE);
		dbNotificationSettings.setBroadcastMessage(Boolean.TRUE);
		return notificationSettingsLocalService.addNotificationSettings(dbNotificationSettings);
	}

}