package com.notification.settings.service.persistence.impl;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.dao.orm.custom.sql.CustomSQL;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.notification.settings.constants.NotificationSettingsKeys;
import com.notification.settings.model.NotificationSettings;
import com.notification.settings.model.impl.NotificationSettingsImpl;
import com.notification.settings.service.persistence.NotificationSettingsFinder;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import java.util.Collections;
import java.util.List;
import java.util.Objects;


@Component(service = NotificationSettingsFinder.class)
public class NotificationSettingsFinderImpl
        extends NotificationSettingsFinderBaseImpl implements NotificationSettingsFinder {

    public static final String FIND_NOTIFICATION_ENABLED_USER_IDS_BY_NEWS =
            NotificationSettingsFinder.class.getName() + ".findNotificationEnabledUserIdsByNews";
    public static final String FIND_NOTIFICATION_ENABLED_USER_IDS_BY_EVENTS =
            NotificationSettingsFinder.class.getName() + ".findNotificationEnabledUserIdsByEvents";
    public static final String FIND_NOTIFICATION_ENABLED_USER_IDS_BY_EVENT_UPDATE =
            NotificationSettingsFinder.class.getName() + ".findNotificationEnabledUserIdsByEventUpdate";
    public static final String FIND_NOTIFICATION_ENABLED_USER_IDS_BY_HIGHLIGHTS =
            NotificationSettingsFinder.class.getName() + ".findNotificationEnabledUserIdsByHighlights";
    public static final String FIND_NOTIFICATION_ENABLED_USER_IDS_BY_BROADCAST_MESSAGE =
            NotificationSettingsFinder.class.getName() + ".findNotificationEnabledUserIdsByBroadcastMessage";
    Log _log = LogFactoryUtil.getLog(NotificationSettingsFinderImpl.class);
    public List<NotificationSettings> findNotificationEnabledUsers(String notificationType, boolean status) {
        Session session = null;
        List<NotificationSettings> notificationSettings = Collections.emptyList();

        try {
            session = openSession();

            // create switch case to get the custom sql query
            String sql = StringPool.BLANK;
            switch (notificationType){
                case NotificationSettingsKeys.NOTIFICATION_TYPE_NEWS:
                    sql = _customSQL.get(getClass(), FIND_NOTIFICATION_ENABLED_USER_IDS_BY_NEWS);
                    break;
                case NotificationSettingsKeys.NOTIFICATION_TYPE_EVENT:
                    sql = _customSQL.get(getClass(), FIND_NOTIFICATION_ENABLED_USER_IDS_BY_EVENTS);
                    break;
                case NotificationSettingsKeys.NOTIFICATION_TYPE_HIGHLIGHT:
                    sql = _customSQL.get(getClass(), FIND_NOTIFICATION_ENABLED_USER_IDS_BY_HIGHLIGHTS);
                    break;
                case NotificationSettingsKeys.NOTIFICATION_TYPE_BROADCAST_MESSAGE:
                    sql = _customSQL.get(getClass(), FIND_NOTIFICATION_ENABLED_USER_IDS_BY_BROADCAST_MESSAGE);
                    break;
            }

            if(Validator.isNotNull(sql)){
                SQLQuery sqlQuery = session.createSynchronizedSQLQuery(sql);
                //sqlQuery.setCacheable(false);
                sqlQuery.addEntity("NotificationSettings", NotificationSettingsImpl.class);
                QueryPos qPos = QueryPos.getInstance(sqlQuery);
                qPos.add(status);
                notificationSettings = (List<NotificationSettings>) sqlQuery.list();

            }

            return notificationSettings;
        }
        catch (Exception exception) {
            throw new SystemException(exception);
        }
        finally {
            closeSession(session);
        }
    }

    public List<NotificationSettings> findNotificationEnabledUsers(String notificationType, long typeId, boolean status) {
        Session session = null;
        List<NotificationSettings> notificationSettings = Collections.emptyList();

        try {
            session = openSession();

            // create switch case to get the custom sql query
            String sql = StringPool.BLANK;
            if (Objects.equals(notificationType, NotificationSettingsKeys.NOTIFICATION_TYPE_EVENT_UPDATE)) {
                sql = _customSQL.get(getClass(), FIND_NOTIFICATION_ENABLED_USER_IDS_BY_EVENT_UPDATE);
            }

            if(Validator.isNotNull(sql)){
                SQLQuery sqlQuery = session.createSynchronizedSQLQuery(sql);
                //sqlQuery.setCacheable(false);
                sqlQuery.addEntity("NotificationSettings", NotificationSettingsImpl.class);
                QueryPos qPos = QueryPos.getInstance(sqlQuery);
                qPos.add(status);
                qPos.add(typeId);
                notificationSettings = (List<NotificationSettings>) sqlQuery.list();

            }

            return notificationSettings;
        }
        catch (Exception exception) {
            throw new SystemException(exception);
        }
        finally {
            closeSession(session);
        }
    }

    @Reference
    private CustomSQL _customSQL;
}
