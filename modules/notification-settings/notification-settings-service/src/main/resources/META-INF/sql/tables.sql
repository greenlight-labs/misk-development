create table misk_notification_settings (
	uuid_ VARCHAR(75) null,
	notificationSettingsId LONG not null primary key,
	groupId LONG,
	appUserId INTEGER,
	companyId LONG,
	createDate DATE null,
	modifiedDate DATE null,
	news BOOLEAN,
	events BOOLEAN,
	highlights BOOLEAN,
	broadcastMessage BOOLEAN
);