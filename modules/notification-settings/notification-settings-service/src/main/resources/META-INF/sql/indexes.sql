create index IX_720899C5 on misk_notification_settings (appUserId);
create index IX_7BBB37AC on misk_notification_settings (broadcastMessage);
create index IX_481C3CFF on misk_notification_settings (events);
create index IX_488D1AA5 on misk_notification_settings (highlights);
create index IX_EBE60D39 on misk_notification_settings (news);
create index IX_5929E626 on misk_notification_settings (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_1421BE28 on misk_notification_settings (uuid_[$COLUMN_LENGTH:75$], groupId);