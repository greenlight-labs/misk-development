<!DOCTYPE html>
<!--[if lte IE 9]>
<html lang="en" class="ie9">
<![endif]-->
<!--[if gt IE 9]><!-->
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Misk City">
    <meta name="keywords" content="Misk City">
    <meta name="author" content="Traffic Digital"/>
    <link rel="apple-touch-icon" sizes="180x180" href="/assets/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/favicon/favicon-16x16.png">
    <!--<link rel="manifest" href="/site.webmanifest">-->
    <meta name="msapplication-TileColor" content="#DA1884">
    <meta name="theme-color" content="#DA1884">
    <meta name="HandheldFriendly" content="True">
    <link rel="stylesheet" href="/assets/css/style.css?v=2">
    <!--[if IE]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <title>404 Page Not Found - Misk City</title>
</head>
<body>
<main id="container" data-scroll-container style="overflow: hidden">
    <div class="no-banner-space" data-scroll-section>
        <div class="static-header">
            <div class="head-white-bg"></div>
            <header>
                <div class="container">
                    <div class="row align-content-center">
                        <div class="col-6 col-md-4 animate" data-animation="fadeInLeft">
                            <a href="/en/" class="logo"><img src="/assets/svgs/logo.svg" alt="" class="img-fluid"></a>
                        </div>
                        <div class="col-6 col-md-8 animate" data-animation="fadeInRight">
                        </div>
                    </div>
                </div>
            </header>
        </div>
    </div>
    <section class="page-404" data-scroll-section>
        <div class="liveable-right-shape animate" data-animation="fadeInRight" data-duration="700"><img src="/assets/svgs/pink-shape.svg" alt="" class="img-fluid"></div>
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h2 class="animate" data-animation="fadeInUp" data-duration="400">ERROR 404<br> NOT FOUND</h2>
                    <p class="animate" data-animation="fadeInUp" data-duration="400">
                        You may have mis-typed the URL. Or the page has been removed. Actually, there is nothing to see here...<br/> Click on the links below to do something, Thanks!</p>
                    <a href="/en/" class="animate btn btn-outline-primary" data-animation="fadeInUp" data-duration="400"><span>TAKE ME OUT OF HERE</span><i class="dot-line"></i></a>
                </div>
            </div>
        </div>
    </section>
    <footer data-scroll-section>
        <div class="container" data-scroll data-scroll-speed="0">
            <div class="row">
                <div class="col-12 order-last col-md-6 order-md-0 animate" data-animation="fadeInLeft" data-duration="300">
                    <p>&copy; 2021 MiSK City. All rights Reserved</p>
                </div>
            </div>
        </div>
    </footer>
</main>
<script src="https://polyfill.io/v3/polyfill.min.js?features=Array.prototype.forEach%2CNodeList.prototype.forEach%2CCustomEvent%2CEvent%2CObject.assign"></script>
<script src="/assets/js/app.min.js"></script>
<script src="/assets/js/main.js"></script>
</body>
</html>